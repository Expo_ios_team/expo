//
//  EXTextField.swift
//  Expo
//
//  Created by Abdul Malik Aman on 26/04/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let fontSize = (self.font!.pointSize)
        self.font = UIFont(name: FONT_NAME, size: fontSize)
        
    }
    // Cursor hide
    //    override func caretRectForPosition(position: UITextPosition) -> CGRect {
    //        return CGRect.zero
    //    }
    override func selectionRectsForRange(range: UITextRange) -> [AnyObject] {
        return []
    }
    
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        
        if action == Selector("copy:") || action == Selector("selectAll:") || action == Selector("paste:") {
            
            return false
            
        }
        
        return super.canPerformAction(action, withSender: sender)
        
    }

}
