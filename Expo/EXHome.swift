//
//  EXHome.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXHome: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate, Change_Title_Logo_Delegate {
    
    // MARK: - variables
    @IBOutlet var scrollContainerBackView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var scrollContentView: UIView!
    @IBOutlet var highlightImage: [UIImageView]!
    @IBOutlet var videoImage: [UIImageView]!
    @IBOutlet var featuredImage: [UIImageView]!
    @IBOutlet var whatsNewImage: [UIImageView]!
    @IBOutlet var previousButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var pagerBackView: UIView!
    @IBOutlet var videoAddImage: UIImageView!
    @IBOutlet var featuredCompanyImage: UIImageView!
    @IBOutlet var whatNewImageOne: UIImageView!
    @IBOutlet var whatNewImageTwo: UIImageView!
    @IBOutlet var pagerDotBackView: UIView!
    @IBOutlet weak var imageScroll: UIScrollView!
    @IBOutlet var imageScrollBackView: UIView!
    
    // MARK: - outlets foer localization
    
    @IBOutlet var highlightsLabel: UILabel!
    @IBOutlet var seperatorHighlightsBackView: UIView!
    @IBOutlet var seperatorHighlightsWhiteView: UIView!
    @IBOutlet var videoAdd_FeaturedCompanyBackView: UIView!
    @IBOutlet var videoAddHeadingBackView: UIView!
    @IBOutlet var videoAddHeadingLabel: UILabel!
    @IBOutlet var videoAddHeadingArrowLabel: UILabel!
    @IBOutlet var videoAddSeperatorBackView: UIView!
    @IBOutlet var videoAddSeperatorWhiteView: UIView!
    @IBOutlet var featuredCompanyHeadingBackView: UIView!
    @IBOutlet var featuredCompanyHeadingLabel: UILabel!
    @IBOutlet var featuredCompanyArrowLabel: UILabel!
    @IBOutlet var featuredCompanySeperatorBackView: UIView!
    @IBOutlet var featuredCompanySeperatorWhiteView: UIView!
    @IBOutlet var videoAddButton: UIButton!
    @IBOutlet var featuredCompanyButton: UIButton!
    @IBOutlet var whatsNewBackView: UIView!
    @IBOutlet var whatsNewHeadingBackView: UIView!
    @IBOutlet var whatsNewHeadingLabel: UILabel!
    @IBOutlet var whatsNewArrowLabel: UILabel!
    @IBOutlet var whatsNewSeperatorBackView: UIView!
    @IBOutlet var whatsNewSeperatorWhiteView: UIView!
    
    @IBOutlet weak var videoScroll: UIScrollView!
    @IBOutlet weak var featuredCompanyScroll: UIScrollView!
    @IBOutlet weak var whatsNewScroll: UIScrollView!
    
    
    var pageIndex = 0
    
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    
    var currentHighlightImageCount: Int = 0
    var highlightsImages: [String] = []
    var featuredImages: [String] = []
    var whatsNewImages: [String] = []
    var VideoImages: NSArray!
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Home Page")
        let slideLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        let slideRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        slideRight.direction = UISwipeGestureRecognizerDirection.Right
        slideLeft.direction = UISwipeGestureRecognizerDirection.Left
        //        self.highlightImage.addGestureRecognizer(slideRight)
        //        self.highlightImage.addGestureRecognizer(slideLeft)
        //        self.highlightImage.userInteractionEnabled = true
        slideLeft.delegate = self
        slideRight.delegate = self
        
        //NSTimer.scheduledTimerWithTimeInterval(8, target: self, selector: "autoScroll", userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        localizeLayoutSubviews()
        title_Logo_Delegate!.mainTitleChange!("")
        title_Logo_Delegate!.ChangeLogoImage!(true)
        title_Logo_Delegate!.HideBackButton!(true)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        title_Logo_Delegate!.HomeSetSelected!(true)
        self.mainScrollView.delegate = self
        
        self.highlightsLabel.text = "HIGHLIGHTS".localized(LANGUAGE!)
        self.videoAddHeadingLabel.text = "VIDEO ADS".localized(LANGUAGE!)
        self.featuredCompanyHeadingLabel.text = "FEATURED COMPANY".localized(LANGUAGE!)
        self.whatsNewHeadingLabel.text = "WHATS NEW".localized(LANGUAGE!)
        
        var params: NSDictionary!
        if let userId = EX_DEFAULTS.valueForKey("User_Id") {
            params  = ["UserId": userId, "lang_key": LANGUAGE!]
            postHome(params)
        }else {
            params  = ["lang_key": LANGUAGE!]
            postHome(params)
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if LANGUAGE == "en"{
            
            ///////////// Top view
            
            self.highlightsLabel.textAlignment = NSTextAlignment.Left
            self.seperatorHighlightsWhiteView.frame = CGRectMake(0, self.seperatorHighlightsWhiteView.frame.origin.y, self.seperatorHighlightsWhiteView.frame.size.width, self.seperatorHighlightsWhiteView.frame.size.height)
            
            ///////////// Top view
            
            ///////////// Middle View
            
            self.videoAddHeadingBackView.frame = CGRectMake(0, self.videoAddHeadingBackView.frame.origin.y, self.videoAddHeadingBackView.frame.size.width, self.videoAddHeadingBackView.frame.size.height)
            self.featuredCompanyHeadingBackView.frame = CGRectMake(self.videoAdd_FeaturedCompanyBackView.frame.size.width - self.featuredCompanyHeadingBackView.frame.size.width, self.featuredCompanyHeadingBackView.frame.origin.y, self.featuredCompanyHeadingBackView.frame.size.width, self.featuredCompanyHeadingBackView.frame.size.height)
            
            self.videoAddImage.frame = CGRectMake(0, self.videoAddImage.frame.origin.y, self.videoAddImage.frame.size.width, self.videoAddImage.frame.size.height)
            self.videoAddHeadingLabel.textAlignment = NSTextAlignment.Left
            self.videoAddHeadingLabel.frame = CGRectMake(0, self.videoAddHeadingLabel.frame.origin.y, self.videoAddHeadingLabel.frame.size.width, self.videoAddHeadingLabel.frame.size.height)
            self.videoAddHeadingArrowLabel.frame = CGRectMake(self.videoAddHeadingBackView.frame.size.width - self.videoAddHeadingArrowLabel.frame.size.width, self.videoAddHeadingArrowLabel.frame.origin.y, self.videoAddHeadingArrowLabel.frame.size.width, self.videoAddHeadingArrowLabel.frame.size.height)
            
            self.featuredCompanyHeadingLabel.textAlignment = NSTextAlignment.Left
            self.featuredCompanyHeadingLabel.frame = CGRectMake(0, self.featuredCompanyHeadingLabel.frame.origin.y, self.featuredCompanyHeadingLabel.frame.size.width, self.featuredCompanyHeadingLabel.frame.size.height)
            self.featuredCompanyArrowLabel.frame = CGRectMake(self.featuredCompanyHeadingBackView.frame.size.width - self.featuredCompanyArrowLabel.frame.size.width, self.featuredCompanyArrowLabel.frame.origin.y, self.featuredCompanyArrowLabel.frame.size.width, self.featuredCompanyArrowLabel.frame.size.height)
            
            self.videoAddSeperatorWhiteView.frame = CGRectMake(0, self.videoAddSeperatorWhiteView.frame.origin.y, self.videoAddSeperatorWhiteView.frame.size.width, self.videoAddSeperatorWhiteView.frame.size.height)
            self.featuredCompanySeperatorWhiteView.frame = CGRectMake(0, self.featuredCompanySeperatorWhiteView.frame.origin.y, self.featuredCompanySeperatorWhiteView.frame.size.width, self.featuredCompanySeperatorWhiteView.frame.size.height)
            
            
            self.featuredCompanyImage.frame = CGRectMake(self.videoAdd_FeaturedCompanyBackView.frame.size.width - self.featuredCompanyImage.frame.size.width, self.featuredCompanyImage.frame.origin.y, self.featuredCompanyImage.frame.size.width, self.featuredCompanyImage.frame.size.height)
            
            self.videoAddButton.frame = CGRectMake(0, self.videoAddButton.frame.origin.y, self.videoAddButton.frame.size.width, self.videoAddButton.frame.size.height)
            self.featuredCompanyButton.frame = CGRectMake(self.videoAdd_FeaturedCompanyBackView.frame.size.width - self.featuredCompanyButton.frame.size.width, self.featuredCompanyButton.frame.origin.y, self.featuredCompanyButton.frame.size.width, self.featuredCompanyButton.frame.size.height)
            
            ///////////// Middle View
            
            ///////////// Down View
            
            self.whatsNewHeadingLabel.textAlignment = NSTextAlignment.Left
            self.whatsNewHeadingLabel.frame = CGRectMake(0, self.whatsNewHeadingLabel.frame.origin.y, self.whatsNewHeadingLabel.frame.size.width, self.whatsNewHeadingLabel.frame.size.height)
            self.whatsNewArrowLabel.frame = CGRectMake(self.whatsNewHeadingBackView.frame.size.width - self.whatsNewArrowLabel.frame.size.width, self.whatsNewArrowLabel.frame.origin.y, self.whatsNewArrowLabel.frame.size.width, self.whatsNewArrowLabel.frame.size.height)
            
            self.whatNewImageOne.frame = CGRectMake(0, self.whatNewImageOne.frame.origin.y, self.whatNewImageOne.frame.size.width, self.whatNewImageOne.frame.size.height)
            self.whatNewImageTwo.frame = CGRectMake(self.whatsNewBackView.frame.size.width - self.whatNewImageTwo.frame.size.width, self.whatNewImageTwo.frame.origin.y, self.whatNewImageTwo.frame.size.width, self.whatNewImageTwo.frame.size.height)
            ///////////// Down View
            
        }else{
            ///////////// Top view
            
            self.highlightsLabel.textAlignment = NSTextAlignment.Right
            self.seperatorHighlightsWhiteView.frame = CGRectMake(self.seperatorHighlightsBackView.frame.size.width - self.seperatorHighlightsWhiteView.frame.width, self.seperatorHighlightsWhiteView.frame.origin.y, self.seperatorHighlightsWhiteView.frame.size.width, self.seperatorHighlightsWhiteView.frame.size.height)
            
            ///////////// Top view
            
            ///////////// Middle View
            print(self.videoAdd_FeaturedCompanyBackView.frame.size.width)
            print(self.videoAddHeadingBackView.frame.size.width)
            print(self.videoAdd_FeaturedCompanyBackView.frame.size.width - self.videoAddHeadingBackView.frame.size.width)
            self.videoAddHeadingBackView.frame = CGRectMake(self.videoAdd_FeaturedCompanyBackView.frame.size.width - self.videoAddHeadingBackView.frame.size.width, self.videoAddHeadingBackView.frame.origin.y, self.videoAddHeadingBackView.frame.size.width, self.videoAddHeadingBackView.frame.size.height)
            self.videoAddHeadingLabel.textAlignment = NSTextAlignment.Right
            self.videoAddHeadingLabel.frame = CGRectMake(self.videoAddHeadingBackView.frame.size.width - self.videoAddHeadingLabel.frame.size.width, self.videoAddHeadingLabel.frame.origin.y, self.videoAddHeadingLabel.frame.size.width, self.videoAddHeadingLabel.frame.size.height)
            self.videoAddHeadingArrowLabel.frame = CGRectMake(0, self.videoAddHeadingArrowLabel.frame.origin.y, self.videoAddHeadingArrowLabel.frame.size.width, self.videoAddHeadingArrowLabel.frame.size.height)
            
            self.featuredCompanyHeadingLabel.textAlignment = NSTextAlignment.Right
            self.featuredCompanyHeadingLabel.frame = CGRectMake(self.featuredCompanyHeadingBackView.frame.size.width - self.featuredCompanyHeadingLabel.frame.size.width, self.featuredCompanyHeadingLabel.frame.origin.y, self.featuredCompanyHeadingLabel.frame.size.width, self.featuredCompanyHeadingLabel.frame.size.height)
            self.featuredCompanyArrowLabel.frame = CGRectMake(0, self.featuredCompanyArrowLabel.frame.origin.y, self.featuredCompanyArrowLabel.frame.size.width, self.featuredCompanyArrowLabel.frame.size.height)
            
            self.videoAddSeperatorWhiteView.frame = CGRectMake(self.videoAddSeperatorBackView.frame.size.width - self.videoAddSeperatorWhiteView.frame.size.width, self.videoAddSeperatorWhiteView.frame.origin.y, self.videoAddSeperatorWhiteView.frame.size.width, self.videoAddSeperatorWhiteView.frame.size.height)
            self.featuredCompanySeperatorWhiteView.frame = CGRectMake(self.featuredCompanySeperatorBackView.frame.size.width - self.featuredCompanySeperatorWhiteView.frame.size.width, self.featuredCompanySeperatorWhiteView.frame.origin.y, self.featuredCompanySeperatorWhiteView.frame.size.width, self.featuredCompanySeperatorWhiteView.frame.size.height)
            
            
            self.featuredCompanyHeadingBackView.frame = CGRectMake(0, self.featuredCompanyHeadingBackView.frame.origin.y, self.featuredCompanyHeadingBackView.frame.size.width, self.featuredCompanyHeadingBackView.frame.size.height)
            
            self.videoAddImage.frame = CGRectMake(self.videoAdd_FeaturedCompanyBackView.frame.size.width - self.videoAddImage.frame.size.width, self.videoAddImage.frame.origin.y, self.videoAddImage.frame.size.width, self.videoAddImage.frame.size.height)
            self.featuredCompanyImage.frame = CGRectMake(0, self.featuredCompanyImage.frame.origin.y, self.featuredCompanyImage.frame.size.width, self.featuredCompanyImage.frame.size.height)
            
            self.videoAddButton.frame = CGRectMake(self.videoAdd_FeaturedCompanyBackView.frame.size.width - self.videoAddButton.frame.size.width, self.videoAddButton.frame.origin.y, self.videoAddButton.frame.size.width, self.videoAddButton.frame.size.height)
            self.featuredCompanyButton.frame = CGRectMake(0, self.featuredCompanyButton.frame.origin.y, self.featuredCompanyButton.frame.size.width, self.featuredCompanyButton.frame.size.height)
            
            ///////////// Middle View
            
            ///////////// Down View
            
            self.whatsNewHeadingLabel.textAlignment = NSTextAlignment.Right
            self.whatsNewHeadingLabel.frame = CGRectMake(self.whatsNewHeadingBackView.frame.size.width - self.whatsNewHeadingLabel.frame.size.width, self.whatsNewHeadingLabel.frame.origin.y, self.whatsNewHeadingLabel.frame.size.width, self.whatsNewHeadingLabel.frame.size.height)
            self.whatsNewArrowLabel.frame = CGRectMake(0, self.whatsNewArrowLabel.frame.origin.y, self.whatsNewArrowLabel.frame.size.width, self.whatsNewArrowLabel.frame.size.height)
            
            self.whatNewImageOne.frame = CGRectMake(self.whatsNewBackView.frame.size.width - self.whatNewImageOne.frame.size.width, self.whatNewImageOne.frame.origin.y, self.whatNewImageOne.frame.size.width, self.whatNewImageOne.frame.size.height)
            self.whatNewImageTwo.frame = CGRectMake(0, self.whatNewImageTwo.frame.origin.y, self.whatNewImageTwo.frame.size.width, self.whatNewImageTwo.frame.size.height)
            ///////////// Down View
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.scrollContentView.frame.size.height)
    }
    
    func localizeLayoutSubviews(){
        
    }
    
    // MARK: - Custom Delegates
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    // MARK: - Actions
    @IBAction func videoAddButtonAction(sender: UIButton) {
        performSegueWithIdentifier("VIdeo_Add", sender: self)
    }
    
    @IBAction func featuredCompanyButtonAction(sender: UIButton) {
        
    }
    func loadPager(count: Int, position: Int){
        if count >= 1 {
            self.pagerDotBackView.frame = CGRectMake(0, 0, (self.pagerBackView.frame.size.height * CGFloat(count)), self.pagerBackView.frame.size.height)
            self.pagerDotBackView.center = CGPointMake(self.pagerBackView.center.x, self.pagerDotBackView.center.y)
            var j = 0
            for (var i = 0; i < count; i++) {
                let dotButton : UIButton = UIButton(frame: CGRectMake(CGFloat(j), 0, self.pagerBackView.frame.size.height, self.pagerBackView.frame.size.height))
                if i == position {
                    dotButton.selected = true
                }
                dotButton.tag = i
                dotButton.setImage(UIImage(named: "pager"), forState: UIControlState.Normal)
                dotButton.setImage(UIImage(named: "active"), forState: UIControlState.Selected)
                dotButton.addTarget(self, action: "pagerDotButtonSelected:", forControlEvents: .TouchUpInside)
                j = j + Int(self.pagerDotBackView.frame.size.height)
                self.pagerDotBackView.addSubview(dotButton)
            }
        }
    }
    
    func pagerDotButtonSelected(sender: UIButton){
        currentHighlightImageCount = sender.tag
        changeHighlightImage(sender.tag)
        let width = self.imageScrollBackView.frame.size.width
        imageScroll.setContentOffset(CGPointMake(width * CGFloat(sender.tag), 0), animated: true)
        loadPager(self.highlightsImages.count, position: sender.tag)
    }
    func autoScroll() {
        let width = self.imageScrollBackView.frame.size.width
        if highlightsImages != [] {
            
            if highlightsImages.count == 1 {
                previousButton.hidden = true
                nextButton.hidden = true
            }else {
                previousButton.hidden = false
                nextButton.hidden = false
            }
            if pageIndex != highlightsImages.count-1 {
                let contect = Int(imageScroll.contentOffset.x) + Int(width)
                imageScroll.setContentOffset(CGPointMake(CGFloat(contect), 0), animated: true)
                pageIndex = pageIndex + 1
            }else {
                imageScroll.setContentOffset(CGPointMake(0, 0), animated: true)
                pageIndex = 0
            }
        }else {
            previousButton.hidden = true
            nextButton.hidden = true
        }
    }
    
    @IBAction func previous_NextButtonAction(sender: UIButton) {
        
        let scrollWidth: CGFloat = imageScroll.frame.size.width;
        let contentSize = imageScroll.contentSize.width
        print(imageScroll.contentSize.width)
        let page: CGFloat = (imageScroll.contentOffset.x + (0.5 * scrollWidth)) / scrollWidth
        let width = self.imageScrollBackView.frame.size.width
        if sender.tag == 1 {
            if pageIndex != highlightsImages.count-1 {
                let contect = Int(imageScroll.contentOffset.x) + Int(width)
                imageScroll.setContentOffset(CGPointMake(CGFloat(contect), 0), animated: true)
                pageIndex = pageIndex + 1
            }
            
            
            //            if currentHighlightImageCount > 0 {
            //                changeHighlightImage(--currentHighlightImageCount)
            //                loadPager(self.highlightsImages.count, position: currentHighlightImageCount)
            //            }
        } else {
            
            if pageIndex != 0 {
                let contect = Int(imageScroll.contentOffset.x) - Int(width)
                imageScroll.setContentOffset(CGPointMake(CGFloat(contect), 0), animated: true)
                pageIndex = pageIndex - 1
            }
            
            //            if currentHighlightImageCount < (highlightsImages.count-1) {
            //                changeHighlightImage(++currentHighlightImageCount)
            //                loadPager(self.highlightsImages.count, position: currentHighlightImageCount)
            //            }
        }
        loadPager(self.highlightsImages.count, position: pageIndex)
    }
    
    func changeHighlightImage(position: Int){
        print(position)
        
        //        self.highlightImage[0].sd_setImageWithURL(NSURL(string: highlightsImages[position]), placeholderImage: UIImage(named: "Loading"))
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped left")
                if currentHighlightImageCount < (highlightsImages.count-1) {
                    changeHighlightImage(++currentHighlightImageCount)
                    loadPager(self.highlightsImages.count, position: currentHighlightImageCount)
                }
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.Right:
                print("Swiped Right")
                if currentHighlightImageCount > 0 {
                    changeHighlightImage(--currentHighlightImageCount)
                    loadPager(self.highlightsImages.count, position: currentHighlightImageCount)
                }
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView == imageScroll {
            let pageWidth = imageScroll.frame.size.width
            let page = Int(floor((imageScroll.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
            pageIndex = page
            loadPager(highlightsImages.count, position: pageIndex)
        }
    }
    
    // MARK: - WebServices
    func postHome(params: NSDictionary) {
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
        }else {
            loadingImage = "loading_ar"
        }
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "home.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let highlightsData = Json["Highlights"] {
                        self.highlightsImages = highlightsData as! [String]
                        
                        let width = self.imageScrollBackView.frame.size.width
                        self.imageScroll.contentSize = CGSizeMake(CGFloat(width) * CGFloat(self.highlightsImages.count), 0)
                        self.highlightImage = [UIImageView](count: self.highlightsImages.count, repeatedValue: UIImageView())
                        
                        
                        
                        for i in 0..<self.highlightsImages.count {
                            
                            self.highlightImage[i] = UIImageView(frame: CGRectMake(width * CGFloat(i), 0, width, self.imageScroll.frame.size.height))
                            self.highlightImage[i].image = UIImage(named: loadingImage)
                            self.highlightImage[i].contentMode = UIViewContentMode.ScaleToFill
                            self.highlightImage[i].clipsToBounds = true
                            self.highlightImage[i].tag = i
                            self.imageScroll.addSubview(self.highlightImage[i])
                            //                            self.highlightImage[i].sd_setImageWithURL(NSURL(string: highlightsData[0]as! String), placeholderImage: UIImage(named: "Loading"))
                        }
                        for i in 0..<self.highlightImage.count {
                            if let url = NSURL(string: self.highlightsImages[i]) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                                    if let data = NSData(contentsOfURL: url) {
                                        dispatch_async(dispatch_get_main_queue()) {
                                            self.highlightImage[i].image = UIImage(data: data)
                                        }
                                    }
                                }
                            }
                        }
                        
                        self.pageIndex = 0
                        //                        self.highlightImage[0].sd_setImageWithURL(NSURL(string: highlightsData[0]as! String), placeholderImage: UIImage(named: "Loading"))
                        self.loadPager(self.highlightsImages.count, position: 0)
                    }
                    if let videoAdsData = Json["VideoAds"] {
                        print((videoAdsData[0])["Thumbnail"])
                        self.videoAddImage.sd_setImageWithURL(NSURL(string: (videoAdsData[0])["Thumbnail"]as! String), placeholderImage: UIImage(named: loadingImage))
                        
                        self.VideoImages = videoAdsData as? NSArray
                        
                        let width = self.videoScroll.frame.size.width
                        self.videoScroll.contentSize = CGSizeMake(CGFloat(width) * CGFloat(self.VideoImages.count), 0)
                        self.videoImage = [UIImageView](count: self.VideoImages.count, repeatedValue: UIImageView())
                        
                        
                        
                        for i in 0..<self.VideoImages.count {
                            
                            self.videoImage[i] = UIImageView(frame: CGRectMake(width * CGFloat(i), 0, width, self.videoScroll.frame.size.height))
                            self.videoImage[i].image = UIImage(named: loadingImage)
                            self.videoImage[i].contentMode = UIViewContentMode.ScaleToFill
                            self.videoImage[i].clipsToBounds = true
                            self.videoImage[i].tag = i
                            self.videoScroll.addSubview(self.videoImage[i])
                            //                            self.highlightImage[i].sd_setImageWithURL(NSURL(string: highlightsData[0]as! String), placeholderImage: UIImage(named: "Loading"))
                        }
                        for i in 0..<self.VideoImages.count {
                            if let url = NSURL(string: (self.VideoImages[i]["Thumbnail"] as? String)!) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                                    if let data = NSData(contentsOfURL: url) {
                                        dispatch_async(dispatch_get_main_queue()) {
                                            self.videoImage[i].image = UIImage(data: data)
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                    }
                    if let featuredCompanyData = Json["FeatureCompanies"] {
                        //print((featuredCompanyData[0])["Thumbnail"])
                        self.featuredCompanyImage.sd_setImageWithURL(NSURL(string: (featuredCompanyData[0]) as! String), placeholderImage: UIImage(named: loadingImage))
                        print(featuredCompanyData[0])
                        
                        self.featuredImages = featuredCompanyData as! [String]
                        
                        let width = self.featuredCompanyScroll.frame.size.width
                        self.featuredCompanyScroll.contentSize = CGSizeMake(CGFloat(width) * CGFloat(self.featuredImages.count), 0)
                        self.featuredImage = [UIImageView](count: self.featuredImages.count, repeatedValue: UIImageView())
                        
                        
                        
                        for i in 0..<self.featuredImages.count {
                            
                            self.featuredImage[i] = UIImageView(frame: CGRectMake(width * CGFloat(i), 0, width, self.featuredCompanyScroll.frame.size.height))
                            self.featuredImage[i].image = UIImage(named: loadingImage)
                            self.featuredImage[i].contentMode = UIViewContentMode.ScaleToFill
                            self.featuredImage[i].clipsToBounds = true
                            self.featuredImage[i].tag = i
                            self.featuredCompanyScroll.addSubview(self.featuredImage[i])
                            //                            self.highlightImage[i].sd_setImageWithURL(NSURL(string: highlightsData[0]as! String), placeholderImage: UIImage(named: "Loading"))
                        }
                        for i in 0..<self.featuredImages.count {
                            if let url = NSURL(string: (self.featuredImages[i] as? String)!) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                                    if let data = NSData(contentsOfURL: url) {
                                        dispatch_async(dispatch_get_main_queue()) {
                                            self.featuredImage[i].image = UIImage(data: data)
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                    }
                    if let whatsNewData = Json["WhatsNew"] {
                        self.whatNewImageOne.sd_setImageWithURL(NSURL(string: whatsNewData[0]as! String), placeholderImage: UIImage(named: loadingImage))
                        
                        self.whatsNewImages = whatsNewData as! [String]
                        
                        let width = self.whatsNewScroll.frame.size.width / 2
                        self.whatsNewScroll.contentSize = CGSizeMake(CGFloat(width) * CGFloat(self.whatsNewImages.count), 0)
                        self.whatsNewImage = [UIImageView](count: self.whatsNewImages.count, repeatedValue: UIImageView())
                        
                        
                        
                        for i in 0..<self.whatsNewImages.count {
                            
                            self.whatsNewImage[i] = UIImageView(frame: CGRectMake(width * CGFloat(i), 0, width, self.whatsNewScroll.frame.size.height))
                            self.whatsNewImage[i].image = UIImage(named: loadingImage)
                            self.whatsNewImage[i].contentMode = UIViewContentMode.ScaleToFill
                            self.whatsNewImage[i].clipsToBounds = true
                            self.whatsNewImage[i].tag = i
                            self.whatsNewScroll.addSubview(self.whatsNewImage[i])
                            //                            self.highlightImage[i].sd_setImageWithURL(NSURL(string: highlightsData[0]as! String), placeholderImage: UIImage(named: "Loading"))
                        }
                        for i in 0..<self.whatsNewImages.count {
                            if let url = NSURL(string: (self.whatsNewImages[i] as? String)!) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                                    if let data = NSData(contentsOfURL: url) {
                                        dispatch_async(dispatch_get_main_queue()) {
                                            self.whatsNewImage[i].image = UIImage(data: data)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if EX_DEFAULTS.valueForKey("isFirstLaunch") == nil {
                        EX_DEFAULTS.setValue(false, forKey: "isFirstLaunch")
                    }
                    if let userId = Json["User_Id"] {
                        USER_ID = userId as? String
                        EX_DEFAULTS.setValue(userId, forKey: "User_Id")
                    }
                    if Platform.isSimulator == false {
                        if MESSAGE_ID != nil {
                            let params = ["lang_key": LANGUAGE!, "User_Id": USER_ID!, "Device_Id": DEVICE_ID!, "Msg_Id": MESSAGE_ID!, "Platform": "2"]
                            self.postDeviceRegistration(params)
                        }
                    }
                }
            })
        }
    }
    func postDeviceRegistration(params: NSDictionary) {
        EX_Network.postJson(WEB_SERVICE + "device_registration.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Json)
                
                if Succeeded == true {
                    EX_DEFAULTS.setValue(true, forKey: "IS_Registered")
                }else {
                    
                }
            })
        }
    }
    
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "VIdeo_Add"{
            let VideoAddPage = segue.destinationViewController as! EXVIdeoAdd
            VideoAddPage.title_Logo_Delegate = self
        }
    }
    
    
}

struct Platform {
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
        return TARGET_IPHONE_SIMULATOR != 0
    }
    
}
