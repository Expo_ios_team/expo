//
//  EXCompanyInner.swift
//  Expo
//
//  Created by Abdul Malik Aman on 05/05/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXCompanyInner: UIViewController, Change_Title_Logo_Delegate {

    // MARK: - Variables
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var productList: NSArray!
    var CompanyId: String!
    var CompanyName: String!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ProductsCollection: UICollectionView!
    @IBOutlet var mainContainerView: UIView!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("")
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate?.navigationBack!(self.navigationController!, tag: 1)
        
        if productList == nil {
            let params = ["CompanyId": CompanyId, "lang_key": LANGUAGE!]
            postProductList(params)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        if LANGUAGE == "en"{
            self.titleLabel.textAlignment = NSTextAlignment.Left
        }else{
            self.titleLabel.textAlignment = NSTextAlignment.Right
        }
        titleLabel.text = CompanyName.uppercaseString
        
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    
    // MARK: - CollectionView Delegate Methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if productList != nil {
            if productList.count > 0 {
                return productList.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = ProductsCollection.dequeueReusableCellWithReuseIdentifier("products", forIndexPath: indexPath) as! EXCompanyInnerCell
        if LANGUAGE == "en"{
            cell.companyName.textAlignment = NSTextAlignment.Left
        }else{
            cell.companyName.textAlignment = NSTextAlignment.Right
        }
        if let productName = productList[indexPath.row]["ProductName"] {
            cell.companyName.text = productName.uppercaseString
        }
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
        }else {
            loadingImage = "loading_ar"
        }
        if let productImage = productList[indexPath.row]["Image"] as? String {
            cell.companyImage.sd_setImageWithURL(NSURL(string: productImage), placeholderImage: UIImage(named:loadingImage))
        }
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let categoryInner : EXCategoryInner = storyboard?.instantiateViewControllerWithIdentifier("EXCategoryInner") as! EXCategoryInner
        categoryInner.productId = productList[indexPath.row]["ID"] as? String
        categoryInner.categoryTitle = CompanyName.uppercaseString
        categoryInner.title_Logo_Delegate = self
        showViewController(categoryInner, sender: self)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    }
    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        let viewWidth = ProductsCollection.frame.width
//        let collectionSize = viewWidth / 2 - 3
//        let height = collectionSize + 20
//        return CGSizeMake(collectionSize , height)
//    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let viewWidth = (self.mainContainerView.frame.size.width/2) - 5
        print(viewWidth)
        return CGSizeMake(viewWidth , viewWidth * 1.2758620)
    }
    
    //    func collectionView(collectionView: UICollectionView,
    //        layout collectionViewLayout: UICollectionViewLayout,
    //        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    //            print(self.storeCollection.frame.size.width)
    //            return CGSizeMake(196 , 196)
    //    }
    //Use for interspacing
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 5.0
    }
    
    func collectionView(collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 5.0
    }
    
    // MARK: - WebServices
    func postProductList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "companylistinner.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let mainDict = Json["COMPNAYLIST_INNERPAGE"] {
                        self.productList = mainDict[0]["ProductList"] as? NSArray
                    }
                }
                self.ProductsCollection.reloadData()
            })
        }
    }

    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXCompanyInnerCell: UICollectionViewCell {
    
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyName: EXLabel!
    
}