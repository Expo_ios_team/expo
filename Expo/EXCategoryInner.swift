//
//  EXCategoryInner.swift
//  Expo
//
//  Created by Ronish on 5/3/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXCategoryInner: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var categoryInnerDictionary: NSDictionary!
    var categoryTitle: String!
    var productImageCount: Int = 0
    var productImages: [String] = []
    var productId: String!
    
    @IBOutlet var scrollContainerCategoryInnerView: UIView!
    @IBOutlet var mainScrollCategoryInnerView: UIScrollView!
    @IBOutlet var scrollContentCategoryInnerView: UIView!
    @IBOutlet var brandLogo: UIImageView!
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var dateOfPublishLabel: UILabel!
    @IBOutlet var numberOfViewLabel: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var companyNameLabel: UILabel!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet var topHeadingBackView: UIView!
    @IBOutlet var intermediateBackView: UIView!
    
    override func viewWillAppear(animated: Bool) {
        
        if categoryInnerDictionary == nil {
            if productId != "" {
                let params: NSDictionary = ["ProductId": "\(productId)", "lang_key": LANGUAGE!]
                postCategoryInner(params)
            }
        }
        
        title_Logo_Delegate!.mainTitleChange!(self.categoryTitle)
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        
        //print(categoryInnerDictionary)
        
        print(self.mainScrollCategoryInnerView.frame.size.width)
        print(self.mainScrollCategoryInnerView.frame.size.height)
        
        print(self.view.frame.size.width)
        print(self.view.frame.size.height)
        
        
        self.mainScrollCategoryInnerView.delegate = self
        
        //loadCategoryData()
        
        let slideLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        let slideRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        slideRight.direction = UISwipeGestureRecognizerDirection.Right
        slideLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.productImage.addGestureRecognizer(slideRight)
        self.productImage.addGestureRecognizer(slideLeft)
        self.productImage.userInteractionEnabled = true
        slideLeft.delegate = self
        slideRight.delegate = self
        
        //self.mainScrollCategoryInnerView.contentSize = CGSizeMake(self.mainScrollCategoryInnerView.frame.size.width, self.scrollContentCategoryInnerView.frame.size.height)
    }
    
    override func viewDidLayoutSubviews() {
        self.mainScrollCategoryInnerView.contentSize = CGSizeMake(self.mainScrollCategoryInnerView.frame.size.width, self.scrollContentCategoryInnerView.frame.size.height + 10)
        if LANGUAGE == "en"{
            self.companyNameLabel.textAlignment = NSTextAlignment.Left
            self.brandLogo.frame = CGRectMake((self.topHeadingBackView.frame.size.width - self.brandLogo.frame.size.width) - 5, self.brandLogo.frame.origin.y, self.brandLogo.frame.size.width, self.brandLogo.frame.size.height)
            self.companyNameLabel.frame = CGRectMake(8, self.companyNameLabel.frame.origin.y, self.companyNameLabel.frame.size.width, self.companyNameLabel.frame.size.height)
            
            self.priceLabel.textAlignment = NSTextAlignment.Left
            self.dateOfPublishLabel.textAlignment = NSTextAlignment.Left
            self.numberOfViewLabel.textAlignment = NSTextAlignment.Right
            self.descriptionTextView.textAlignment = NSTextAlignment.Left
            
            self.dateOfPublishLabel.frame = CGRectMake(8, self.dateOfPublishLabel.frame.origin.y, self.dateOfPublishLabel.frame.size.width, self.dateOfPublishLabel.frame.size.height)
            self.numberOfViewLabel.frame = CGRectMake((self.intermediateBackView.frame.size.width - self.numberOfViewLabel.frame.size.width) - 8, self.numberOfViewLabel.frame.origin.y, self.numberOfViewLabel.frame.size.width, self.numberOfViewLabel.frame.size.height)
            
        }else{
            self.companyNameLabel.textAlignment = NSTextAlignment.Right
            self.brandLogo.frame = CGRectMake(5, self.brandLogo.frame.origin.y, self.brandLogo.frame.size.width, self.brandLogo.frame.size.height)
            self.companyNameLabel.frame = CGRectMake((self.topHeadingBackView.frame.size.width - self.companyNameLabel.frame.size.width) - 8, self.companyNameLabel.frame.origin.y, self.companyNameLabel.frame.size.width, self.companyNameLabel.frame.size.height)
            
            self.priceLabel.textAlignment = NSTextAlignment.Right
            self.dateOfPublishLabel.textAlignment = NSTextAlignment.Right
            self.numberOfViewLabel.textAlignment = NSTextAlignment.Left
            self.descriptionTextView.textAlignment = NSTextAlignment.Right
            
            self.dateOfPublishLabel.frame = CGRectMake((self.intermediateBackView.frame.size.width - self.dateOfPublishLabel.frame.size.width) - 8, self.dateOfPublishLabel.frame.origin.y, self.dateOfPublishLabel.frame.size.width, self.dateOfPublishLabel.frame.size.height)
            self.numberOfViewLabel.frame = CGRectMake(8, self.numberOfViewLabel.frame.origin.y, self.numberOfViewLabel.frame.size.width, self.numberOfViewLabel.frame.size.height)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(categoryInnerArray)
        
        // Do any additional setup after loading the view.
    }
    
    func postCategoryInner(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "productdetail.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                //                print(Json["CATAGORIES_INNERPAGE_ProductDetails"])
                
                //                print(self.categoryInnerArray[0]["ProductImagelist"])
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let prdctDetails = Json["CATAGORIES_INNERPAGE_ProductDetails"] {
                        print(prdctDetails)
                        //self.categoryInnerArray = prdctDetails as! NSArray
                        self.categoryInnerDictionary = prdctDetails[0] as! NSDictionary
                        print(self.categoryInnerDictionary)
                    }
                    self.loadCategoryData()
                }
            })
        }
    }
    
    func loadCategoryData(){
        
        if (self.categoryInnerDictionary["ProductImagelist"] as? [String])?.count > 0{
            self.productImages = self.categoryInnerDictionary["ProductImagelist"] as! [String]
        }
        
        self.companyNameLabel.text = self.categoryInnerDictionary["CompanyName"]?.uppercaseString
        self.companyNameLabel.textColor = THEME_COLOUR
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
        }else {
            loadingImage = "loading_ar"
        }
        self.brandLogo.sd_setImageWithURL(NSURL(string: (self.categoryInnerDictionary["BrandLogo"] as? String)!), placeholderImage: UIImage(named: loadingImage))
        if productImages.count > 0 {
            if productImages.count == 1 {
                previousButton.hidden = true
                nextButton.hidden = true
            }else {
                previousButton.hidden = false
                nextButton.hidden = false
            }
            self.productImage.sd_setImageWithURL(NSURL(string: (self.categoryInnerDictionary["ProductImagelist"]![0] as? String)!), placeholderImage: UIImage(named: loadingImage))
        }else {
            previousButton.hidden = true
            nextButton.hidden = true
        }
        let price = self.categoryInnerDictionary["Price"] as? String
        self.priceLabel.text = "Price : ".localized(LANGUAGE!) + price! + "KD".localized(LANGUAGE!)
        let DOP = self.categoryInnerDictionary["DataofPublish"] as? String
        self.dateOfPublishLabel.text = "Date of publish :".localized(LANGUAGE!) + DOP!
        self.dateOfPublishLabel.adjustsFontSizeToFitWidth = true
        let NOV = self.categoryInnerDictionary["NumberofViews"] as? String
        self.numberOfViewLabel.text = "No of views : ".localized(LANGUAGE!) + NOV!
        let sampleText = self.categoryInnerDictionary["Description"] as? String
        self.descriptionTextView.text = sampleText
        self.descriptionTextView.textColor = THEME_COLOUR
        self.descriptionTextView.font = UIFont(name: FONT_NAME_R, size: 12)
        //print(self.categoryInnerDictionary["CatagoryName"] as? String)
        self.categoryTitle = self.categoryInnerDictionary["CatagoryName"]!.uppercaseString
        title_Logo_Delegate!.mainTitleChange!(self.categoryTitle)
    }
    
    @IBAction func previous_NextButtonAction(sender: UIButton) {
        //print(productImageCount)
        //print(productImages)
        if sender.tag == 1 {
            if productImageCount > 0 {
                changeHighlightImage(--productImageCount)
            }
        } else {
            if productImageCount < (productImages.count-1) {
                changeHighlightImage(++productImageCount)
            }
        }
    }
    
    func changeHighlightImage(position: Int){
        print(position)
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
        }else {
            loadingImage = "loading_ar"
        }
        self.productImage.sd_setImageWithURL(NSURL(string: productImages[position]), placeholderImage: UIImage(named: loadingImage))
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped left")
                if productImageCount < (productImages.count-1) {
                    changeHighlightImage(++productImageCount)
                }
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.Right:
                print("Swiped Right")
                if productImageCount > 0 {
                    changeHighlightImage(--productImageCount)
                }
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
