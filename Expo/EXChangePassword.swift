//
//  EXChangePassword.swift
//  Expo
//
//  Created by Abdul Malik Aman on 18/05/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXChangePassword: UIViewController {

    // MARK: - Variables
    var titleArray = ["OLD PASSWORD", "NEW PASSWORD", "CONFIRM NEW PASSWORD"]
    var valueArray: [String] = [String](count: 3, repeatedValue: "")
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    @IBOutlet weak var ChangePasswordTable: UITableView!
    @IBOutlet weak var changePasswordContainer: UIView!
    var is_keyboard_up = false
    var table_y: CGFloat!
    
    // MARK: - View Methods
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == true {
            changePasswordContainer.frame.origin.y = 30
        }else {
            changePasswordContainer.frame.origin.y = table_y
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_y = changePasswordContainer.frame.origin.y
        // Do any additional setup after loading the view.
    }

    // MARK: - Actions
    @IBAction func Close(sender: AnyObject) {
        self.view.endEditing(true)
        title_Logo_Delegate?.showChangePassword!(true, container: "ChangePassword")
    }
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    @IBAction func ChangePassword(sender: AnyObject) {
        self.view.endEditing(true)
        validate()
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        title_Logo_Delegate?.showChangePassword!(true, container: "ChangePassword")
    }
    func validate() {
        var message = ""
        if EX_DELEGATE.TrimString(valueArray[0]) == "" {
            message = "Enter current password"
        }else if EX_DELEGATE.TrimString(valueArray[1]) == "" {
            message = "Enter new password"
        }else if EX_DELEGATE.TrimString(valueArray[1]).characters.count < 6 {
            message = "Try one with at least 6 characters"
        }else if EX_DELEGATE.TrimString(valueArray[2]) == "" {
            message = "Enter new password again"
        }else if EX_DELEGATE.TrimString(valueArray[1]) != EX_DELEGATE.TrimString(valueArray[2]) {
            message = "Password does not match"
        }
        if message == "" {
            let params: NSDictionary = ["lang_key": LANGUAGE!, "User_Id": USER_ID!, "OldPassword": valueArray[0], "NewPassword": valueArray[1]]
            postChangePassword(params)
        }else {
            EX_DELEGATE.showAlert("Message", message: message.localized(LANGUAGE!), buttonTitle: "OK")
        }
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = "more"
        if indexPath.row != 3 {
            identifier = "text"
        }else {
            identifier = "button"
        }
        let cell = ChangePasswordTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXChangePasswordCell
        
        if indexPath.row != 3 {
            cell.passwordText.tag = indexPath.row
            cell.passwordText.text = valueArray[indexPath.row]
            cell.placeHolder.text = titleArray[indexPath.row]
        }
        
        
        return cell
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row != 3 {
            return 80
        }else {
            return 58
        }
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        ChangePasswordTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        is_keyboard_up = true
        scrollToTop(textField.tag, section: 0, table: ChangePasswordTable)
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        is_keyboard_up = false
        valueArray[textField.tag] = textField.text!
        viewDidLayoutSubviews()
    }
    
    // MARK: - Web Services
    func postChangePassword(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "change_password.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let account = Json["MyAccount"] {
                        EX_DELEGATE.showAlert("Message", message: account["Message"] as! String, buttonTitle: "OK")
                        self.title_Logo_Delegate?.showChangePassword!(true, container: "ChangePassword")
                    }
                }
            })
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXChangePasswordCell: UITableViewCell {
    
    @IBOutlet weak var placeHolder: EXLabel!
    @IBOutlet weak var passwordText: EXTextField!
    
}
