//
//  EXButton.swift
//  Expo
//
//  Created by Abdul Malik Aman on 26/04/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXButton: UIButton {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
//        self.layer.cornerRadius = 8.0;
//        self.backgroundColor = THEME_COLOUR
//        self.tintColor = UIColor.whiteColor()
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        let fontSize = (self.titleLabel?.font.pointSize)! // -2.0
        self.titleLabel?.font = UIFont(name: FONT_NAME, size: fontSize)
        self.sizeToFit()
    }

}
