//
//  EXEditProfile.swift
//  Expo
//
//  Created by Abdul Malik Aman on 18/05/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit
protocol editProfileDelegate {
    func reloadEditProfileView()
}
class EXEditProfile: UIViewController, UIPopoverPresentationControllerDelegate, countryChangeDelegate, editProfileDelegate {

    // MARK: - Variables
    var delegate: editProfileDelegate?
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    @IBOutlet weak var MyAccountTable: UITableView!
    var myAccountDetails: NSDictionary!
    var valueArray: [String] = [String](count: 6, repeatedValue: "")
    var is_keyboard_up = false
    var table_y: CGFloat!
    var table_h: CGFloat!
    @IBOutlet weak var EditProfileContainer: UIView!
    
    // MARK: - View Methods
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == true {
            MyAccountTable.frame.origin.y = 40
            MyAccountTable.frame.size.height = self.view.frame.height - (KEYBOARD_FRAME + 40)
        }else {
            MyAccountTable.frame.origin.y = table_y
            MyAccountTable.frame.size.height = table_h
        }
    }
    override func viewWillAppear(animated: Bool) {
        if myAccountDetails == nil {
            let params: NSDictionary  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
            postMyAccount(params)
        }else {
            MyAccountTable.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        table_y = MyAccountTable.frame.origin.y
        table_h = MyAccountTable.frame.size.height
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    @IBAction func Close(sender: AnyObject) {
        self.view.endEditing(true)
        title_Logo_Delegate?.showChangePassword!(true, container: "Profile")
    }
    
    @IBAction func EditProfile(sender: AnyObject) {
        self.view.endEditing(true)
        let params = ["lang_key": LANGUAGE!, "User_Id": USER_ID!, "Name": valueArray[0], "Email": valueArray[1], "Country": valueArray[2], "City": valueArray[3], "Address": valueArray[4], "Mobile": valueArray[5]]
        postEditProfile(params)
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        title_Logo_Delegate?.showChangePassword!(true, container: "Profile")
    }
    func showChangeCountry(myView: UITextField) {
        let changeCountry: EXRegistrationPopUp = storyboard!.instantiateViewControllerWithIdentifier("EXRegistrationPopUp") as! EXRegistrationPopUp
        changeCountry.delegate = self
        changeCountry.modalPresentationStyle = .Popover
        changeCountry.preferredContentSize = CGSizeMake(200, 200)
        let popoverMenuViewController = changeCountry.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = view
        popoverMenuViewController?.sourceRect = CGRect(
            x: self.view.frame.width / 2 ,
            y: self.view.frame.height / 2,
            width: 1,
            height: 1)
        presentViewController(
            changeCountry,
            animated: true,
            completion: nil)
        
    }
    func reloadEditProfileView() {
        self.viewDidLoad()
    }
    

    // MARK: - TableView Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if myAccountDetails != nil {
            return 1
        }else {
            return 0
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var identifier = "placeHolder"
        if indexPath.row == 0 {
            identifier = "personal"
        }else if indexPath.row == 7 {
            identifier = "buttons"
        }else {
            identifier = "placeHolder"
        }
        
        let cell = MyAccountTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXMyAccountCell
        if indexPath.row != 0 && indexPath.row != 7{
            cell.editProfile.tag = indexPath.row - 1
            let dictKeys: [String] = ["Name", "Email", "Country", "City", "Address", "Mobile"]
            if let profileData = myAccountDetails.objectForKey(dictKeys[indexPath.row - 1]) {
                if profileData as? String != "" {
                    cell.editProfile.text = valueArray[indexPath.row - 1] // myAccountDetails.objectForKey(dictKeys[indexPath.row]) as? String
                }else {
                    cell.editProfile.placeholder = dictKeys[indexPath.row - 1]
                }
            }
            
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row != 6 {
            return 56
        }else {
            return 50
        }
    }
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        MyAccountTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        is_keyboard_up = true
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        is_keyboard_up = false
        valueArray[textField.tag] = textField.text!
        viewDidLayoutSubviews()
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField.tag == 2 {
            textField.endEditing(true)
            textField.resignFirstResponder()
            //            changeCountry(textField)
            showChangeCountry(textField)
            return false
        }else {
            return true
        }
    }
    
    // MARK: - Custom Delegates
    func changeCountry(countryName: String, countryId: String) {
        valueArray[2] = countryName
        MyAccountTable.reloadData()
    }
    
    // MARK: - Web Services
    func postMyAccount(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "myaccount.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let myAccount = Json["MyAccount"] {
                        self.myAccountDetails = myAccount as! NSDictionary
                        ["Name", "Email", "Country", "City", "Address", "Mobile"]
                        self.valueArray[0] = myAccount["Name"] as! String
                        self.valueArray[1] = myAccount["Email"] as! String
                        self.valueArray[2] = myAccount["Country"] as! String
                        self.valueArray[3] = myAccount["City"] as! String
                        self.valueArray[4] = myAccount["Address"] as! String
                        self.valueArray[5] = myAccount["Mobile"] as! String
                        
                        self.MyAccountTable.reloadData()
                    }
                    
                }
            })
        }
    }
    func postEditProfile(params: NSDictionary) {
        print(params)
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "myaccount_update.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let account = Json["MyAccount"] {
                        EX_DELEGATE.showAlert("Message", message: account["Message"] as! String, buttonTitle: "OK")
                        self.title_Logo_Delegate?.showChangePassword!(true, container: "Profile")
                    }
                }else {
                    EX_DELEGATE.showAlert("Message", message: "Failed to Update Profile".localized(LANGUAGE!), buttonTitle: "OK")
                    
                }
            })
        }
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
