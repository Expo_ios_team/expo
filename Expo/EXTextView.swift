//
//  EXTextView.swift
//  Expo
//
//  Created by Ronish on 5/9/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXTextView: UITextView {


    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.setup()
    }
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        self.text = self.text
        self.textColor = self.textColor
        self.font = self.font
        self.layer.display()
        let fontSize = self.font!.pointSize;
        self.font = UIFont(name: FONT_NAME, size: fontSize)
    }

}
