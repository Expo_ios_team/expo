//
//  EXRootContainer.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

@objc protocol Change_Title_Logo_Delegate {
    optional func mainTitleChange(title: String)
    optional func ChangeLogoImage(isHide: Bool)
    optional func HideBackButton(isHide: Bool)
    optional func navigationBack(navi: UINavigationController, tag: Int)
    optional func showChangePassword(isHide: Bool, container: String)
    optional func HomeSetSelected(isSelected: Bool)
    optional func reloadProfileView()
}

class EXRootContainer: UIViewController, Change_Title_Logo_Delegate {

    // MARK: - Variables
    var titleArray = ["LANGUAGE", "SEARCH", "SETTINGS", "LOGIN", "SHOPPING CART", "ORDER HISTORY", "NOTIFICATIONS", "TERMS & CONDITIONS", "CONTACT US", "SHARE THE APP"]
    var imagesArray = ["languageIcon", "search", "settings", "login", "shopping_cart", "order_history", "notifications", "termsnconditions", "contactus", "sharetheapp"]
    
    var profileDelegate: editProfileDelegate?
    var editProfile = EXEditProfile()
    @IBOutlet weak var profileEditContainer: UIView!
    @IBOutlet weak var changePasswordContainer: UIView!
    @IBOutlet var moreMenuTable: UITableView!
    var rootContanerView = EXRootTab()
    @IBOutlet weak var menuSlideBackView: UIView!
    @IBOutlet var tabButtons: [UIButton]!
    @IBOutlet weak var moreMenuButton: UIButton!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var topLogoSmall: UIImageView!
    @IBOutlet var seperatorForSmallLogo: UIImageView!
    @IBOutlet var topLogoBig: UIImageView!
    @IBOutlet var seperatorForBigLogo: UIImageView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet weak var masterContainer: UIView!
    @IBOutlet weak var masterTabBar: UIView!
    @IBOutlet weak var moreLabel: UILabel!
    
    var loadingView: UIView = UIView()
    var actView: UIView = UIView()
    var activityLabel: UILabel = UILabel()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var navigationControllerPointer = UINavigationController()
    var flagForLanguageSwitching : Int = 0
    
    // Frame variable
    
    var moreMenuButtonFrame : CGRect?
    var backButtonFrame : CGRect?
    var tabButtonFrame1 : CGRect?
    var tabButtonFrame2 : CGRect?
    var tabButtonFrame3 : CGRect?
    var tabButtonFrame4 : CGRect?
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
//        rootContanerView.selectedIndex = 4
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapToRemoveMenu = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        tapToRemoveMenu.direction = UISwipeGestureRecognizerDirection.Right
        self.transparentView.addGestureRecognizer(tapToRemoveMenu)
        
        let touchToRemoveMenu = UITapGestureRecognizer(target: self, action: "respondToTouchEvent:")
        self.transparentView.addGestureRecognizer(touchToRemoveMenu)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
        rightSwipe.direction = .Right
        self.view.addGestureRecognizer(rightSwipe)
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: Selector("handleSwipes:"))
        leftSwipe.direction = .Left
        self.view.addGestureRecognizer(leftSwipe)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
        self.moreMenuButtonFrame = self.moreMenuButton.frame
        self.backButtonFrame = self.backButton.frame
        if self.flagForLanguageSwitching == 0 {
            self.tabButtonFrame1 = self.tabButtons[0].frame
            self.tabButtonFrame2 = self.tabButtons[1].frame
            self.tabButtonFrame3 = self.tabButtons[2].frame
            self.tabButtonFrame4 = self.tabButtons[3].frame
        }
        
        print(LANGUAGE)
        if LANGUAGE == "en"{
            self.moreMenuButton.frame = CGRectMake((self.view.frame.width - self.moreMenuButtonFrame!.size.width)-9, (self.moreMenuButtonFrame?.origin.y)!, (self.moreMenuButtonFrame?.size.width)!, (self.moreMenuButtonFrame?.size.height)!)
            self.backButton.frame = CGRectMake(12, (self.backButtonFrame?.origin.y)!, (self.backButtonFrame?.size.width)!, (self.backButtonFrame?.size.height)!)
            self.menuSlideBackView.frame = CGRectMake(self.view.frame.size.width, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
            self.tabButtons[0].frame = self.tabButtonFrame1!
            self.tabButtons[1].frame = self.tabButtonFrame2!
            self.tabButtons[2].frame = self.tabButtonFrame3!
            self.tabButtons[3].frame = self.tabButtonFrame4!
        }else {
            self.moreMenuButton.frame = CGRectMake(9, (self.moreMenuButtonFrame?.origin.y)!, (self.moreMenuButtonFrame?.size.width)!, (self.moreMenuButtonFrame?.size.height)!)
            self.backButton.frame = CGRectMake((self.view.frame.width - self.backButtonFrame!.size.width)-12, (self.backButtonFrame?.origin.y)!, (self.backButtonFrame?.size.width)!, (self.backButtonFrame?.size.height)!)
            self.menuSlideBackView.frame = CGRectMake(-self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
            self.tabButtons[0].frame = self.tabButtonFrame4!
            self.tabButtons[1].frame = self.tabButtonFrame3!
            self.tabButtons[2].frame = self.tabButtonFrame2!
            self.tabButtons[3].frame = self.tabButtonFrame1!
        }
    }
    
   // MARK: - Actions
    
    @IBAction func tabButtonAction(sender: UIButton) {
        self.flagForLanguageSwitching = 1
        for tabButton in self.tabButtons {
            tabButton.selected = false
            if sender.tag == tabButton.tag {
                tabButton.selected = true
            }
        }
        showMoreMenu(true)
        self.navigationControllerPointer.popToRootViewControllerAnimated(false)
        rootContanerView.selectedIndex = sender.tag
//        self.topLogoBig.hidden = true
//        self.seperatorForBigLogo.hidden = true
//        switch sender.tag {
//        case 0 :
//            self.titleLabel.hidden = true
//            self.topLogoSmall.hidden = true
//            self.seperatorForSmallLogo.hidden = true
//            self.topLogoBig.hidden = false
//            self.seperatorForBigLogo.hidden = false
//        case 1 :
//            self.titleLabel.hidden = false
//            self.titleLabel.text = "CATEGORIES"
//            self.topLogoSmall.hidden = false
//            self.seperatorForSmallLogo.hidden = false
//        case 2 :
//            self.titleLabel.hidden = false
//            self.titleLabel.text = "COMPANY LIST"
//            self.topLogoSmall.hidden = false
//            self.seperatorForSmallLogo.hidden = false
//        case 3 :
//            self.titleLabel.hidden = false
//            self.titleLabel.text = "SHOPPING CART"
//            self.topLogoSmall.hidden = false
//            self.seperatorForSmallLogo.hidden = false
//        default:
//            print("Default case")
//        }
    }
    
    func handleSwipes(sender:UISwipeGestureRecognizer) {
        if LANGUAGE == "en" {
            if (sender.direction == .Left) {
                
                print("Swipe Left")
                self .moreMenuButtonAction(self.moreMenuButton)
            }
        }else {
            if (sender.direction == .Right) {
                
                print("Swipe Right")
                self .moreMenuButtonAction(self.moreMenuButton)
            }
        }
        
    }
    
    @IBAction func backButtonAction(sender: UIButton) {
        //self.tabButtonAction(sender)
        //rootContanerView.selectedIndex = 4
        if sender.tag == 0{
            rootContanerView.selectedIndex = 0
        }
        else if sender.tag == 2{
            rootContanerView.selectedIndex = 3
        }else{
            self.navigationControllerPointer.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func moreMenuButtonAction(sender: UIButton) {
        if sender.tag == 0{
            showMoreMenu(false)
        }else {
            showMoreMenu(true)
        }
        moreMenuTable.reloadData()
    }
    
    
    
    func showMoreMenu(selected: Bool) {
        self.view.endEditing(true)
        if selected {
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                if LANGUAGE == "en"{
                    self.menuSlideBackView.frame = CGRectMake(self.view.frame.size.width, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
                }else {
                    self.menuSlideBackView.frame = CGRectMake(-self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
                }
                self.menuSlideBackView.updateConstraints()
                self.transparentView.alpha = 0
                }, completion: { (finished: Bool) -> Void in
                    self.moreMenuButton.selected = false
            })
        } else {
            UIView.animateWithDuration(0.5, delay: 0.0, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
                if LANGUAGE == "en"{
                    self.menuSlideBackView.frame = CGRectMake(self.view.frame.size.width - self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
                }else {
                    self.menuSlideBackView.frame = CGRectMake(0, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
                }
                self.menuSlideBackView.updateConstraints()
                self.transparentView.alpha = 1
                }, completion: { (finished: Bool) -> Void in
                    self.moreMenuButton.selected = true
            })
        }
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Right:
                showMoreMenu(true)
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func respondToTouchEvent(gesture : UITapGestureRecognizer){
        showMoreMenu(true)
    }

    // MARK: - Custom Delegates
    func mainTitleChange(title: String) {
        titleLabel.text = title.uppercaseString
    }
    
    func ChangeLogoImage(isHide: Bool) {
        if isHide{
            self.titleLabel.hidden = true
            self.topLogoSmall.hidden = true
            self.seperatorForSmallLogo.hidden = true
            self.topLogoBig.hidden = false
            self.seperatorForBigLogo.hidden = false
        }else {
            self.titleLabel.hidden = false
            self.topLogoSmall.hidden = false
            self.seperatorForSmallLogo.hidden = false
            self.topLogoBig.hidden = true
            self.seperatorForBigLogo.hidden = true
        }
    }
    
    func HideBackButton(isHIde: Bool){
        if isHIde {
            self.backButton.hidden = true
        } else {
            self.backButton.hidden = false
        }
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        self.navigationControllerPointer = navi
        self.backButton.tag = tag
    }
    func showChangePassword(isHide: Bool, container: String) {
        if container == "Profile" {
            self.profileEditContainer.hidden = isHide
        }else {
            self.changePasswordContainer.hidden = isHide
        }
    }
    
    func HomeSetSelected(isSelected: Bool){
        for tabButton in self.tabButtons {
            tabButton.selected = false
            if tabButton.tag == 0 {
                tabButton.selected = true
            }
        }
    }
    func reloadProfileView() {
        profileDelegate?.reloadEditProfileView()
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = "more"
        if LANGUAGE == "en" {
            identifier = "more"
        }else {
            identifier = "more_ar"
        }
        let cell = moreMenuTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXMoreCell
        
        cell.moreImage.image = UIImage(named: imagesArray[indexPath.row])
        cell.moreTitle.text = titleArray[indexPath.row].localized(LANGUAGE!)
        
        if IS_GUEST == false {
            if indexPath.row == 3 {
                cell.moreTitle.text = "MY ACCOUNT".localized(LANGUAGE!)
            }
        }else {
            if indexPath.row == 3 {
                cell.moreTitle.text = "LOGIN".localized(LANGUAGE!)
            }
        }
        cell.moreTitle.adjustsFontSizeToFitWidth = true
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        for tabButton in self.tabButtons {
            tabButton.selected = false
        }
        showMoreMenu(true)
        switch indexPath.row {
        case 0 :
            print("Language")
            
         //   /*  commented for testing
            
            // create the alert
            let alert = UIAlertController(title: "Change Language", message: "Would you like to change language", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.Default, handler: { action in
                if LANGUAGE == "en"{
                    LANGUAGE = "ar"
                    EX_DEFAULTS.setValue("ar", forKey: "Language")
                }else{
                    LANGUAGE = "en"
                    EX_DEFAULTS.setValue("en", forKey: "Language")
                }
                //print(LANGUAGE!)
                self.moreLabel.text = "MORE".localized(LANGUAGE!)
                self.navigationControllerPointer.popToRootViewControllerAnimated(false)
                IS_LANGUAGE_SWITCHED = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                self.rootContanerView.selectedIndex = 10
                self.rootContanerView.selectedIndex = 0
                self.flagForLanguageSwitching = 1
                //self.viewDidLoad()
                if LANGUAGE == "en"{
                    self.moreMenuButton.frame = CGRectMake((self.view.frame.width - self.moreMenuButtonFrame!.size.width)-9, (self.moreMenuButtonFrame?.origin.y)!, (self.moreMenuButtonFrame?.size.width)!, (self.moreMenuButtonFrame?.size.height)!)
                    self.backButton.frame = CGRectMake(12, (self.backButtonFrame?.origin.y)!, (self.backButtonFrame?.size.width)!, (self.backButtonFrame?.size.height)!)
                    self.menuSlideBackView.frame = CGRectMake(self.view.frame.size.width, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
                    self.tabButtons[0].frame = self.tabButtonFrame1!
                    self.tabButtons[1].frame = self.tabButtonFrame2!
                    self.tabButtons[2].frame = self.tabButtonFrame3!
                    self.tabButtons[3].frame = self.tabButtonFrame4!
                }else {
                    self.moreMenuButton.frame = CGRectMake(9, (self.moreMenuButtonFrame?.origin.y)!, (self.moreMenuButtonFrame?.size.width)!, (self.moreMenuButtonFrame?.size.height)!)
                    self.backButton.frame = CGRectMake((self.view.frame.width - self.backButtonFrame!.size.width)-12, (self.backButtonFrame?.origin.y)!, (self.backButtonFrame?.size.width)!, (self.backButtonFrame?.size.height)!)
                    self.menuSlideBackView.frame = CGRectMake(-self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.origin.y, self.menuSlideBackView.frame.size.width, self.menuSlideBackView.frame.size.height)
                    self.tabButtons[0].frame = self.tabButtonFrame4!
                    self.tabButtons[1].frame = self.tabButtonFrame3!
                    self.tabButtons[2].frame = self.tabButtonFrame2!
                    self.tabButtons[3].frame = self.tabButtonFrame1!
                }
                
                for tabButton in self.tabButtons {
                    tabButton.selected = false
                    if tabButton.tag == 0 {
                        tabButton.selected = true
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            self.viewDidLoad()
        case 1 :
            // search page
            rootContanerView.selectedIndex = 6
        case 2 :
            // settings
            rootContanerView.selectedIndex = 7
        case 3 :
            // login
            if IS_GUEST == false {
                rootContanerView.selectedIndex = 13
            }else {
                EX_DEFAULTS.setValue(true, forKey: "Login")
                APPDELEGATE?.SessionExpare(true)
            }
        case 4 :
            // Shopping Cart
            rootContanerView.selectedIndex = 14
        case 5 :
            // order history
            rootContanerView.selectedIndex = 8
        case 6 :
            // notifications
            rootContanerView.selectedIndex = 9
        case 7 :
            // terms and condition
            rootContanerView.selectedIndex = 10
        case 8 :
            // contact us
            rootContanerView.selectedIndex = 11
        case 9 :
            // share the app
            rootContanerView.selectedIndex = 12
        default:
            print("Default Case")
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "rootTab" {
            rootContanerView = segue.destinationViewController as! EXRootTab
            let login = rootContanerView.viewControllers![4] as! EXLogin
            login.title_Logo_Delegate = self
            
            let register = rootContanerView.viewControllers![5] as! EXRegistration
            register.title_Logo_Delegate = self
            
            let homeNavigation = rootContanerView.viewControllers![0] as! UINavigationController
            let home = homeNavigation.viewControllers[0] as! EXHome
            home.title_Logo_Delegate = self
            
            let catagoryNavigation = rootContanerView.viewControllers![1] as! UINavigationController
            let catagory = catagoryNavigation.viewControllers[0] as! EXCategoryList
            catagory.title_Logo_Delegate = self
            
            let companyNavigation = rootContanerView.viewControllers![2] as! UINavigationController
            let company = companyNavigation.viewControllers[0] as! EXCompanyList
            company.title_Logo_Delegate = self
            
            let productNavigation = rootContanerView.viewControllers![3] as! UINavigationController
            let product = productNavigation.viewControllers[0] as! EXStoreList
            product.title_Logo_Delegate = self
            
            let searchNavigation = rootContanerView.viewControllers![6] as! UINavigationController
            let search = searchNavigation.viewControllers[0] as! EXSearch
            search.title_Logo_Delegate = self
            
            let settings = rootContanerView.viewControllers![7] as! EXSettings
            settings.title_Logo_Delegate = self
            
            let orderHistoryNavigation = rootContanerView.viewControllers![8] as! UINavigationController
            let orderHistory = orderHistoryNavigation.viewControllers[0] as! EXOrderHistory
            orderHistory.title_Logo_Delegate = self
            
            let notificationNavigation = rootContanerView.viewControllers![9] as! UINavigationController
            let notification = notificationNavigation.viewControllers[0] as! EXNotifications
            notification.title_Logo_Delegate = self
            
            let terms = rootContanerView.viewControllers![10] as! EXTermsAndConditions
            terms.title_Logo_Delegate = self
            
            let contactUs = rootContanerView.viewControllers![11] as! EXContactUs
            contactUs.title_Logo_Delegate = self
            
            let share = rootContanerView.viewControllers![12] as! EXShareTheApp
            share.title_Logo_Delegate = self
            
            let myAccount = rootContanerView.viewControllers![13] as! EXMyAccount
            myAccount.title_Logo_Delegate = self
            
            let shoppingNavigation = rootContanerView.viewControllers![14] as! UINavigationController
            let shoppingCart = shoppingNavigation.viewControllers[0] as! EXShoppingCart
            shoppingCart.title_Logo_Delegate = self
            
        }else if segue.identifier == "changePwd" {
            let changePwd = segue.destinationViewController as! EXChangePassword
            changePwd.title_Logo_Delegate = self
        }else if segue.identifier == "editProfile" {
            editProfile = segue.destinationViewController as! EXEditProfile
            editProfile.title_Logo_Delegate = self
//            editProfile.delegate = self
        }
    }

}
class EXMoreCell: UITableViewCell {
    
    @IBOutlet weak var moreImage: UIImageView!
    @IBOutlet weak var moreTitle: EXLabel!
    
}
