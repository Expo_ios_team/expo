//
//  EXCategoryDetails.swift
//  Expo
//
//  Created by Ronish on 5/2/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXCategoryDetails: UIViewController, UITableViewDelegate, UITableViewDataSource, Change_Title_Logo_Delegate {
    
    // MARK: - Variables
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var categoryID : String = ""
    var categoryTitle : String = ""
    var categoryInnerArray: NSDictionary!
    var categoryDict: NSDictionary!
    var productList: NSArray!
    var CategoryInner = EXCategoryInner()
    var dropDownTable: UITableView!
    var is_dropDownShow = false
    var brandNameArray: [String] = []
    var dropDownArray: NSArray!
    var productKey = ""
    var cellCount = 0
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var dropDownView: UIView!
    
    @IBOutlet var tittleLabel: EXLabel!
    
    @IBOutlet var dropDownButton: EXButton!
    @IBOutlet var spaceAudjustingView: UIView!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        
        title_Logo_Delegate!.mainTitleChange!("")
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        
        if productList == nil {
            if categoryID != "" {
                let params: NSDictionary = ["CatagoryId": "\(categoryID)", "lang_key": LANGUAGE!]
                postCategoryInner(params)
            }else {
                categoryCollection.reloadData()
            }
        }else {
            categoryCollection.reloadData()
        }
    }
    
    override func viewDidLayoutSubviews() {
        print("hai")
        if LANGUAGE == "en"{
            self.tittleLabel.textAlignment = NSTextAlignment.Left
            self.tittleLabel.frame = CGRectMake(0, self.tittleLabel.frame.origin.y, self.tittleLabel.frame.size.width, self.tittleLabel.frame.size.height)
            self.dropDownButton.frame = CGRectMake(self.spaceAudjustingView.frame.size.width - self.dropDownButton.frame.size.width, self.dropDownButton.frame.origin.y, self.dropDownButton.frame.size.width, self.dropDownButton.frame.size.height)
            self.dropDownView.frame = CGRectMake(self.spaceAudjustingView.frame.size.width - self.dropDownView.frame.size.width, self.dropDownView.frame.origin.y, self.dropDownView.frame.size.width, self.dropDownView.frame.size.height)
        }else{
            self.tittleLabel.textAlignment = NSTextAlignment.Right
            self.tittleLabel.frame = CGRectMake(self.spaceAudjustingView.frame.size.width - self.tittleLabel.frame.size.width, self.tittleLabel.frame.origin.y, self.tittleLabel.frame.size.width, self.tittleLabel.frame.size.height)
            self.dropDownButton.frame = CGRectMake(0, self.dropDownButton.frame.origin.y, self.dropDownButton.frame.size.width, self.dropDownButton.frame.size.height)
            self.dropDownView.frame = CGRectMake(self.spaceAudjustingView.frame.origin.x, self.dropDownView.frame.origin.y, self.dropDownView.frame.size.width, self.dropDownView.frame.size.height)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        tittleLabel.text = categoryTitle
        tittleLabel.adjustsFontSizeToFitWidth = true
        
        dropDownTable = UITableView(frame: CGRectMake(0, 0, 180, 150))
        dropDownTable.delegate = self
        dropDownTable.dataSource = self
        dropDownTable.rowHeight = 30
        dropDownTable.layer.borderColor = THEME_COLOUR.CGColor
        dropDownTable.layer.borderWidth = 3
        dropDownTable.separatorColor = UIColor.clearColor()
        dropDownTable.backgroundColor = UIColor.clearColor()
        dropDownTable.scrollEnabled = true
        dropDownTable.registerClass(EXDropDownCell.self, forCellReuseIdentifier: "dropDown")
        dropDownView.addSubview(dropDownTable)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    func showDropDownTable() {
        
        if categoryInnerArray != nil {
            if categoryInnerArray.count > 0 {
                UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: { () -> Void in
                    
                    if self.is_dropDownShow == false {
                        //                self.dropDownView.frame = CGRectMake(self.view.frame.size.width - 142, 67, self.dropDownView.frame.size.width, 140)
                        self.dropDownView.frame.size.height = CGFloat(self.dropDownArray.count * 30)
//                        self.dropDownTable.frame = CGRectMake(0, 0, 120, 150)
                        //                self.dropDownTable.frame = CGRectMake(0, 0, self.dropDownView.frame.size.width, 140)
                        
                    }else {
                        //                self.dropDownView.frame = CGRectMake(self.view.frame.size.width - 142, 67, self.dropDownView.frame.size.width, 0)
                        self.dropDownView.frame.size.height = 0
//                        self.dropDownTable.frame = CGRectMake(0, 0, 120, 0)
                        
                    }
                    }) { (completed: Bool) -> Void in
                        if self.is_dropDownShow == false {
                            self.is_dropDownShow = true
                        }else {
                            self.is_dropDownShow = false
                        }
                }
            }
        }
        
    }
    @IBAction func showDropDown(sender: AnyObject) {
        showDropDownTable()
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }

    
    // MARK: - CollectionView Delegate Methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if cellCount != 0 {
            return 1
        }else {
            return 0
        }
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if productList != nil {
            if productList.count > 0 {
                return productList.count
            }else {
                return 0
            }
        }else {
            return 0
        }
        
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = categoryCollection.dequeueReusableCellWithReuseIdentifier("category", forIndexPath: indexPath) as! EXCategoryDetailCell
        if LANGUAGE == "en"{
            cell.categoryDescription.textAlignment = NSTextAlignment.Left
        }else{
            cell.categoryDescription.textAlignment = NSTextAlignment.Right
        }
        cell.categoryDescription.text = productList[indexPath.row]["Title"] as? String
        let ImageList = nullToNil(productList[indexPath.row]["Image"])
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
        }else {
            loadingImage = "loading_ar"
        }
        if ImageList != nil {
            cell.categoryImage.sd_setImageWithURL(NSURL(string: ImageList as! String), placeholderImage: UIImage(named: loadingImage))
        }
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("Category_Inner", sender: indexPath)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let viewWidth = self.categoryCollection.frame.width
        let collectionSize = viewWidth / 2 - 10
        return CGSizeMake(collectionSize , collectionSize)
    }
    func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dropDownArray != nil {
            if dropDownArray.count > 0 {
                return dropDownArray.count
            }else {
                return 0
            }
        }else {
            return 0
        }
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = dropDownTable.dequeueReusableCellWithIdentifier("dropDown", forIndexPath: indexPath) as! EXDropDownCell
        cell.userInteractionEnabled = true
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        let seperator = UILabel(frame: CGRectMake(0, 0, cell.contentView.frame.size.width, 1))
        seperator.backgroundColor = THEME_COLOUR
        seperator.text = ""
        cell.contentView.addSubview(seperator)
        
        let dropDownText = EXLabel(frame: CGRectMake(7, cell.contentView.frame.size.height / 2 - 12, cell.contentView.frame.size.width - 10, 24))
        dropDownText.font = UIFont(name: FONT_NAME, size: 13)
        cell.contentView.addSubview(dropDownText)
        let companyName = dropDownArray[indexPath.row]["CompanyName"] as? String
        dropDownText.text = companyName?.capitalizedString
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        productKey = (dropDownArray[indexPath.row]["CompanyName"] as? String)!
        dropDownButton.setTitle(productKey.capitalizedString, forState: UIControlState.Normal)
        showDropDownTable()
        productList = categoryInnerArray[productKey] as? NSArray
        cellCount = productList.count
        categoryCollection.reloadData()
    }
    
    
    // MARK: - Web Services
    func postCategoryInner(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "categoriesinner.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
//                print(Json["CATAGORIES_INNERPAGE_ProductDetails"])
                
                //                print(self.categoryInnerArray[0]["ProductImagelist"])
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let prdctDetails = Json["CATAGORIES_INNERPAGE"] {
                        self.categoryDict = prdctDetails[0] as? NSDictionary
                        let companiesList = self.nullToNil(prdctDetails[0]["CompanyListforDropdown"])
                        if companiesList != nil{
                            self.dropDownArray = companiesList as! NSArray
                            
                            if self.dropDownArray != nil {
                                if self.dropDownArray.count > 0 {
                                    self.productKey = (self.dropDownArray[0]["CompanyName"] as? String)!
                                    self.dropDownTable.frame.size.height = CGFloat(self.dropDownArray.count * 30)
                                    if self.dropDownArray.count < 6 {
                                        self.dropDownView.backgroundColor = UIColor.clearColor()
                                        self.dropDownTable.scrollEnabled = false
                                    }else {
                                        self.dropDownView.backgroundColor = UIColor.whiteColor()
                                        self.dropDownTable.scrollEnabled = true
                                    }
                                }
                            }
                            
                            self.dropDownButton.setTitle(self.productKey.capitalizedString, forState: UIControlState.Normal)
                        }else {
                            EX_DELEGATE.showAlert("Message", message: "No items in selected category".localized(LANGUAGE!), buttonTitle: "OK")
                            self.dropDownButton.setTitle("", forState: UIControlState.Normal)
                        }
                        
                        
                        if let inner = self.nullToNil(prdctDetails[0]["ProductList"]) {
                            if inner.count > 0 {
                                self.categoryInnerArray = inner as? NSDictionary
                                self.productList = self.categoryInnerArray[self.productKey] as? NSArray
                                self.cellCount = self.productList.count
                                self.categoryInnerArray = inner as! NSDictionary
                            }
                        }
                        
                    }
                    self.categoryCollection.reloadData()
                    self.dropDownTable.reloadData()
                }
            })
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "Category_Inner"{
            CategoryInner = segue.destinationViewController as! EXCategoryInner
            let row = (sender as! NSIndexPath).row
            CategoryInner.productId = productList[row]["ID"] as? String
            CategoryInner.categoryTitle = self.categoryTitle.uppercaseString
            CategoryInner.title_Logo_Delegate = self
        }
    }
    
    
}
class EXCategoryDetailCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryDescription: EXLabel!
    
}
class EXDropDownCell: UITableViewCell {
    
    @IBOutlet weak var dropDownText: EXLabel!
    
}
