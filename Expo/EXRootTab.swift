//
//  EXRootTab.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXRootTab: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.hidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
