//
//  EXConstants.swift
//  Expo
//
//  Created by Abdul Malik Aman on 26/04/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

let App_Version = "1.0.0"
let APPDELEGATE = UIApplication.sharedApplication().delegate as? AppDelegate
var WEB_SERVICE = "http://expo.mawaqaatest.com/"
var LANGUAGE = EX_DEFAULTS.stringForKey("Language")
var USER_ID = EX_DEFAULTS.stringForKey("User_Id")
let THEME_COLOUR = UIColor(red: 0.7843, green: 0.1765, blue: 0.2, alpha: 1.0) /* #c82d33 */
var IS_LANGUAGE_SWITCHED = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var IS_CART_UPDATED = [0, 0]

// MARK:- Public Variables
let EX_DELEGATE: EXCustomDelegates = EXCustomDelegates()
let EX_Network: EXNetwork = EXNetwork()
var IS_SESSION_EXPIRED = EX_DEFAULTS.boolForKey("Login")
let FONT_NAME = "Raleway-Light" // "Raleway-Extralight"
let FONT_NAME_B = "Raleway-Bold"
let FONT_NAME_R = "Raleway-Medium"
let EX_DEFAULTS = NSUserDefaults.standardUserDefaults()
var IS_GUEST = EX_DEFAULTS.boolForKey("isGuest")
var COUNTRIES = EX_DEFAULTS.arrayForKey("CountryLoaded")
let EX_DATE_FORMATTER = NSDateFormatter()
var IS_REACHABLE: Bool!
var KEYBOARD_FRAME: CGFloat = 216.0
let Device = UIDevice.currentDevice()
var iPad =  (Device.userInterfaceIdiom == UIUserInterfaceIdiom.Pad)
var iPhone = (Device.userInterfaceIdiom == UIUserInterfaceIdiom.Phone)
var bounds: CGRect = UIScreen.mainScreen().bounds
var SCREEN_HEIGHT:CGFloat = bounds.size.height
var IS_ACTIVITY_SHOWN = false
var MESSAGE_ID = EX_DEFAULTS.valueForKey("MessageID")
var DEVICE_ID = EX_DEFAULTS.valueForKey("Device_Id")

// MARK: - Google
let GOOGLE_CLIENT_ID = "676108000881-11r6rqkl69notgo4jd9hs01579ne8jbt.apps.googleusercontent.com"
