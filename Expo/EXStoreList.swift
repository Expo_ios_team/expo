//
//  EXProductList.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXStoreList: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,Change_Title_Logo_Delegate {

    @IBOutlet var storeCollection: UICollectionView!
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    @IBOutlet var containerView: UIView!
    @IBOutlet var cartCountLabel: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var filterButton: UIButton!
    
    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var topBackView: UIView!
    @IBOutlet var searchMainBackView: UIView!
    @IBOutlet var searchTextImageView: UIImageView!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var cartBackView: UIView!
    
    
    var dropDownTable: UITableView!
    var cartCount : String = ""
    var productList : [NSDictionary] = []
    var filterOption : [NSDictionary] = []
    var is_dropDownShow = false
    var storeListInner = EXStoreListDetails()
    
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("")
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        
        headingLabel.text = "STORE LISTING".localized(LANGUAGE!)
        
        if IS_LANGUAGE_SWITCHED[3] == 1{

            var params: NSDictionary!
            if let userId = EX_DEFAULTS.valueForKey("User_Id") {
                params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
                //            params  = ["User_Id": userId, "lang_key": LANGUAGE!]
                postStoreList(params)
            }else {
                params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
                //params  = ["lang_key": LANGUAGE!]
                postStoreList(params)
            }
            IS_LANGUAGE_SWITCHED[3] = 0
            viewDidLoad()
        }
        
        if IS_CART_UPDATED[0] == 1{
            IS_CART_UPDATED[0] = 0
            //viewDidLoad()
            var params: NSDictionary!
            params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
            postStoreList(params)
        }
        if productList == [] {
            var params: NSDictionary!
            params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
            postStoreList(params)
        }else {
            storeCollection.reloadData()
        }
        
        self.searchTextField.text = "Please type the product name     ".localized(LANGUAGE!)
        
        if LANGUAGE == "en" {
            filterButton.setBackgroundImage(UIImage(named: "Filter"), forState: UIControlState.Normal)
        }else {
            filterButton.setBackgroundImage(UIImage(named: "Filter_ar"), forState: UIControlState.Normal)
        }
        //self.searchTextField.backgroundColor = UIColor.greenColor()
    }
    override func viewDidLayoutSubviews() {
        if LANGUAGE == "en"{
            self.headingLabel.textAlignment = NSTextAlignment.Left
            self.searchMainBackView.frame = CGRectMake(5, self.searchMainBackView.frame.origin.y, self.searchMainBackView.frame.size.width, self.searchMainBackView.frame.size.height)
            self.filterButton.frame = CGRectMake((self.topBackView.frame.width - self.filterButton.frame.size.width) - 9, self.filterButton.frame.origin.y, self.filterButton.frame.size.width, self.filterButton.frame.size.height)
            self.searchTextField.frame = CGRectMake(8, self.searchTextField.frame.origin.y, self.searchTextField.frame.size.width, self.searchTextField.frame.size.height)
            self.searchButton.frame = CGRectMake((self.searchMainBackView.frame.width - self.searchButton.frame.width), self.searchButton.frame.origin.y, self.searchButton.frame.size.width, self.searchButton.frame.size.height)
            self.searchTextField.textAlignment = NSTextAlignment.Left
            self.cartBackView.frame = CGRectMake((self.topBackView.frame.width - self.cartBackView.frame.width) + 10, self.cartBackView.frame.origin.y, self.cartBackView.frame.size.width, self.cartBackView.frame.size.height)
            self.searchTextImageView.image = UIImage(named: "Search_text_back")
            self.dropDownView.frame = CGRectMake(self.containerView.frame.size.width - self.dropDownView.frame.size.width, self.dropDownView.frame.origin.y, self.dropDownView.frame.size.width, self.dropDownView.frame.size.height)
        }else{
            self.headingLabel.textAlignment = NSTextAlignment.Right
            self.searchMainBackView.frame = CGRectMake((self.topBackView.frame.width - self.searchMainBackView.frame.size.width) - 5, self.searchMainBackView.frame.origin.y, self.searchMainBackView.frame.size.width, self.searchMainBackView.frame.size.height)
            self.filterButton.frame = CGRectMake(9, self.filterButton.frame.origin.y, self.filterButton.frame.size.width, self.filterButton.frame.size.height)
            self.searchTextField.frame = CGRectMake((self.searchMainBackView.frame.width - self.searchTextField.frame.width) - 8, self.searchTextField.frame.origin.y, self.searchTextField.frame.size.width, self.searchTextField.frame.size.height)
            self.searchButton.frame = CGRectMake(0, self.searchButton.frame.origin.y, self.searchButton.frame.size.width, self.searchButton.frame.size.height)
            self.searchTextField.textAlignment = NSTextAlignment.Right
            print(self.topBackView.frame.origin.x)
            self.cartBackView.frame = CGRectMake(self.topBackView.frame.origin.x + 10, self.cartBackView.frame.origin.y, self.cartBackView.frame.size.width, self.cartBackView.frame.size.height)
            self.searchTextImageView.image = UIImage(named: "Search_text_back_ar")
            self.dropDownView.frame = CGRectMake(self.containerView.frame.origin.x, self.dropDownView.frame.origin.y, self.dropDownView.frame.size.width, self.dropDownView.frame.size.height)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Product Page")
        
        dropDownTable = UITableView(frame: CGRectMake(0, 0, 180, 150))
        dropDownTable.delegate = self
        dropDownTable.dataSource = self
        dropDownTable.rowHeight = 30
        dropDownTable.layer.borderColor = THEME_COLOUR.CGColor
        dropDownTable.layer.borderWidth = 3
        dropDownTable.separatorColor = UIColor.clearColor()
        dropDownTable.backgroundColor = UIColor.clearColor()
        dropDownTable.scrollEnabled = true
        dropDownTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "dropDown")
        dropDownView.addSubview(dropDownTable)
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filterOption.count > 0 {
            return filterOption.count
        }else {
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = dropDownTable.dequeueReusableCellWithIdentifier("dropDown", forIndexPath: indexPath) as! UITableViewCell
        cell.userInteractionEnabled = true
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        let seperator = UILabel(frame: CGRectMake(0, 0, cell.contentView.frame.size.width, 1))
        seperator.backgroundColor = THEME_COLOUR
        seperator.text = ""
        cell.contentView.addSubview(seperator)
        cell.backgroundColor = UIColor.whiteColor()
        
//        let dropDownText = EXLabel(frame: CGRectMake(8, cell.contentView.frame.size.height / 2 - 12, cell.contentView.frame.size.width - 15, 24))
        cell.textLabel!.font = UIFont(name: FONT_NAME, size: 13)
//        cell.contentView.addSubview(dropDownText)
        if LANGUAGE == "en"{
            cell.textLabel!.textAlignment = NSTextAlignment.Left
        }else{
            cell.textLabel!.textAlignment = NSTextAlignment.Right
        }
        
        let companyName = filterOption[indexPath.row]["CatagoryName"] as? String
        print(companyName)
        cell.textLabel!.text = companyName?.capitalizedString
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let companyID = filterOption[indexPath.row]["ID"] as? String
        var params: NSDictionary!
        params  = ["CategoryId": companyID!, "lang_key": LANGUAGE!]
        postFilterList(params)
        showDropDownTable()
    }
    
    // MARK: - CollectionView Delegate Methods
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        if self.productList.count != 0 {
            return 1
        }else {
            return 0
        }
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = EXStoreDetailCell()
        cell = storeCollection.dequeueReusableCellWithReuseIdentifier("Store", forIndexPath: indexPath) as! EXStoreDetailCell
        let cellDictionary : NSDictionary = self.productList[indexPath.row]
        print(cellDictionary)
        
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
            cell.productBuyButton.setBackgroundImage(UIImage(named: "Buy"), forState: UIControlState.Normal)
        }else {
            loadingImage = "loading_ar"
            cell.productBuyButton.setBackgroundImage(UIImage(named: "Buy_ar"), forState: UIControlState.Normal)
        }
        
        
        if let productImage = cellDictionary["Image"] as? String {
            cell.productImage .sd_setImageWithURL(NSURL(string: productImage), placeholderImage: UIImage(named: loadingImage))
        }
        cell.productDesctiption.text = cellDictionary["Description"] as! String
        if LANGUAGE == "en"{
            cell.productDesctiption.textAlignment = NSTextAlignment.Left
        }else{
            cell.productDesctiption.textAlignment = NSTextAlignment.Right
        }
        
        //(jsonDict["totalfup"] as! NSString).doubleValue
        if let productID = (cellDictionary["ID"] as? NSString) {
            cell.productID = productID as String
        }
        
        let price = cellDictionary["Price"] as? String
        
        var attribute: NSMutableAttributedString!
        attribute = EX_DELEGATE.attributed(NSAttributedString(string: "Price: ".localized(LANGUAGE!)), isBold: false, fontsize: 13.0, fontColor: UIColor.blackColor())
        attribute.appendAttributedString(EX_DELEGATE.attributed(NSAttributedString(string: price! + " KD".localized(LANGUAGE!)), isBold: false, fontsize: 13.0, fontColor: THEME_COLOUR))
        
        cell.productPrice.attributedText = attribute
        
        cell.availableStock = (cellDictionary["AvailableStock"] as! NSString).integerValue
        
        cell.productBuyButton.tag = indexPath.row
        
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        //performSegueWithIdentifier("Category_Inner", sender: indexPath)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let viewWidth = (self.containerView.frame.size.width/2) - 5
        print(viewWidth)
        return CGSizeMake(viewWidth , viewWidth * 1.593103)
    }

//    func collectionView(collectionView: UICollectionView,
//        layout collectionViewLayout: UICollectionViewLayout,
//        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//            print(self.storeCollection.frame.size.width)
//            return CGSizeMake(196 , 196)
//    }
    //Use for interspacing
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 5.0
    }
    
    func collectionView(collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 5.0
    }
    
    // MARK: - WebServices
    func postStoreList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "storelisting.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let storeListData = Json["Storelisting"] {
                        
                        print(storeListData["CartCount"])
                        self.cartCount = (String(storeListData["CartCount"] as! String))
                        
                        self.productList = storeListData["ProductList"] as! [NSDictionary]
                        
                        self.filterOption = storeListData["FilterOption"] as! [NSDictionary]
                        
                        print(self.filterOption)
                        
                        self.dropDownTable.frame.size.height = CGFloat(self.filterOption.count * 30)
                        if self.filterOption.count < 6 {
                            self.dropDownView.backgroundColor = UIColor.clearColor()
                            self.dropDownTable.scrollEnabled = false
                        }else {
                            self.dropDownView.backgroundColor = UIColor.whiteColor()
                            self.dropDownTable.scrollEnabled = true
                        }
                        if self.cartCount == "0"{
                            self.cartCountLabel.text = ""
                        }else{
                            self.cartCountLabel.text = self.cartCount
                        }
                        self.storeCollection.reloadData()
                        self.dropDownTable.reloadData()
                    }
                }
            })
        }
    }
    
    func postFilterList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "storelisting_filter.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let storeListData = Json["Storelisting"] {
                        
                        //self.cartCount = storeListData["CartCount"] as! Int
                        
                        self.productList = storeListData["ProductList"] as! [NSDictionary]
                        
                        //self.filterOption = storeListData["FilterOption"] as! [NSDictionary]
                        
//                        self.dropDownTable.frame.size.height = CGFloat(self.filterOption.count * 30)
//                        if self.filterOption.count < 6 {
//                            self.dropDownView.backgroundColor = UIColor.clearColor()
//                            self.dropDownTable.scrollEnabled = false
//                        }else {
//                            self.dropDownView.backgroundColor = UIColor.whiteColor()
//                            self.dropDownTable.scrollEnabled = true
//                        }
                        //self.cartCountLabel.text = String(self.cartCount)
                        self.storeCollection.reloadData()
                        //self.dropDownTable.reloadData()
                    }
                }
            })
        }
    }
    
    func postSearchList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "product_search.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let storeListData = Json["Storelisting"] {
                        
                        //self.cartCount = storeListData["CartCount"] as! Int
                        
                        self.productList = storeListData["ProductList"] as! [NSDictionary]
                        //self.searchTextField.text = ""
                        self.storeCollection.reloadData()
                        //self.dropDownTable.reloadData()
                    }
                }
            })
        }
    }
        
    @IBAction func filterButtonActn(sender: UIButton) {
        showDropDownTable()
        dropDownTable.reloadData()
    }
    
    @IBAction func productBuyButtonAction(sender: UIButton) {
        print(sender.tag)
        performSegueWithIdentifier("StoreInner", sender: sender.tag)
    }
    
    // MARK: - Actions
    func showDropDownTable() {
        
        if self.filterOption.count > 0 {
            
            UIView.animateWithDuration(0.5, delay: 0.0, options: .CurveEaseInOut, animations: { () -> Void in
                
                if self.is_dropDownShow == false {
                    //                self.dropDownView.frame = CGRectMake(self.view.frame.size.width - 142, 67, self.dropDownView.frame.size.width, 140)
                    self.dropDownView.frame.size.height = CGFloat(self.filterOption.count * 30)
                    //                        self.dropDownTable.frame = CGRectMake(0, 0, 120, 150)
                    //                self.dropDownTable.frame = CGRectMake(0, 0, self.dropDownView.frame.size.width, 140)
                }else {
                    //                self.dropDownView.frame = CGRectMake(self.view.frame.size.width - 142, 67, self.dropDownView.frame.size.width, 0)
                    self.dropDownView.frame.size.height = 0
                    //                        self.dropDownTable.frame = CGRectMake(0, 0, 120, 0)
                }
                }) { (completed: Bool) -> Void in
                    if self.is_dropDownShow == false {
                        self.is_dropDownShow = true
                    }else {
                        self.is_dropDownShow = false
                    }
            }
        }
        
    }
    
    @IBAction func searchButtonAction(sender: UIButton) {
        search()
    }
    func search() {
        searchTextField.resignFirstResponder()
        if (EX_DELEGATE.TrimString(self.searchTextField.text!) == "") || ((self.searchTextField.text!) == "Please type the product name     ".localized(LANGUAGE!)) {
            EX_DELEGATE.showAlert("Message", message: "Please enter text to search".localized(LANGUAGE!), buttonTitle: "OK")
        }else{
            let seachTag = EX_DELEGATE.TrimString(self.searchTextField.text!)
            var params: NSDictionary!
            params  = ["Search_Key": seachTag, "lang_key": LANGUAGE!]
            postSearchList(params)
        }
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        search()
        return true
    }
    
    // MARK: - TextField Delegates
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField.text == "Please type the product name     ".localized(LANGUAGE!) {
            textField.text = ""
        }
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.text == "" {
            textField.text = "Please type the product name     ".localized(LANGUAGE!)
        }
    }
    
    @IBAction func cartButtonAction(sender: UIButton) {
        self.tabBarController?.selectedIndex = 14
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "StoreInner" {
            storeListInner = segue.destinationViewController as! EXStoreListDetails
            let cellDictionary : NSDictionary = self.productList[sender as! Int]
            
            if let productID = (cellDictionary["ID"] as? NSString) {
                storeListInner.productId = productID as String
            }
            
            storeListInner.title_Logo_Delegate = self
        }
    }
}


class EXStoreDetailCell: UICollectionViewCell {
    
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var productDesctiption: UITextView!
    @IBOutlet var productPrice: UILabel!
    @IBOutlet var productBuyButton: UIButton!
    var productID : String = ""
    var availableStock : Int = 0
}
