//
//  EXRegistrationPopUp.swift
//  Expo
//
//  Created by Abdul Malik Aman on 02/05/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

protocol countryChangeDelegate {
    func changeCountry(countryName: String, countryId: String)
}
class EXRegistrationPopUp: UIViewController {

    var CountryArray: NSArray!
    var delegate: countryChangeDelegate?
    @IBOutlet weak var ChangeCountryTable: UITableView!
    
    override func viewWillAppear(animated: Bool) {
        if COUNTRIES!.count > 0 {
            CountryArray = COUNTRIES
        }        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if CountryArray != nil {
            return CountryArray.count
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = ChangeCountryTable.dequeueReusableCellWithIdentifier("country", forIndexPath: indexPath) as! EXRegistrationPopUpCell
        
        let countryName = CountryArray[indexPath.row] as! NSDictionary
        cell.countryName!.text = countryName["Country"] as? String
        if LANGUAGE == "en" {
            cell.countryName.textAlignment = NSTextAlignment.Left
        }else {
            cell.countryName.textAlignment = NSTextAlignment.Right
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let countryDict = CountryArray[indexPath.row] as! NSDictionary
        let countryName = countryDict["Country"] as? String
        let countryId = countryDict["ID"] as? String
        delegate?.changeCountry(countryName!, countryId: countryId!)
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    // MARK: - Web Services
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXRegistrationPopUpCell: UITableViewCell {
    
    @IBOutlet weak var countryName: EXLabel!
    
}