//
//  EXTermsAndConditions.swift
//  Expo
//
//  Created by Ronish on 4/21/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXTermsAndConditions: UIViewController {

    // MARK: - Variable
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    @IBOutlet weak var Terms: UIWebView!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBarHidden = true
        title_Logo_Delegate!.mainTitleChange!("TERMS & CONDITIONS".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        
        let params = ["lang_key": LANGUAGE!, "Page_Name": "terms_conditions"]
        postTermsAndConditions(params)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Web Services
    func postTermsAndConditions(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "page_content.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let terms = Json["content"] {
                        self.Terms.loadHTMLString(terms as! String, baseURL: nil)
                    }
                    
                }
            })
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
