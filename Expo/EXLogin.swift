//
//  EXLogin.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXLogin: UIViewController, UITextFieldDelegate {
    
    // MARK: - Variables
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    let facebookReadPermissions = ["public_profile", "email", "user_friends"]
    var userName = ""
    var password = ""
    var loginAttempt: Int!
    var keepMeSignIn = false
    var faceBook_Clicked = false
    var is_keyboard_up = false
    
    @IBOutlet weak var loginTitle: EXLabel!
    @IBOutlet weak var loginContainer: UIView!
    @IBOutlet weak var LoginTable: UITableView!
    
    // MARK: - View Methods
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == false {
            if EX_DEFAULTS.valueForKey("isFirstLaunch") != nil {
                LoginTable.frame = CGRectMake(0, 0, self.loginContainer.frame.size.width, self.loginContainer.frame.size.height)
            }else {
                LoginTable.frame = CGRectMake(0, 0, self.loginContainer.frame.size.width, self.loginContainer.frame.size.height)
            }
        }else {
            if EX_DEFAULTS.valueForKey("isFirstLaunch") == nil {
                LoginTable.frame = CGRectMake(0, 0, self.loginContainer.frame.size.width, self.loginContainer.frame.size.height - (KEYBOARD_FRAME - 20))
            }else {
                LoginTable.frame = CGRectMake(0, 0, self.loginContainer.frame.size.width , self.loginContainer.frame.size.height - (KEYBOARD_FRAME - 70))
            }
        }
    }
    override func viewWillAppear(animated: Bool) {
//        title_Logo_Delegate!.mainTitleChange!("LOGIN")
//            title_Logo_Delegate!.ChangeLogoImage!(false)
//            title_Logo_Delegate!.HideBackButton!(true)
//            title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 0)
        userName = ""
        password = ""
//        keepMeSignIn = false
        LoginTable.reloadData()
        
        loginTitle.text = "LOGIN".localized(LANGUAGE!)
        if EX_DEFAULTS.valueForKey("CountryLoaded") == nil {
            getCountry(LANGUAGE!)
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
        // Do any additional setup after loading the view.
        
        loginContainer.backgroundColor = UIColor.clearColor()
        
    }

    // MARK: - Actions
    @IBAction func continueAsGuest(sender: AnyObject) {
        if let UserID = EX_DEFAULTS.valueForKey("GuestUser_Id") {
            USER_ID = UserID as? String
            EX_DEFAULTS.setValue(UserID, forKey: "User_Id")
        }else {
            let userId = NSUUID().UUIDString
            USER_ID = userId
            EX_DEFAULTS.setValue(userId, forKey: "User_Id")
            EX_DEFAULTS.setValue(userId, forKey: "GuestUser_Id")
        }
        IS_GUEST = true
        EX_DEFAULTS.setValue(true, forKey: "isGuest")
        EX_DEFAULTS.setBool(false, forKey: "Login")
        performSegueWithIdentifier("Home", sender: self)
        
        EX_DELEGATE.showAlert("Message", message: "Successfully Logged in as Guest".localized(LANGUAGE!), buttonTitle: "OK")
    }
    @IBAction func loginButtonAction(sender: UIButton) {
        self.view.endEditing(true)
        if sender.tag == 0 {
            validateLogin()
        }else {
            performSegueWithIdentifier("Registration", sender: self)
        }
    }
    func findButtonRowIndex(Sender : UITextField) ->NSIndexPath{
        let buttonPosition:CGPoint = Sender.convertPoint(CGPointZero, toView: LoginTable)
        let IndexPath:NSIndexPath = LoginTable.indexPathForRowAtPoint(buttonPosition)!
        return IndexPath
    }
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        let keyboardHeight = keyboardRectangle.height
        KEYBOARD_FRAME = keyboardHeight + 10
    }
    func validateLogin() {
        loginAttempt = nil
        if userName == "" || password == "" {
            if userName == "" {
                loginAttempt = 0
                EX_DELEGATE.showAlert("Message", message: "Enter username".localized(LANGUAGE!), buttonTitle: "OK")
            }else {
                loginAttempt = 1
                EX_DELEGATE.showAlert("Message", message: "Enter password".localized(LANGUAGE!), buttonTitle: "OK")
            }
            LoginTable.reloadData()
        }else {
            postLogin(userName, pword: password)
        }
    }
    @IBAction func KeepMeSignedIn(sender: EXButton) {
        self.view.endEditing(true)
        if keepMeSignIn == true {
            keepMeSignIn = false
        }else {
            keepMeSignIn = true
        }
        LoginTable.reloadData()
    }
    @IBAction func ForgetPassword(sender: AnyObject) {
        self.view.endEditing(true)
        if userName != "" {
            if EX_DELEGATE.isValidEmail(userName) == true {
                postForgetPassword(userName)
            }else {
                loginAttempt = 0
                LoginTable.reloadData()
            }
        }else {
            loginAttempt = 0
            LoginTable.reloadData()
            EX_DELEGATE.showAlert("Message", message: "Invalid Email".localized(LANGUAGE!), buttonTitle: "OK")
        }
    }
    
    
    //MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if EX_DEFAULTS.valueForKey("isFirstLaunch") == nil {
//            return 7
//        }else {
//            return 6
//        }
        return 7
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = "textFieldCell"
        if indexPath.row == 0 || indexPath.row == 1 {
            if LANGUAGE == "en" {
                identifier = "textFieldCell"
            }else {
                identifier = "textFieldCell_ar"
            }
        }else if indexPath.row == 2 {
            if LANGUAGE == "en" {
                identifier = "checkBoxCell"
            }else {
                identifier = "checkBoxCell_ar"
            }
        }else if indexPath.row == 3 || indexPath.row == 4 {
            identifier = "buttonCell"
        }else if indexPath.row == 5 {
            identifier = "facebookCell"
        }else if indexPath.row == 6 {
            identifier = "guest"
        }
        let cell = LoginTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXLoginCell
        
        if indexPath.row == 0 {
            cell.placeHolder.text = "EMAIL".localized(LANGUAGE!)
            cell.textInnerImage.image = UIImage(named: "man")
            cell.loginInput.returnKeyType = UIReturnKeyType.Default
            cell.loginInput.tag = 0
            cell.loginInput.keyboardType = UIKeyboardType.EmailAddress
            cell.loginInput.autocorrectionType = UITextAutocorrectionType.No
            cell.loginSeperator.hidden = true
            cell.loginInput.text = userName
        }else if indexPath.row == 1 {
            cell.placeHolder.text = "PASSWORD".localized(LANGUAGE!)
            cell.textInnerImage.image = UIImage(named: "lock")
            cell.loginInput.returnKeyType = UIReturnKeyType.Default
            cell.loginInput.keyboardType = UIKeyboardType.ASCIICapable
            cell.loginInput.autocorrectionType = UITextAutocorrectionType.No
            cell.loginInput.secureTextEntry = true
            cell.loginInput.tag = 1
            cell.loginInput.text = password
            cell.loginSeperator.hidden = false
        }else if indexPath.row == 2 {
            cell.forgetPassword.setTitle("FORGOT PASSWORD?".localized(LANGUAGE!), forState: UIControlState.Normal)
            cell.keepMeSignIn.setTitle("KEEP ME SIGNED IN".localized(LANGUAGE!), forState: UIControlState.Normal)
            cell.forgetPassword.titleLabel?.adjustsFontSizeToFitWidth = true
            cell.keepMeSignIn.titleLabel?.adjustsFontSizeToFitWidth = true
            
            if SCREEN_HEIGHT < 600 {
                cell.keepMeSignIn.titleLabel?.font = UIFont(name: FONT_NAME, size: 13)
            }
            if keepMeSignIn == false {
                cell.checkImage.hidden = false
                cell.tickMark.hidden = true
            }else {
                cell.checkImage.hidden = true
                cell.tickMark.hidden = false
            }
        }else if indexPath.row == 3 {
            cell.LoginButton.setTitle("LOGIN".localized(LANGUAGE!), forState: UIControlState.Normal)
            cell.LoginButton.tag = 0
        }else if indexPath.row == 4 {
            cell.LoginButton.setTitle("REGISTER".localized(LANGUAGE!), forState: UIControlState.Normal)
            cell.LoginButton.tag = 1
        }else if indexPath.row == 5 {
            cell.facebookLoginLabel.text = (cell.facebookLoginLabel.text?.localized(LANGUAGE!))!
            if faceBook_Clicked == false {
                cell.facebookLogin.userInteractionEnabled = true
            }else {
                cell.facebookLogin.userInteractionEnabled = false
            }
        }else if indexPath.row == 6 {
            cell.guestButton.setTitle("SKIP".localized(LANGUAGE!), forState: UIControlState.Normal)
        }
        
        if loginAttempt != nil {
            if indexPath.row == loginAttempt {
                EX_DELEGATE.ShakeAnimation(cell)
            }
            loginAttempt = nil
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 || indexPath.row == 1 {
            return 82
        }else if indexPath.row == 2 {
            return 60
        }else {
            return 68
        }
    }
    
    // MARK: - Web Services
    func postLogin(uname: String, pword: String) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        let params = ["Email": uname, "Password": pword, "lang_key": LANGUAGE!]
        EX_Network.postJson(WEB_SERVICE + "login.php", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let signIn = Json["signin"] {
                        if let success = signIn["Success"] {
                            if success as! Bool == true {
                                
                                let userDetails = signIn["user"] as! NSDictionary
                                let email = userDetails["Email"] as! String
                                let mobile = userDetails["MobileNumber"] as! String
                                let name = userDetails["Name"] as! String
                                let userId = userDetails["UserId"] as! String
                                
                                EX_DEFAULTS.setValue(name, forKey: "User_Name")
                                EX_DEFAULTS.setValue(email, forKey: "User_Email")
                                EX_DEFAULTS.setValue(mobile, forKey: "User_Phone")
                                EX_DEFAULTS.setValue(userId, forKey: "User_Id")
                                
                                USER_ID = userId
                            
                                IS_SESSION_EXPIRED = false
                                EX_DEFAULTS.setBool(false, forKey: "Login")
                                
                                IS_GUEST = false
                                EX_DEFAULTS.setValue(false, forKey: "isGuest")
                                
                                self.performSegueWithIdentifier("Home", sender: self)
//                                if EX_DEFAULTS.valueForKey("isFirstLaunch") == nil {
//                                    
//                                }else {
//                                    self.tabBarController?.selectedIndex = 0
//                                }
                                EX_DELEGATE.showAlert("Message", message: "Successfully Logged in".localized(LANGUAGE!), buttonTitle: "OK")
                            }else {
                                EX_DELEGATE.showAlert("Message", message: "Incorrect username or password".localized(LANGUAGE!), buttonTitle: "OK")
                                self.loginAttempt = 1
                                self.LoginTable.reloadData()
                            }
                        }
                    }else {
                        let signIn = Json["signin"] as! NSDictionary
                        let msg = signIn["Message"] as! String
                        EX_DELEGATE.showAlert("Message", message: msg, buttonTitle: "OK")
                        self.loginAttempt = 1
                        self.LoginTable.reloadData()
                    }
                }else {
                    EX_DELEGATE.showAlert("Message", message: "Incorrect username or password".localized(LANGUAGE!), buttonTitle: "OK")
                    self.loginAttempt = 1
                    self.LoginTable.reloadData()
                }
            })
        }
    }
    
    func postForgetPassword(email: String) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        let params = ["Email": email, "lang_key": LANGUAGE!]
        EX_Network.postJson(WEB_SERVICE + "forgot_password.php", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                
                if Succeeded == true {
                    if let message = Json["Message"] {
                        EX_DELEGATE.showAlert("Message", message: message as! String, buttonTitle: "OK")
                    }
                }else {
                    
                }
            })
        }
    }
    func postFacebook(email: String, fName: String, lName: String, uName: String) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        let params = ["Email": email, "FirstName": fName, "LastName": lName, "UserName": uName]
        EX_Network.postJson(WEB_SERVICE + "fb_register.php", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                
                if Succeeded == true {
                    if let facebookLogin = Json["fb_login_status"] {
                        if let success = facebookLogin["Success"] {
                            if success as! Bool == true {
                                
                                let userId = facebookLogin["UserId"] as! String
                                
                                IS_SESSION_EXPIRED = false
                                EX_DEFAULTS.setBool(false, forKey: "Login")
                                
                                EX_DEFAULTS.setValue(userId, forKey: "User_Id")
                                self.performSegueWithIdentifier("Home", sender: self)
                                IS_GUEST = false
                                EX_DEFAULTS.setValue(false, forKey: "isGuest")
                                
//                                if EX_DEFAULTS.valueForKey("isFirstLaunch") == nil {
//                                    self.performSegueWithIdentifier("Home", sender: self)
//                                }else {
//                                    self.tabBarController?.selectedIndex = 0
//                                }
                            }else {
                                self.loginAttempt = 1
                                self.LoginTable.reloadData()
                            }
                        }
                    }
                }else {
                    
                }
            })
        }
    }
    func getCountry(language: String) {
        let params = ["lang_key": language]
        
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "country.php", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), {
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Json.count > 0 {
                    COUNTRIES = Json["country"] as? [AnyObject]
                    EX_DEFAULTS.setValue(COUNTRIES, forKey: "CountryLoaded")
                }
            })
        }
        
    }
    
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        LoginTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        loginContainer.backgroundColor = UIColor.whiteColor()
        is_keyboard_up = true
        if textField.tag == 0 {
            scrollToTop(0, section: 0, table: LoginTable)
        }else {
            scrollToTop(1, section: 0, table: LoginTable)
        }
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        loginContainer.backgroundColor = UIColor.clearColor()
        is_keyboard_up = false
        if textField.tag == 0 {
            userName = textField.text!
        }else {
            password = textField.text!
        }
        viewDidLayoutSubviews()
    }
    
    // MARK: - Facebook
    @IBAction func loginFacebook(sender: UIButton) {
        self.faceBook_Clicked = true
        loginToFacebookWithSuccess({ () -> () in
                print("FB login Success")
            self.faceBook_Clicked = false
            IS_SESSION_EXPIRED = false
            EX_DEFAULTS.setBool(false, forKey: "Login")
            
            EX_DELEGATE.showAlert("Message", message: "Successfully Logged in with facebook".localized(LANGUAGE!), buttonTitle: "OK")
            }) { (NSError) -> () in
                print("Error")
            self.faceBook_Clicked = false
        }
        
    }
    func loginToFacebookWithSuccess(successBlock: () -> (), andFailure failureBlock: (NSError?) -> ()) {
        if FBSDKAccessToken.currentAccessToken() != nil {
            //For debugging, when we want to ensure that facebook login always happens
            FBSDKLoginManager().logOut()
            //Otherwise do:
            //            return
        }
        //        FBSDKLoginManager().logInWithReadPermissions(self.facebookReadPermissions, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
        FBSDKLoginManager().logInWithReadPermissions(self.facebookReadPermissions, fromViewController: nil, handler: {(result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
            
            if error != nil {
                FBSDKLoginManager().logOut()
                failureBlock(error)
            } else if result.isCancelled {
                FBSDKLoginManager().logOut()
                failureBlock(nil)
            } else {
                var allPermsGranted = true
                
                //result.grantedPermissions returns an array of _NSCFString pointers
                let grantedPermissions = result.grantedPermissions // .allObjects.map( {"\($0)"} )
                for permission in self.facebookReadPermissions {
                    if grantedPermissions.contains(permission) {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted == false {
                    let fbToken = result.token.tokenString
                    //                    let fbUserID = result.token.userID
                    let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,picture.width(600).height(600)"], tokenString: fbToken, version: nil, HTTPMethod: "GET")
                    req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
                        if(error == nil)
                        {
                            print("result \(result)")
                            
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                let email = result["email"] as! String
                                let user_id = result["id"] as! String
                                let name = result["name"] as! String
                                let fullName = name.componentsSeparatedByString(" ")
                                let fName = fullName[0]
                                let lName = fullName[1]
                                
                                EX_DEFAULTS.setValue(fullName, forKey: "User_Name")
                                EX_DEFAULTS.setValue(email, forKey: "User_Email")
                                EX_DEFAULTS.setValue("", forKey: "User_Phone")
                                
                                self.postFacebook(email, fName: fName, lName: lName, uName: user_id)
                            })
                            let imageArray = result["picture"] as? NSDictionary
                            let imageData = imageArray!["data"] as? NSDictionary
                            let urlString = imageData!["url"] as? String
                            
                            if let url = NSURL(string: urlString!) {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                                    if let data = NSData(contentsOfURL: url) {
                                        dispatch_async(dispatch_get_main_queue()) {
                                            //                                            self.proImage?.image = UIImage(data: data)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            print("error \(error)")
                        }
                    })
                    successBlock()
                } else {
                    print("Failed to login")
                    //                    failureBlock((nil)
                }
            }
        })
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class EXLoginCell: UITableViewCell {
    
    @IBOutlet weak var placeHolder: EXLabel!
    @IBOutlet weak var loginInput: UITextField!
    @IBOutlet weak var textInnerImage: UIImageView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var keepMeSignIn: EXButton!
    @IBOutlet weak var forgetPassword: EXButton!
    @IBOutlet weak var LoginButton: EXButton!
    @IBOutlet weak var tickMark: UIImageView!
    @IBOutlet weak var loginSeperator: UIView!
    @IBOutlet weak var facebookLogin: EXButton!
    @IBOutlet weak var facebookLoginLabel: EXLabel!
    @IBOutlet weak var guestButton: EXButton!
    
}
