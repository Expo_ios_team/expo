//
//  EXMyAccount.swift
//  Expo
//
//  Created by Ronish on 5/11/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class EXMyAccount: UIViewController, UIPopoverPresentationControllerDelegate {

    // MARK: - Variables
    let placHolderArray = ["Name", "Email", "Country", "City", "Address", "Phone"]
    @IBOutlet weak var MyAccountTable: UITableView!
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var myAccountDetails: NSDictionary!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        title_Logo_Delegate!.mainTitleChange!("MY ACCOUNT")
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 0)
        
        let params: NSDictionary  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
        postMyAccount(params)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    
    // MARK: - Actions
    @IBAction func editMyAccount(sender: AnyObject) {
        title_Logo_Delegate?.reloadProfileView!()
        title_Logo_Delegate?.showChangePassword!(false, container: "Profile")
    }
    func showChangePassword(myView: UIView) {
        let changePassword: EXChangePassword = storyboard!.instantiateViewControllerWithIdentifier("EXChangePassword") as! EXChangePassword
//        changePassword.delegate = self
//        changeCountry.CountryArray = CountryArray
        changePassword.modalPresentationStyle = .Popover
        changePassword.preferredContentSize = CGSizeMake(self.view.frame.width - 20, 300)
        let popoverMenuViewController = changePassword.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = view
        popoverMenuViewController?.sourceRect = CGRect(
            x: self.view.frame.width / 2 ,
            y: self.view.frame.height / 2,
            width: 1,
            height: 1)
        presentViewController(
            changePassword,
            animated: true,
            completion: nil)
        
    }
    @IBAction func logOut(sender: AnyObject) {
        if sender.tag == 6 {
//            showChangePassword(self.view)
            title_Logo_Delegate?.showChangePassword!(false, container: "ChangePassword")
        }else {
            if FBSDKAccessToken.currentAccessToken() != nil {
                FBSDKLoginManager().logOut()
                FBSDKAccessToken.setCurrentAccessToken(nil)
                FBSDKProfile.setCurrentProfile(nil)
            }
            IS_GUEST = false
            EX_DEFAULTS.setValue(false, forKey: "isGuest")
            EX_DEFAULTS.setValue(true, forKey: "Login")
            EX_DEFAULTS.setValue(nil, forKey: "isFirstLaunch")
            EX_DEFAULTS.setValue(nil, forKey: "MessageID")
            APPDELEGATE?.SessionExpare(true)
        }
    }

    // MARK: - TableView Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if myAccountDetails != nil {
            return 1
        }else {
            return 0
        }
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var identifier = "placeHolder"
        if indexPath.row <= 5 {
            identifier = "placeHolder"
        }else {
            identifier = "buttons"
        }
        
        let cell = MyAccountTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXMyAccountCell
        if indexPath.row <= 5 {
            let dictKeys: [String] = ["Name", "Email", "Country", "City", "Address", "Mobile"]
            cell.placeHolder.text = myAccountDetails.objectForKey(dictKeys[indexPath.row]) as? String
        }else {
            cell.accountButton.tag = indexPath.row
            if indexPath.row == 6 {
                cell.accountButton.setTitle("CHANGE PASSWORD", forState: UIControlState.Normal)
            }else {
                cell.accountButton.setTitle("LOGOUT", forState: UIControlState.Normal)
            }
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    // MARK: - Web Services
    func postMyAccount(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "myaccount.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let myAccount = Json["MyAccount"] {
                        self.myAccountDetails = myAccount as! NSDictionary
                        self.MyAccountTable.reloadData()
                    }
                    
                }
            })
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXMyAccountCell: UITableViewCell {
    
    @IBOutlet weak var placeHolder: EXLabel!
    @IBOutlet weak var accountButton: EXButton!
    @IBOutlet weak var editProfile: EXTextField!

    
}