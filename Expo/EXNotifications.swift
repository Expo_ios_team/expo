//
//  EXNotifications.swift
//  Expo
//
//  Created by Ronish on 4/21/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXNotifications: UIViewController {

    // MARK: - Variables
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    @IBOutlet weak var NotificationTable: UITableView!
    var NotificationArray: NSArray!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBarHidden = true
        title_Logo_Delegate!.mainTitleChange!("NOTIFICATIONS".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        
        let params: NSDictionary  = ["lang_key": LANGUAGE!]
        postNotification(params)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        NotificationTable.rowHeight = 60
        NotificationTable.estimatedRowHeight = 1000
        NotificationTable.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if NotificationArray != nil {
            return NotificationArray.count
        }else {
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = NotificationTable.dequeueReusableCellWithIdentifier("notification", forIndexPath: indexPath) as! EXNotificationCell
        
        
        cell.notificationMessage!.text = NotificationArray.objectAtIndex(indexPath.row).valueForKey("message") as? String
        if indexPath.row == 3 {
            cell.notificationMessage!.text = "test \n multiple \n lines \n for \n expo \n done"
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.rowHeight
    }
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - WebService
    func postNotification(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "notification_history.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let notifications = Json["notifications"] {
                        self.NotificationArray = notifications as! NSArray
                    }
                    self.NotificationTable.reloadData()
                }
            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXNotificationCell: UITableViewCell {
    
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var notificationMessage: EXLabel!
    
}
