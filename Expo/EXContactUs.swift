//
//  EXContactUs.swift
//  Expo
//
//  Created by Ronish on 4/21/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXContactUs: UIViewController {

    // MARK: - Variables
    @IBOutlet weak var ContactTable: UITableView!
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var contactArray = [String](count: 6, repeatedValue: "")
    let placeHolderArray = ["NAME", "MOBILE NUMBER", "EMAIL", "SUBJECT", "Your Message...     "]
    @IBOutlet weak var contactContainer: UIView!
    var is_keyboard_up = false
    
    // MARK: - View Methods
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == false {
            ContactTable.frame = CGRectMake(0, 0, self.contactContainer.frame.size.width, self.contactContainer.frame.size.height)
        }else {
            ContactTable.frame = CGRectMake(0, 0, self.contactContainer.frame.size.width , self.contactContainer.frame.size.height - (KEYBOARD_FRAME - 70))
        }
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        title_Logo_Delegate!.mainTitleChange!("CONTACT US".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        
        let params: NSDictionary = ["lang_key": LANGUAGE!, "Page_Name": "contact"]
        getContactUs(params)
        ContactTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Action
    
    @IBAction func SendMessage(sender: AnyObject) {
        self.view.endEditing(true)
        if Validate() == "" {
            let params = ["lang_key": LANGUAGE!, "Name": contactArray[1], "Email": contactArray[3], "Mobile": contactArray[2], "Subject": contactArray[4], "Message": contactArray[5]]
            postSendMessage(params)
        }else {
            EX_DELEGATE.showAlert("Message", message: Validate(), buttonTitle: "OK")
        }
        
    }
    func Validate() -> String {
        var message: String = ""
        if contactArray[1] == "" {
            message = "Name cannot be empty".localized(LANGUAGE!)
        }else if contactArray[2] == "" {
            message = "Mobile Number cannot be empty".localized(LANGUAGE!)
        }else if EX_DELEGATE.TrimNumbers(contactArray[2]) == false{
            message = "Mobile Number not valid".localized(LANGUAGE!)
        }else if contactArray[3] == "" {
            message = "Email cannot be empty".localized(LANGUAGE!)
        }else if EX_DELEGATE.isValidEmail(contactArray[3]) == false {
            message = "Email not valid".localized(LANGUAGE!)
        }else if contactArray[4] == "" {
            message = "Subject cannot be empty".localized(LANGUAGE!)
        }else if contactArray[5] == "" {
            message = "Message cannot be empty".localized(LANGUAGE!)
        }else if contactArray[5] == "Your Message...     ".localized(LANGUAGE!) {
            message = "Message cannot be empty".localized(LANGUAGE!)
        }
        
        return message
    }
    
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = "TextField"
        if indexPath.row == 0 {
            identifier = "Web"
        }else if indexPath.row == 5 {
            identifier = "TextView"
        }else if indexPath.row == 6 {
            identifier = "Button"
        }else {
            identifier = "TextField"
        }
        let cell = ContactTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXContactUsCell
        
        if indexPath.row != 6 {
            if indexPath.row == 0 {
                cell.contactWeb.loadHTMLString(contactArray[0], baseURL: nil)
            }else if indexPath.row == 5 {
                cell.contactTextView.text = contactArray[5]
                cell.contactTextView.layer.cornerRadius = 5
                cell.contactTextView.layer.borderColor = UIColor.blackColor().CGColor
                cell.contactTextView.layer.borderWidth = 2
                cell.contactTextView.text = placeHolderArray[4].localized(LANGUAGE!)
            }else {
                cell.userDetails.tag = indexPath.row
                cell.userDetails.placeholder = placeHolderArray[indexPath.row - 1].localized(LANGUAGE!)
                cell.userDetails.text = contactArray[indexPath.row]
            }
        }else {
            cell.sendButton.setTitle("SEND MESSAGE".localized(LANGUAGE!), forState: UIControlState.Normal)
        }
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 225
        }else if indexPath.row == 5 {
            return 110
        }else {
            return 60
        }
        
    }
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        if indexPath.row == 0 {
//            return 100
//        }else if indexPath.row == 5 {
//            return 110
//        }else {
//            return 60
//        }
//    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        ContactTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        contactContainer.backgroundColor = UIColor.whiteColor()
        is_keyboard_up = true
        scrollToTop(textField.tag + 2, section: 0, table: ContactTable)
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        contactContainer.backgroundColor = UIColor.clearColor()
        is_keyboard_up = false        
        contactArray[textField.tag] = EX_DELEGATE.TrimString(textField.text!)
        
        viewDidLayoutSubviews()
    }
    
    // MARK: - TextView Delegate
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewDidBeginEditing(textView: UITextView) {
        is_keyboard_up = true
        if textView.text == "Your Message...     ".localized(LANGUAGE!) {
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        scrollToTop(6, section: 0, table: ContactTable)
        viewDidLayoutSubviews()
    }
    func textViewDidEndEditing(textView: UITextView) {
        is_keyboard_up = false
        if EX_DELEGATE.TrimString(textView.text) == "" {
            textView.text = "Your Message...     ".localized(LANGUAGE!)
            textView.textColor = UIColor.lightGrayColor()
        }else {
            contactArray[5] = EX_DELEGATE.TrimString(textView.text)
        }
        ContactTable.reloadData()
        viewDidLayoutSubviews()
    }
    
    // MARK: - WebService
    func getContactUs(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "page_content.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    self.contactArray[0] = Json["content"] as! String
                    self.ContactTable.reloadData()
                }
            })
        }
    }
    
    func postSendMessage(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "contact_submit.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let contactResponse = Json["contact_status"] {
                        if contactResponse["Success"] as! Bool == true {
                            EX_DELEGATE.showAlert("Message", message: contactResponse["Message"] as! String, buttonTitle: "OK")
                        }else {
                            EX_DELEGATE.showAlert("Message", message: contactResponse["Message"] as! String, buttonTitle: "OK")
                        }
                    }else {
                        EX_DELEGATE.showAlert("Message", message: "Message send Failed".localized(LANGUAGE!), buttonTitle: "OK")
                    }
                }
            })
        }
    }

    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXContactUsCell: UITableViewCell {
    
    @IBOutlet weak var sendButton: EXButton!
    @IBOutlet weak var userDetails: EXTextField!
    @IBOutlet weak var contactTextView: UITextView!
    @IBOutlet weak var contactWeb: UIWebView!
}
