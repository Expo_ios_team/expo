//
//  EXOrderHistory.swift
//  Expo
//
//  Created by Ronish on 4/21/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXOrderHistory: UIViewController, Change_Title_Logo_Delegate {

    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    @IBOutlet weak var OrderHistoryTable: UITableView!
    var orderHistory: NSArray!
    @IBOutlet weak var noItems: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        title_Logo_Delegate!.mainTitleChange!("ORDER HISTORY".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        let params: NSDictionary  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
        postOrderHistory(params)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // MARK: - Custom Delegate
    @IBAction func Login(sender: AnyObject) {
        APPDELEGATE?.SessionExpare(true)
    }
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    // MARK: - TableView Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if IS_GUEST == true {
            return 1
        }else {
            if orderHistory != nil {
                if orderHistory.count > 0 {
                    return orderHistory.count
                }else {
                    return 0
                }
            } else {
                return 0
            }
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = "login"
        if IS_GUEST == true {
            identifier = "login"
        }else {
            identifier = "history"
        }
        let cell = OrderHistoryTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXOrderHistoryCell
        
        if IS_GUEST == false {
            let dop = orderHistory[indexPath.row]["Date"] as! String
            let status = orderHistory[indexPath.row]["Status"] as! String
            let Id = orderHistory[indexPath.row]["OrderNumber"] as! String
            let url = NSURL(string: orderHistory[indexPath.row]["Image"] as! String)
            
            cell.dateOfPurchase.text = "Date of Purchase : ".localized(LANGUAGE!) + " " + dop
            cell.orderStatus.text = status.localized(LANGUAGE!)
            cell.Order_Id.text = "Order ID : ".localized(LANGUAGE!) + Id
            
            cell.dateOfPurchase.adjustsFontSizeToFitWidth = true
            cell.Order_Id.adjustsFontSizeToFitWidth = true
            cell.orderStatus.adjustsFontSizeToFitWidth = true
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                if let data = NSData(contentsOfURL: url!) {
                    dispatch_async(dispatch_get_main_queue()) {
                        cell.historyImage.image = UIImage(data: data)
                    }
                }
            }
        }else {
            cell.loginButton.setTitle("LOGIN".localized(LANGUAGE!), forState: UIControlState.Normal)
            cell.emptyLabel.text = "You are logged in as Guest.!\nPlease Login to see order History.".localized(LANGUAGE!)
        }
        
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if IS_GUEST == false {
            let Id = orderHistory[indexPath.row]["OrderNumber"] as! String
            let details = storyboard?.instantiateViewControllerWithIdentifier("EXOrderDetails") as! EXOrderDetails
            details.title_Logo_Delegate = self
            details.orderId = Id
            showViewController(details, sender: self)
        }
        
    }
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        
//    }
    
    // MARK: - WebServices
    func postOrderHistory(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "orderhistory.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    self.noItems.hidden = true
                    if let history = Json["OrderHistory"] {
                        self.orderHistory = history as! NSArray
                        self.OrderHistoryTable.reloadData()
                    }else {
                        self.noItems.hidden = false
                    }
                    
                }
            })
        }
    }

    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXOrderHistoryCell: UITableViewCell {
    
    @IBOutlet weak var historyImage: UIImageView!
    @IBOutlet weak var Order_Id: EXLabel!
    @IBOutlet weak var dateOfPurchase: EXLabel!
    @IBOutlet weak var orderStatus: EXLabel!
    @IBOutlet weak var emptyLabel: EXLabel!
    @IBOutlet weak var loginButton: EXButton!
    
}
