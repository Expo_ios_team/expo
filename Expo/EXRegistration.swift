//
//  EXRegistration.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXRegistration: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIPopoverPresentationControllerDelegate, countryChangeDelegate {
    
    // MARK: - Variables
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    
    @IBOutlet weak var RegistrationTable: UITableView!
    @IBOutlet weak var RegistrationContainer: UIView!
    @IBOutlet weak var registrationTitle: EXLabel!
    
    let placeHolderArray: [String] = ["NAME", "EMAIL", "PASSWORD", "RE-ENTER PASSWORD", "COUNTRY", "CITY", "ADDRESS", "MOBILE NUMBER"]
    var registrationArray: [String] = [String](count: 8, repeatedValue: "")
    var realHeight: CGFloat = 0
    var shrinkedHeight: CGFloat = 0
    var CountryArray: NSArray!
    var ChangeCountryPicker: UIPickerView!
    var ChangeCountryTable: UITableView!
    var Date_Popup: UIView!
    var countryText: UITextField!
    var Is_Picker_Up: Bool = false
    var is_keyboard_up = false
    
    override func viewDidLayoutSubviews() {
        if is_keyboard_up == false {
            RegistrationTable.frame = CGRectMake(0, 0, RegistrationContainer.frame.size.width, RegistrationContainer.frame.size.height)
        }else {
            RegistrationTable.frame = CGRectMake(0, 0, RegistrationContainer.frame.size.width, RegistrationContainer.frame.size.height - (KEYBOARD_FRAME - 20) )
        }
    }
    override func viewWillAppear(animated: Bool) {
        
        registrationTitle.text = "REGISTRATION".localized(LANGUAGE!)
        if EX_DEFAULTS.valueForKey("isFirstLaunch") == nil {
//            title_Logo_Delegate?.hideTabBar!(true)
        }else {
//            title_Logo_Delegate!.mainTitleChange!("REGISTRATION")
//            title_Logo_Delegate!.ChangeLogoImage!(false)
//            title_Logo_Delegate!.HideBackButton!(false)
//            title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 0)
        }
        RegistrationTable.reloadData()
    }
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    
        realHeight = RegistrationTable.frame.size.height
        shrinkedHeight = realHeight - KEYBOARD_FRAME
        
        if let countryList = EX_DEFAULTS.valueForKey("CountryLoaded") {
            CountryArray = countryList as! NSArray
            COUNTRIES = countryList as? [AnyObject]
        }else {
            getCountry(LANGUAGE!)
        }
        // Do any additional setup after loading the view.
    }

    
    // MARK: - Actions
    
    @IBAction func goBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func validateRegistration() {
        self.view.endEditing(true)
        
    }
    @IBAction func registrationButtonAction(sender: UIButton) {
        self.view.endEditing(true)
        print(registrationArray)
        var message: String!
        
        for i in 0..<registrationArray.count {
            if EX_DELEGATE.TrimString(registrationArray[i]) == "" {
                message = "\(placeHolderArray[i].capitalizedString) cannot be empty"
                break
            }
        }
        
        if message == nil {
            if EX_DELEGATE.isValidEmail(registrationArray[1]) == false {
                message = "Email not valid".localized(LANGUAGE!)
            }else if registrationArray[2].characters.count < 6 {
                message = "Try one with at least 6 characters".localized(LANGUAGE!)
            }else if registrationArray[2] != registrationArray[3] {
                message = "Password does not match".localized(LANGUAGE!)
            }else if EX_DELEGATE.TrimNumbers(registrationArray[7]) == false {
                message = "Phone Number cannot enter characters".localized(LANGUAGE!)
            }else if registrationArray[7].characters.count < 8 {
                message = "Please enter a valid Phone Number".localized(LANGUAGE!)
            }
        }
        
        if message == nil {
            postRegistration(registrationArray[1], name: registrationArray[0], lang: "en", address: registrationArray[6], country: registrationArray[4], city: registrationArray[5], mobile: registrationArray[7], passwd: registrationArray[2])
        }else {
            EX_DELEGATE.showAlert("Message", message: message, buttonTitle: "OK")
        }
    }
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    /*
    func changeCountry() {
        if Is_Picker_Up == false && CountryArray.count > 0 {
            Date_Popup = UIView(frame: CGRectMake(20, self.view.frame.height-200, self.view.frame.width-40, 200))
            Date_Popup.backgroundColor = UIColor.whiteColor()
            Date_Popup.layer.borderColor = UIColor.blackColor().CGColor
            Date_Popup.layer.borderWidth = 1
            // Blur effect
            //        var darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Light)
            //        var blurView = UIVisualEffectView(effect: darkBlur)
            //        blurView.frame = Date_Popup.bounds
            //        Date_Popup.addSubview(blurView)
            
            // BG Image
            let bgImage: UIImageView = UIImageView(frame: CGRectMake(0, 0, Date_Popup.frame.width, Date_Popup.frame.height))
            bgImage.image = UIImage(named: "more_BG")
            Date_Popup.addSubview(bgImage)
            
            // PickerView
            ChangeCountryPicker = UIPickerView(frame: CGRectMake(0, 0, Date_Popup.frame.width, 200))
            ChangeCountryPicker.delegate = self
            ChangeCountryPicker.dataSource = self
            Date_Popup.addSubview(ChangeCountryPicker)
            self.view.addSubview(Date_Popup)
            
            //Done Button
            let Done: UIButton = UIButton(frame: CGRectMake(self.view.frame.width-110, 5, 60, 30))
            Done.backgroundColor = THEME_COLOUR
            Done.layer.cornerRadius = 8
            Done.titleLabel?.font = Done.titleLabel?.font.fontWithSize(15)
            Done.setTitleColor(UIColor.whiteColor() , forState: UIControlState.Normal)
            Done.setTitle("OK", forState: UIControlState.Normal)
            Done.addTarget(self, action: "RemovePicker", forControlEvents: UIControlEvents.TouchUpInside)
            Date_Popup.addSubview(Done)
            
            var checkCountry = "Kuwait"
            
            if registrationArray[4] == "" {
                checkCountry = "Kuwait"
            }else {
                checkCountry = registrationArray[4]
            }
            for i in 0..<CountryArray.count {
                let countryName = CountryArray[i] as! NSDictionary
                let indexArray = countryName["Country"] as? String
                
                if indexArray == checkCountry {
                    ChangeCountryPicker.selectRow(i, inComponent: 0, animated: true)
                    break
                }
            }
            Is_Picker_Up = true
        }else {
            RemovePicker()
        }
    }
    */
    
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = ""
        if indexPath.row == 8 {
            identifier = "button"
        }else if indexPath.row == 4{
            identifier = "dropDown"
        }else {
            identifier = "textField"
        }
        let cell = RegistrationTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXRegistrationCell
        
        if indexPath.row != 8{
            cell.placeHolder.text = placeHolderArray[indexPath.row].localized(LANGUAGE!)
            cell.inputText.tag = indexPath.row
            cell.inputText.text = registrationArray[indexPath.row]
            cell.inputText.autocorrectionType = UITextAutocorrectionType.No
            cell.inputText.keyboardType = UIKeyboardType.Alphabet
            cell.inputText.returnKeyType = UIReturnKeyType.Default // UIReturnKeyType.Next
            cell.inputText.secureTextEntry = false
            cell.inputText.inputAccessoryView = nil
            
            if LANGUAGE == "en" {
                cell.inputText.textAlignment = NSTextAlignment.Left
                cell.placeHolder.textAlignment = NSTextAlignment.Left
            }else {
                cell.inputText.textAlignment = NSTextAlignment.Right
                cell.placeHolder.textAlignment = NSTextAlignment.Right
            }
            
            if indexPath.row == 1 {
                cell.inputText.keyboardType = UIKeyboardType.EmailAddress
            }else if indexPath.row == 2 {
                cell.inputText.secureTextEntry = true
            }else if indexPath.row == 3 {
                cell.inputText.secureTextEntry = true
            }else if indexPath.row == 4 {
                countryText = cell.inputText
                if LANGUAGE == "en" {
                    cell.dropDownImage.image = UIImage(named: "dropdown")
                }else {
                    cell.dropDownImage.image = UIImage(named: "dropdown_ar")
                }
            }else if indexPath.row == 7 {
                cell.inputText.keyboardType = UIKeyboardType.NumberPad
                cell.inputText.returnKeyType = UIReturnKeyType.Go
                let numberToolbar: UIToolbar! = UIToolbar(frame: CGRectMake(self.view.frame.width - 80, 0, self.view.frame.width, 50))
                numberToolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
                    UIBarButtonItem(title: "Return", style: UIBarButtonItemStyle.Plain, target: self, action: "validateRegistration")]
                cell.inputText.inputAccessoryView = numberToolbar
            }
        }else {
            cell.registerButton.setTitle("REGISTER".localized(LANGUAGE!), forState: UIControlState.Normal)
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 8 {
            return 68
        }else {
            return 82
        }
    }
    
    //MARK: - PickerView Delegates
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return CountryArray.count
    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView
    {
        let titleLabel = UILabel()
        titleLabel.textColor = THEME_COLOUR
        titleLabel.font = UIFont(name: FONT_NAME, size: 15)
        titleLabel.textAlignment = NSTextAlignment.Center
        let countryName = CountryArray[row] as! NSDictionary
        titleLabel.text = countryName["Country"] as? String
        
        return titleLabel
    }

    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        let countryName = CountryArray[row] as! NSDictionary
        registrationArray[4] = countryName["Country"] as! String
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        RegistrationTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        is_keyboard_up = true
        RegistrationTable.frame.size.height = self.view.frame.height - 100
        scrollToTop(textField.tag, section: 0, table: RegistrationTable)
        if textField == countryText {
            textField.endEditing(true)
            RegistrationTable.scrollEnabled = false
        }
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        is_keyboard_up = false
        registrationArray[textField.tag] = EX_DELEGATE.TrimString(textField.text!)
        viewDidLayoutSubviews()
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField == countryText {
            scrollToTop(textField.tag, section: 0, table: RegistrationTable)
            textField.endEditing(true)
            textField.resignFirstResponder()
//            changeCountry(textField)
            showChangeCountry(countryText)
            return false
        }else {
            return true
        }
    }
    
    // MARK: - Custom Delegates 
    func showChangeCountry(myView: UITextField) {
        self.view.endEditing(true)
        let changeCountry: EXRegistrationPopUp = storyboard!.instantiateViewControllerWithIdentifier("EXRegistrationPopUp") as! EXRegistrationPopUp
        changeCountry.delegate = self
        changeCountry.CountryArray = CountryArray
        changeCountry.modalPresentationStyle = .Popover
        changeCountry.preferredContentSize = CGSizeMake(self.view.frame.size.width - 25, 200)
        let popoverMenuViewController = changeCountry.popoverPresentationController
        popoverMenuViewController?.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = view
        
        var yValue: CGFloat = 65
        if SCREEN_HEIGHT > 500 && SCREEN_HEIGHT < 600 {
            yValue = 53
        }else if SCREEN_HEIGHT > 600 && SCREEN_HEIGHT < 700 {
            yValue = 104
        }else if SCREEN_HEIGHT > 700 {
            yValue = 137
        }
        popoverMenuViewController?.sourceRect = CGRect(
            x: self.view.frame.width / 2,
            y: self.view.frame.height / 2 + yValue,
            width: 1,
            height: 1)
        presentViewController(
            changeCountry,
            animated: true,
            completion: nil)
        
    }
    func changeCountry(countryName: String, countryId: String) {
        registrationArray[4] = countryName
        RegistrationTable.scrollEnabled = true
        Is_Picker_Up = false
        RegistrationTable.reloadData()
    }
    
    // MARK: - Web Services
    func postRegistration(email: String, name: String, lang: String, address: String, country: String, city: String, mobile: String, passwd: String) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        let params = ["Email": email, "Name": name, "lang_key": lang, "Address": address, "Country": country, "City": city, "MobileNumber": mobile, "Password": passwd]
        
        EX_Network.postJson(WEB_SERVICE + "register.php", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), {
                print(Succeeded)
//                print(Json)
                if let regn = Json["registration_status"] {
                    if let status = regn["Success"] {
                        if status as! Bool == true {
                            
                            if let userId = regn["User_Id"] {
                                USER_ID = userId as? String
                                EX_DEFAULTS.setValue(userId, forKey: "User_Id")
                            }
                            IS_SESSION_EXPIRED = false
                            EX_DEFAULTS.setBool(false, forKey: "Login")
                            
                            IS_GUEST = false
                            EX_DEFAULTS.setValue(false, forKey: "isGuest")
                            
                            EX_DELEGATE.showAlert("Message", message: regn["Message"] as! String, buttonTitle: "OK")
                            self.performSegueWithIdentifier("Home", sender: self)
                        }
                    }else {
                        EX_DELEGATE.showAlert("Message", message: "Registration Failed", buttonTitle: "OK")
                    }
                }
                EX_DELEGATE.removeActivity(self.view)
            })
        }
    }
    func getCountry(language: String) {
        let params = ["lang_key": language]
        
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "country.php", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), {
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Json.count > 0 {
                    self.CountryArray = Json["country"] as! NSArray
                    COUNTRIES = self.CountryArray as? [AnyObject]
                    EX_DEFAULTS.setValue(self.CountryArray, forKey: "CountryLoaded")
                }
            })
        }
        
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class EXRegistrationCell: UITableViewCell {
    
    @IBOutlet weak var placeHolder: EXLabel!
    @IBOutlet weak var inputText: UITextField!
    @IBOutlet weak var popUpLabel: EXLabel!
    @IBOutlet weak var dropDownImage: UIImageView!
    @IBOutlet weak var registerButton: EXButton!
    @IBOutlet weak var countryView: UIView!
    
}
