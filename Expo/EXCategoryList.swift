//
//  EXCategoryList.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXCategoryList: UIViewController, Change_Title_Logo_Delegate {

    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var categoryTitleArray = [String]()
    var categoryThumbNailArray = [String]()
    var categoryIDArray = [String]()
    var CategoryDetails = EXCategoryDetails()
    
    @IBOutlet var categoryTableView: UITableView!
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("CATEGORIES".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        
        //print(IS_LANGUAGE_SWITCHED)
        if IS_LANGUAGE_SWITCHED[1] == 1{
            var params: NSDictionary!
            params  = ["lang_key": LANGUAGE!]
            postCategory(params)
            IS_LANGUAGE_SWITCHED[1] = 0
        }
        
        if categoryTitleArray == []{
            let params = ["lang_key": LANGUAGE!]
            postCategory(params)
        }else {
            categoryTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    func postCategory(params: NSDictionary) {
        
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "categories.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    self.categoryTitleArray = []
                    self.categoryThumbNailArray = []
                    self.categoryIDArray = []
                    if let categoriesData = Json["CATAGORIES"] {
                        print(categoriesData.count)
                        for var i = 0; i < categoriesData.count; i++ {
                            self.categoryTitleArray.insert(categoriesData[i]["Title"]as! String, atIndex: i)
                            self.categoryThumbNailArray.insert(categoriesData[i]["Image"]as! String, atIndex: i)
                            self.categoryIDArray.insert(categoriesData[i]["ID"]as! String, atIndex: i)
                            self.categoryTableView.reloadData()
                        }
                    }
                }
            })
        }
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryIDArray.count
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "Category Cell"
        
        let cell = categoryTableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXCategoryCell
        if LANGUAGE == "en"{
            cell.categoryTitleLabel.textAlignment = NSTextAlignment.Left
        }else{
            cell.categoryTitleLabel.textAlignment = NSTextAlignment.Right
        }
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
        }else {
            loadingImage = "loading_ar"
        }
        
        cell.categoryTitleLabel.text = self.categoryTitleArray[indexPath.row].uppercaseString
        cell.categoryThumbNail.sd_setImageWithURL(NSURL(string: self.categoryThumbNailArray[indexPath.row]), placeholderImage: UIImage(named: loadingImage))
        cell.tag = indexPath.row
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("Category_Details", sender: indexPath)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "Category_Details"{
            CategoryDetails = segue.destinationViewController as! EXCategoryDetails
            let row = (sender as! NSIndexPath).row;
            CategoryDetails.categoryID = self.categoryIDArray[row]
            CategoryDetails.categoryTitle = self.categoryTitleArray[row].uppercaseString
            CategoryDetails.title_Logo_Delegate = self
        }
    }
    

}


class EXCategoryCell: UITableViewCell {
    
    @IBOutlet var categoryTitleLabel: EXLabel!
    
    @IBOutlet var categoryThumbNail: UIImageView!
    
}
