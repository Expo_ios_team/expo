//
//  EXNetwork.swift
//  Expo
//
//  Created by Abdul Malik Aman on 27/04/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXNetwork: NSObject {

    func postJson(url: String, method: String, params: Dictionary<String, AnyObject>, is_Dictionary: Bool, postCompleted : (Succeeded: Bool, Json: AnyObject) -> ()) {
        if IS_REACHABLE == true {
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            let session = NSURLSession.sharedSession()
            
            request.HTTPMethod = method
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted)
            } catch let error as NSError {
                NSErrorPointer().memory = error
                request.HTTPBody = nil
            } // 7.1 try! NSJSONSerialization.dataWithJSONObject(params, options:.PrettyPrinted)
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            print(params, terminator: "")
            let task = session.dataTaskWithRequest(request) {(data, response, error) in
                dispatch_async(dispatch_get_main_queue(), {
                    if response != nil {
                        var jsonResult: AnyObject!
                        if is_Dictionary == true {
                            jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                        }else {
                            jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)) as! NSArray
                        }
                        postCompleted(Succeeded: true, Json: jsonResult)
                    }else {
                        postCompleted(Succeeded: false, Json: "")
                    }
                })
                
            }
            task.resume()
        }else {
            EX_DELEGATE.showAlert("Message", message: "No Internet Connection".localized(LANGUAGE!), buttonTitle: "OK")
        }
    }
    func getJson(url: String, method: String, postCompleted : (Succeeded: Bool, Message: AnyObject) -> ()) {
        let url : String = url
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        request.HTTPMethod = method
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse?, data: NSData?, error: NSError?) -> Void in
            
            dispatch_async(dispatch_get_main_queue(), {
                if response != nil {
                    
                    var jsonResult = NSDictionary()
                    jsonResult = (try! NSJSONSerialization.JSONObjectWithData(data!, options:NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                    postCompleted(Succeeded: true, Message: jsonResult)
                    print(jsonResult)
                }else {
                    postCompleted(Succeeded: false, Message: [:])
                }
            })
        })
    }
    
}
