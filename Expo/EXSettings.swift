//
//  EXSettings.swift
//  Expo
//
//  Created by Ronish on 4/21/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXSettings: UIViewController {

    // MARK: - Variables
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    
    @IBOutlet weak var pushButton: UIButton!
    @IBOutlet weak var newsletterButton: UIButton!
    let buttonOn = "settings_on"
    let buttonOff = "settings_off"
    var is_Push_On = true
    var is_News_On = false
    @IBOutlet weak var englishContainer: UIView!
    @IBOutlet weak var arabicContainer: UIView!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        title_Logo_Delegate!.mainTitleChange!("SETTINGS".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        if LANGUAGE == "en" {
            englishContainer.hidden = false
            arabicContainer.hidden = true
        }else {
            englishContainer.hidden = true
            arabicContainer.hidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        refreshSwitch()
        let params: NSDictionary = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
        postSettings(params)
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Actions
    @IBAction func enableNotification(sender: AnyObject) {
        if is_Push_On == true {
            is_Push_On = false
        }else {
            is_Push_On = true
        }
        let params: NSDictionary = ["User_Id": USER_ID!, "lang_key": LANGUAGE!, "PushnotificationStatus": is_Push_On, "NewsLetterStatus": is_News_On]
        postChangeSettings(params)
//        refreshSwitch()
    }
    
    @IBAction func enableNewsletter(sender: AnyObject) {
        if is_News_On == true {
            is_News_On = false
        }else {
            is_News_On = true
        }
//        refreshSwitch()
        let params: NSDictionary = ["User_Id": USER_ID!, "lang_key": LANGUAGE!, "PushnotificationStatus": is_Push_On, "NewsLetterStatus": is_News_On]
        postChangeSettings(params)
    }
    func refreshSwitch() {
        if is_Push_On == true {
            pushButton.setBackgroundImage(UIImage(named: buttonOn), forState: UIControlState.Normal)
        }else {
            pushButton.setBackgroundImage(UIImage(named: buttonOff), forState: UIControlState.Normal)
        }
        
        if is_News_On == true {
            newsletterButton.setBackgroundImage(UIImage(named: buttonOn), forState: UIControlState.Normal)
        }else {
            newsletterButton.setBackgroundImage(UIImage(named: buttonOff), forState: UIControlState.Normal)
        }
    }
    
    // MARK: - WebServices
    func postSettings(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "settingspage.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let settings = Json["Settings"] {
                        if let push = settings["PushnotificationStatus"] as? Bool {
                            self.is_Push_On = push
                        }
                        if let news = settings["PushnotificationStatus"] as? Bool {
                            self.is_News_On = news
                        }
                    }
                    self.refreshSwitch()
                }
            })
        }
    }

    func postChangeSettings(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "changesettings.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let settings = Json["Settings"] {
                        if let status = settings["SuccessSuccess"] as? Bool {
                            if status == true {
                                self.refreshSwitch()
                            }
                        }
                    }
                    self.refreshSwitch()
                }
            })
        }
    }
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
