//
//  EXCompanyList.swift
//  Expo
//
//  Created by Ronish on 4/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXCompanyList: UIViewController, Change_Title_Logo_Delegate {

    // MARK: - Variables
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var companyListArray: NSArray!
    var passCompanyID: String!
    var passCompanyName: String!
    var CompanyInner = EXCompanyInner()
    @IBOutlet weak var CompanyListCollection: UICollectionView!
    
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("COMPANY LISTS".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        
        if IS_LANGUAGE_SWITCHED[2] == 1{
            let params: NSDictionary = ["lang_key": LANGUAGE!]
            postCompanyList(params)
            IS_LANGUAGE_SWITCHED[2] = 0
        }
        if companyListArray == nil {
            let params: NSDictionary = ["lang_key": LANGUAGE!]
            postCompanyList(params)
        }else {
            CompanyListCollection.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }

    // MARK: - CollectionView Delegate Methods
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if companyListArray != nil {
            if companyListArray.count > 0 {
                return companyListArray.count
            }else {
                return 0
            }
        }else {
            return 0
        }
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = CompanyListCollection.dequeueReusableCellWithReuseIdentifier("companyList", forIndexPath: indexPath) as! EXCompanyListCell
        
        if LANGUAGE == "en"{
            cell.companyName.textAlignment = NSTextAlignment.Left
        }else{
            cell.companyName.textAlignment = NSTextAlignment.Right
        }
        if let company = companyListArray[indexPath.row]["CompanyName"] {
            cell.companyName.text = company.uppercaseString
        }
        if let Logo = companyListArray[indexPath.row]["CompanyImage"] {
            cell.companyImage.sd_setImageWithURL(NSURL(string: Logo as! String), placeholderImage: UIImage(named: "Loading"))
        }
        return cell
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let ID = companyListArray[indexPath.row]["ID"] {
            passCompanyID = ID as? String
        }
        if let company = companyListArray[indexPath.row]["CompanyName"] {
            passCompanyName = company as? String
        }
        performSegueWithIdentifier("companyDetails", sender: self)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let viewWidth = self.CompanyListCollection.frame.width
        let collectionSize = viewWidth / 2 - 3
        let height = collectionSize / 2 + 30
        return CGSizeMake(collectionSize , height)
    }
    // MARK: - WebServices
    func postCompanyList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "companylist.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
//                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    self.companyListArray = Json["CompanyList"] as? NSArray
                }
                self.CompanyListCollection.reloadData()
            })
        }
    }
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "companyDetails" {
            CompanyInner = segue.destinationViewController as! EXCompanyInner
            CompanyInner.CompanyId = passCompanyID
            CompanyInner.CompanyName = passCompanyName
            CompanyInner.title_Logo_Delegate = self
        }
    }

}
class EXCompanyListCell: UICollectionViewCell {
    
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyName: EXLabel!
}
