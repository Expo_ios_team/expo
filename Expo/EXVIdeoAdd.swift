//
//  EXVIdeoAdd.swift
//  Expo
//
//  Created by Ronish on 5/2/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXVIdeoAdd: UIViewController {
    
    var title_Logo_Delegate : Change_Title_Logo_Delegate?

    var VideoListArray: NSArray!
    
    @IBOutlet var VideoListTable: UITableView!
    override func viewWillAppear(animated: Bool) {
        print(LANGUAGE)
        title_Logo_Delegate!.mainTitleChange!("VIDEO ADS".localized(LANGUAGE!))
       // title_Logo_Delegate!.mainTitleChange!("VIDEO ADS".localized("en"))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        
        fetchVideoAds(getPostParam()!)
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.VideoListTable.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if VideoListArray != nil {
            if VideoListArray.count > 0 {
                return VideoListArray.count
            }else {
                return 0
            }
        }else {
            return 0
        }
        
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier = "VideoItem"
        
        let cell = self.VideoListTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXVideoCell
        cell.VideoWebview.scrollView.scrollEnabled = false
        
        let testLink = VideoListArray[indexPath.row]["Video"]as! String
        let testTitle = VideoListArray[indexPath.row]["Title"] as! String
 
        let url = NSURL (string: testLink)
        let requestObj = NSURLRequest(URL: url!)
        cell.VideoWebview.loadRequest(requestObj)
        cell.VideoTitle.text = testTitle
        cell.VideoTitle.textColor =  THEME_COLOUR
        cell.VideoTitle.font = UIFont(name: FONT_NAME_B, size: 20)
        return cell
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    func fetchVideoAds(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "videoads.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                
                if Succeeded == true {
                    if let highlightsData = Json["VideoAds"] {

                        self.VideoListArray = highlightsData as! NSArray
                        self.VideoListTable.reloadData()
                        EX_DELEGATE.removeActivity(self.view)

                        
                    }
                    
                }
            })
        }
    }
    
    func getPostParam() -> NSDictionary?{
        var params : NSDictionary!
        
        params = ["lang_key":LANGUAGE!]
        
        return params
    }
    
}




class EXVideoCell :UITableViewCell
{
    
    @IBOutlet var VideoWebview: UIWebView!
 
    @IBOutlet var VideoTitle: UILabel!
    
    
}



