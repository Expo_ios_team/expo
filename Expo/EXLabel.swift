//
//  EXLabel.swift
//  Expo
//
//  Created by Abdul Malik Aman on 26/04/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXLabel: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.setup()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.setup()
    }
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        self.text = self.text
        self.textColor = self.textColor
        self.font = self.font
        self.layer.display()
        let fontSize = self.font.pointSize;
        self.font = UIFont(name: FONT_NAME, size: fontSize)
    }

}
