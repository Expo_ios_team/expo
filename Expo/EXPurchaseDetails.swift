//
//  EXPurchaseDetails.swift
//  Expo
//
//  Created by Ronish on 5/17/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXPurchaseDetails: UIViewController,Change_Title_Logo_Delegate , UITextFieldDelegate {
    
    @IBOutlet var mainContainerView: UIView!
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var orderConfirmation = EXOrderConfirmation()
    var addressArray: [String] = [String](count: 6, repeatedValue: "")
    var CountryArray: NSArray!
    var is_keyboard_up = false
    var shippingCharge :String!
    var subTotal :String!
    var orderTotal :String!
    var orderId : String!
    var confirmation : String!
    var pageSelector : String!
    
    let placeHolderArray: [String] = ["NAME".localized(LANGUAGE!), "EMAIL".localized(LANGUAGE!), "COUNTRY".localized(LANGUAGE!), "CITY".localized(LANGUAGE!), "ADDRESS".localized(LANGUAGE!), "MOBILE NUMBER".localized(LANGUAGE!)]
    
    @IBOutlet var PurchaseTable: UITableView!
    override func viewWillAppear(animated: Bool) {
        if self.pageSelector == "Billing"{
            title_Logo_Delegate!.mainTitleChange!("SHOPPING CART".localized(LANGUAGE!))
            title_Logo_Delegate!.ChangeLogoImage!(false)
            title_Logo_Delegate!.HideBackButton!(false)
            title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        }else{
            title_Logo_Delegate!.mainTitleChange!("Shipping Address".localized(LANGUAGE!))
            title_Logo_Delegate!.ChangeLogoImage!(false)
            title_Logo_Delegate!.HideBackButton!(false)
            title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(shippingCharge)
        var params: NSDictionary!
        if self.pageSelector == "Billing"{
            params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
            getDeliveryAddressDetailList(params)
        }else{
            params  = ["Order_Id": orderId!, "lang_key": LANGUAGE!]
            getShippingAddressDetailList(params)
        }
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.pageSelector == "Billing"{
            return 9
        }else{
            return 7
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = "textField"
        
        if indexPath.row == 0{
            identifier = "TopHeader"
        }
        else if indexPath.row == 7{
            
            identifier = "PurchaseButton"
        }
            
        else if indexPath.row == 8{
            
            identifier = "BottomCell"
        }
            
        else {
            identifier = "textField"
        }
        
        let cell = PurchaseTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXPurchaseDetailsCell
        
        if (indexPath.row != 0) && (indexPath.row != 7) && (indexPath.row != 8){
            
            if LANGUAGE == "en"{
                cell.placeHolder.textAlignment = NSTextAlignment.Left
                cell.inputText.textAlignment = NSTextAlignment.Left
            }else{
                cell.placeHolder.textAlignment = NSTextAlignment.Right
                cell.inputText.textAlignment = NSTextAlignment.Right
            }
            cell.placeHolder.text = placeHolderArray[(indexPath.row - 1)]
            cell.inputText.tag = (indexPath.row - 1)
            cell.inputText.text = addressArray[(indexPath.row - 1)]
            cell.inputText.autocorrectionType = UITextAutocorrectionType.No
            cell.inputText.keyboardType = UIKeyboardType.Alphabet
            cell.inputText.returnKeyType = UIReturnKeyType.Default // UIReturnKeyType.Next
            cell.inputText.secureTextEntry = false
            cell.inputText.inputAccessoryView = nil
            if indexPath.row == 1 {
                cell.inputText.keyboardType = UIKeyboardType.EmailAddress
                //cell.inputText.text = "testName"
            }else if indexPath.row == 2 {
                
            }else if indexPath.row == 3 {
                
            }else if indexPath.row == 4 {
                
            }else if indexPath.row == 5 {
                
            }else if indexPath.row == 6
            {
                cell.inputText.keyboardType = UIKeyboardType.NumberPad
                cell.inputText.returnKeyType = UIReturnKeyType.Go
                let numberToolbar: UIToolbar! = UIToolbar(frame: CGRectMake(self.view.frame.width - 80, 0, self.view.frame.width, 50))
                numberToolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
                    UIBarButtonItem(title: "Return", style: UIBarButtonItemStyle.Plain, target: self, action: "validateRegistration")]
                cell.inputText.inputAccessoryView = numberToolbar
                //cell.inputText.text = "testMobileNumber"
            }
        }else if indexPath.row == 8 {
            let shipping = String(self.shippingCharge)
            cell.shippingChargeLabel.text = "SHIPPING : ".localized(LANGUAGE!) + shipping + "KD".localized(LANGUAGE!)
            let subTotal = String(self.subTotal)
            cell.subTotalLabel.text = "CART SUBTOTAL : ".localized(LANGUAGE!) + subTotal + "KD".localized(LANGUAGE!)
            let orderTotal = String(self.orderTotal)
            cell.orderTotalLabel.text = "ORDER TOTAL : ".localized(LANGUAGE!) + orderTotal + "KD".localized(LANGUAGE!)
        }else if indexPath.row == 0 {
            if LANGUAGE == "en"{
                cell.headingLabel.textAlignment = NSTextAlignment.Left
            }else{
                cell.headingLabel.textAlignment = NSTextAlignment.Right
            }
            if self.pageSelector == "Billing"{
                cell.headingLabel.text = "Enter Delivery Address".localized(LANGUAGE!)
            }else{
                cell.headingLabel.text = "Shipping Address".localized(LANGUAGE!)
            }
        }else if indexPath.row == 7{
            cell.purchaseButton.setTitle("PURCHASE".localized(LANGUAGE!), forState:.Normal)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 8 {
            return self.mainContainerView.frame.width * 0.306666667
        }
        else
        if indexPath.row == 0 {
            return self.mainContainerView.frame.width * 0.133333334
        }
        else
            if indexPath.row == 7 {
                return self.mainContainerView.frame.width * 0.153333334
            }
        
        else {
            return self.mainContainerView.frame.width * 0.2466666666666
        }
    }
    
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        PurchaseTable.frame.size.height = self.mainContainerView.frame.size.height
        PurchaseTable.reloadData()
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField) {
        is_keyboard_up = true
        PurchaseTable.frame.size.height = self.view.frame.height - 150
        scrollToTop(textField.tag, section: 0, table: PurchaseTable)
//        if textField == countryText {
//            textField.endEditing(true)
//            PurchaseTable.scrollEnabled = false
//        }
        viewDidLayoutSubviews()
    }
    func textFieldDidEndEditing(textField: UITextField) {
        is_keyboard_up = false
        print(textField.tag)
        addressArray[textField.tag] = EX_DELEGATE.TrimString(textField.text!)
        viewDidLayoutSubviews()
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        return true
    }
    
    func scrollToTop(row: Int, section: Int, table: UITableView) {
        let indexPath = NSIndexPath(forRow: row, inSection: section)
        table.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Top, animated: true)
    }
    
    func validateRegistration() {
        self.view.endEditing(true)
        PurchaseTable.frame.size.height = self.mainContainerView.frame.size.height
    }
    
    @IBAction func purchaseButtonAction(sender: UIButton) {
        self.view.endEditing(true)
        print(addressArray)
        var message: String!
        
        for i in 0..<addressArray.count {
            if EX_DELEGATE.TrimString(addressArray[i]) == "" {
//                message = "\(placeHolderArray[i].capitalizedString) cannot be empty"
                message = placeHolderArray[i].capitalizedString + " cannot be empty".localized(LANGUAGE!)
                break
            }
        }
        
        if message == nil {
            if EX_DELEGATE.isValidEmail(addressArray[1]) == false {
                message = "Email not valid".localized(LANGUAGE!)
            }else if EX_DELEGATE.TrimNumbers(addressArray[5]) == false {
                message = "Phone Number cannot be characters".localized(LANGUAGE!)
            }else if addressArray[5].characters.count < 8 {
                message = "Please enter a valid Phone Number".localized(LANGUAGE!)
            }
        }
        
        if message == nil {
            postDeliveryAddressDetailList(addressArray[1], name: addressArray[0], lang: LANGUAGE!, address: addressArray[4], country: addressArray[2], city: addressArray[3], mobile: addressArray[5], userId: USER_ID!, shippingCharge: self.shippingCharge, subTotal: self.subTotal, orderTotal: self.orderTotal)
        }else {
            EX_DELEGATE.showAlert("Message", message: message, buttonTitle: "OK")
        }
    }
    
    // MARK: - Web Services
    
    func postDeliveryAddressDetailList(email: String, name: String, lang: String, address: String, country: String, city: String, mobile: String,userId: String, shippingCharge: String, subTotal: String, orderTotal: String) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        let params = ["lang_key": lang, "User_Id": userId, "Name": name,"Email": email, "Address": address, "Country": country, "City": city, "Mobile": mobile, "SubTotal": subTotal, "ShippingCharge": shippingCharge, "OrderType": orderTotal, ]
        
        EX_Network.postJson(WEB_SERVICE + "ordersubmit.php", method: "POST", params: params, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), {
            print(Succeeded)
//            print(Json)
                print(params)
                if let orderStatusData = Json["order_status"] {
                    self.orderId = orderStatusData["order_id"] as! String
                    self.confirmation = orderStatusData["message"] as! String
                    self.performSegueWithIdentifier("orderConfirmation", sender: self)
                }
            EX_DELEGATE.removeActivity(self.view)
            })
        }
    }
    
    
    func getDeliveryAddressDetailList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "get_deliveryaddress.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(params)
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let addressData = Json["MyAccount"] {
                        self.addressArray[0] = addressData["Name"]as! String
                        self.addressArray[1] = addressData["Email"]as! String
                        self.addressArray[2] = addressData["Country"]as! String
                        self.addressArray[3] = addressData["City"]as! String
                        self.addressArray[4] = addressData["Address"]as! String
                        self.addressArray[5] = addressData["Mobile"]as! String
                        self.PurchaseTable.reloadData()
                    }
                }else{
                    //alert
                    EX_DELEGATE.showAlert("Message", message: "No Internet Connection".localized(LANGUAGE!), buttonTitle: "OK")
                }
            })
        }
    }
    
    func getShippingAddressDetailList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "order_shippingaddress.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(params)
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let addressData = Json["ShippingAddress"] {
                        self.addressArray[0] = addressData["Name"]as! String
                        self.addressArray[1] = addressData["Email"]as! String
                        self.addressArray[2] = addressData["Country"]as! String
                        self.addressArray[3] = addressData["City"]as! String
                        self.addressArray[4] = addressData["Address"]as! String
                        self.addressArray[5] = addressData["Mobile"]as! String
                        self.PurchaseTable.reloadData()
                    }
                }else{
                    //alert
                    EX_DELEGATE.showAlert("Message", message: "No Internet Connection".localized(LANGUAGE!), buttonTitle: "OK")
                }
            })
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "orderConfirmation" {
            orderConfirmation = segue.destinationViewController as! EXOrderConfirmation
            orderConfirmation.orderId = self.orderId
            orderConfirmation.confirmation = self.confirmation
            orderConfirmation.title_Logo_Delegate = self
        }
    }
    
    
}


class EXPurchaseDetailsCell: UITableViewCell {
    
    @IBOutlet weak var placeHolder: EXLabel!
    @IBOutlet weak var inputText: UITextField!
    @IBOutlet var shippingChargeLabel: UILabel!
    @IBOutlet var subTotalLabel: UILabel!
    @IBOutlet var orderTotalLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var purchaseButton: UIButton!
}


