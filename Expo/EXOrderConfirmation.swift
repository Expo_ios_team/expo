//
//  EXOrderConfirmation.swift
//  Expo
//
//  Created by Ronish on 5/17/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXOrderConfirmation: UIViewController,Change_Title_Logo_Delegate {

    // MARK: - Variables
    
    @IBOutlet weak var orderConfirmation: UIWebView!
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var orderDetails = EXOrderDetails()
    var orderId = ""
    var confirmation = ""
    
    @IBOutlet weak var orderDetailsButton: UIButton!
    // MARK: - View Methods
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("ORDER CONFIRMATION".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 0)
        
        self.orderDetailsButton.setTitle("ORDER DETAILS".localized(LANGUAGE!), forState: .Normal)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        orderConfirmation?.loadHTMLString(confirmation, baseURL: nil)
//        orderConfirmation.text = confirmation
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }

    // MARK: - Action
    @IBAction func confirmOrder(sender: AnyObject) {
        performSegueWithIdentifier("orderDetails", sender: self)
    }
    
    // MARK: - WebServices
    func postOrderConfirmation(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    
                }
            })
        }
    }
    
    
    // MARK: - System
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "orderDetails" {
            orderDetails = segue.destinationViewController as! EXOrderDetails
            orderDetails.orderId = self.orderId
            orderDetails.title_Logo_Delegate = self
        }
    }
    

}
