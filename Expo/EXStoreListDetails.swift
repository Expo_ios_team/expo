//
//  EXStoreListDetails.swift
//  Expo
//
//  Created by Ronish on 5/9/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXStoreListDetails: UIViewController,UIScrollViewDelegate, UIGestureRecognizerDelegate,Change_Title_Logo_Delegate {
    
    var productId : String = ""
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var currentProductImageCount: Int = 0
    var cartCount : String!
    var productTitle : String!
    var availableStock : String?
    var productName : String!
    var productImage : [String]!
    var productPrice : Float!
    var productDescription : String!
    var pageIndex = 0
    var shoppingCart = EXShoppingCart()
    
    @IBOutlet var outOfStockLabel: UILabel!
    @IBOutlet var addToCartButton: UIButton!
    @IBOutlet var productNameLabel: EXLabel_Regular!
    @IBOutlet var priceLabel: EXLabel!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var pagerDotBackView: UIView!
    @IBOutlet var pagerBackView: UIView!
    @IBOutlet var imageScroll: UIScrollView!
    @IBOutlet var highlightImage: [UIImageView]!
    @IBOutlet var productImageScrollBackView: UIView!
    @IBOutlet var cartCountLabel: UILabel!
    @IBOutlet var mainScrollStoreListDetails: UIScrollView!
    @IBOutlet var scrollContentstoreInnerView: UIView!
    
    @IBOutlet var spaceAdjustingView: UIView!
    @IBOutlet var cartBackView: UIView!
    @IBOutlet var cartButton: UIButton!
    
    @IBOutlet var changeQuantityBackView: UIView!
    @IBOutlet var priceQuantityBackView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.productId)
        
//        NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "autoScroll", userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("")
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 1)
        self.mainScrollStoreListDetails.delegate = self
//        self.addToCartButton.titleLabel?.text = "  Add To Cart  ".localized(LANGUAGE!)
        self.addToCartButton.setTitle("  Add To Cart  ".localized(LANGUAGE!), forState:.Normal)
        self.addToCartButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        if IS_CART_UPDATED[1] == 1{
            var params: NSDictionary!
            params  = ["User_Id": USER_ID!,"Product_Id": self.productId, "lang_key": LANGUAGE!]
            postStoreDetailList(params)
            IS_CART_UPDATED[1] = 0
            viewDidLoad()
        }
        
        var params: NSDictionary!
        if productTitle == nil {
            params  = ["User_Id": USER_ID!,"Product_Id": self.productId, "lang_key": LANGUAGE!]
            postStoreDetailList(params)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.mainScrollStoreListDetails.contentSize = CGSizeMake(self.mainScrollStoreListDetails.frame.size.width, self.scrollContentstoreInnerView.frame.size.height)
        
        if LANGUAGE == "en"{
            self.cartBackView.frame = CGRectMake((self.spaceAdjustingView.frame.width - self.cartBackView.frame.width) + 10, self.cartBackView.frame.origin.y, self.cartBackView.frame.size.width, self.cartBackView.frame.size.height)
            self.productNameLabel.textAlignment = NSTextAlignment.Left
            self.priceLabel.frame = CGRectMake(8, self.priceLabel.frame.origin.y, self.priceLabel.frame.size.width, self.priceLabel.frame.size.height)
            self.priceLabel.textAlignment = NSTextAlignment.Left
            self.addToCartButton.frame = CGRectMake((self.priceQuantityBackView.frame.size.width - self.addToCartButton.frame.size.width)-8 , self.addToCartButton.frame.origin.y, self.addToCartButton.frame.size.width, self.addToCartButton.frame.size.height)
            
            self.changeQuantityBackView.frame = CGRectMake((self.priceLabel.frame.origin.x + self.priceLabel.frame.size.width) + 2 , self.changeQuantityBackView.frame.origin.y, self.changeQuantityBackView.frame.size.width, self.changeQuantityBackView.frame.size.height)
            
            self.descriptionTextView.textAlignment = NSTextAlignment.Left
        }else{
            self.cartBackView.frame = CGRectMake(self.spaceAdjustingView.frame.origin.x + 10, self.cartBackView.frame.origin.y, self.cartBackView.frame.size.width, self.cartBackView.frame.size.height)
            self.productNameLabel.textAlignment = NSTextAlignment.Right
            self.priceLabel.frame = CGRectMake((self.priceQuantityBackView.frame.size.width - self.priceLabel.frame.size.width)-8 , self.priceLabel.frame.origin.y, self.priceLabel.frame.size.width, self.priceLabel.frame.size.height)
            self.priceLabel.textAlignment = NSTextAlignment.Right
            self.addToCartButton.frame = CGRectMake(8 , self.addToCartButton.frame.origin.y, self.addToCartButton.frame.size.width, self.addToCartButton.frame.size.height)
            
            self.changeQuantityBackView.frame = CGRectMake((self.addToCartButton.frame.origin.x + self.addToCartButton.frame.size.width) + 5 , self.changeQuantityBackView.frame.origin.y, self.changeQuantityBackView.frame.size.width, self.changeQuantityBackView.frame.size.height)
            
            self.descriptionTextView.textAlignment = NSTextAlignment.Right
        }
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    

    // MARK: - WebServices
    func postStoreDetailList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "storelisting_details.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let storeDetailsListData = Json["Storelisting_ProductDetails"] {
                        
                        self.cartCount = storeDetailsListData["CartCount"] as? String
                        
                        self.productTitle = storeDetailsListData["Title"] as? String
                        
                        if self.productTitle != nil {
                            self.title_Logo_Delegate!.mainTitleChange!(self.productTitle)
                        }
                        self.availableStock = storeDetailsListData["PurchaseQuantity"] as? String
                        
                        self.productName = storeDetailsListData["ProductName"] as? String
                        
                        if (storeDetailsListData["Productimages"] as? [String])?.count > 0{
                            self.productImage = (storeDetailsListData["Productimages"] as? [String])!
                        }
                        
                        self.productPrice = (storeDetailsListData["Price"] as? NSString)?.floatValue
                        
                        self.productDescription = storeDetailsListData["Description"] as? String
                        
                        if self.cartCount == "0"{
                            self.cartCountLabel.text = ""
                        }else{
                            self.cartCountLabel.text = self.cartCount
                        }
                        
                        if Int(self.availableStock!)! == 0{
                            print(self.availableStock)
                            self.outOfStockLabel.hidden = false
                        }else {
                            self.outOfStockLabel.hidden = true
                        }
                        self.outOfStockLabel.text = "OUT OF STOCK!".localized(LANGUAGE!)
                        self.loadListView()
                    }
                }
            })
        }
    }
    
    func postAddToCart(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "addtocart.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(params)
                print(Succeeded)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let addToCartData = Json["AddtoCartMessage"] {
                        
                        if ((addToCartData["Success"]as? Bool) != nil){
                            self.cartCount = addToCartData["BasketCount"] as? String
                            self.cartCountLabel.text = String(self.cartCount)
                            IS_CART_UPDATED = [1, 1]
                        }
                        EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: (addToCartData["Message"]as? String)!, buttonTitle: "OK")
                    }
                }else{
                    //alert
                    EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "No Internet Connection".localized(LANGUAGE!), buttonTitle: "OK")
                }
            })
        }
    }
    
    func loadListView(){
        self.productNameLabel.text = self.productName
        self.productNameLabel.textColor = THEME_COLOUR
        let price = String(self.productPrice)
        self.priceLabel.text = "Price : ".localized(LANGUAGE!) + price + "KD".localized(LANGUAGE!)
        self.priceLabel.adjustsFontSizeToFitWidth = true
        self.priceLabel.textColor = THEME_COLOUR
        if Int(self.availableStock!) > 0{
            self.quantityLabel.text = "1"
        }else {
            self.quantityLabel.text = "0"
        }
        print(self.productDescription)
        self.descriptionTextView.text = self.productDescription
        self.descriptionTextView.textColor = THEME_COLOUR
        self.descriptionTextView.font = UIFont(name: FONT_NAME_R, size: 12)
        print(self.productImage)
        
        ///////////////
        
        //self.productImage = Json["Highlights"] as! [String]
        
        let width = self.productImageScrollBackView.frame.size.width
        self.imageScroll.contentSize = CGSizeMake(CGFloat(width) * CGFloat(self.productImage.count), 0)
        self.highlightImage = [UIImageView](count: self.productImage.count, repeatedValue: UIImageView())
        
        var loadingImage: String!
        if LANGUAGE == "en" {
            loadingImage = "Loading"
        }else {
            loadingImage = "loading_ar"
        }
        
        for i in 0..<self.productImage.count {
            
            self.highlightImage[i] = UIImageView(frame: CGRectMake(width * CGFloat(i), 0, width, self.imageScroll.frame.size.height))
            self.highlightImage[i].image = UIImage(named: loadingImage)
//            self.highlightImage[i].contentMode = UIViewContentMode.ScaleAspectFit
            self.highlightImage[i].clipsToBounds = true
            self.highlightImage[i].tag = i
            self.imageScroll.addSubview(self.highlightImage[i])
            //                            self.highlightImage[i].sd_setImageWithURL(NSURL(string: highlightsData[0]as! String), placeholderImage: UIImage(named: "Loading"))
        }
        for i in 0..<self.highlightImage.count {
            if let url = NSURL(string: self.productImage[i]) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)) {
                    if let data = NSData(contentsOfURL: url) {
                        dispatch_async(dispatch_get_main_queue()) {
                            self.highlightImage[i].image = UIImage(data: data)
                        }
                    }
                }
            }
        }
        
        self.pageIndex = 0
        //                        self.highlightImage[0].sd_setImageWithURL(NSURL(string: highlightsData[0]as! String), placeholderImage: UIImage(named: "Loading"))
        self.loadPager(self.productImage.count, position: 0)

        
        ///////////////
    }
    @IBAction func plus_MinusButtonAction(sender: UIButton) {
        if sender.tag == 0{
            if (Int(self.quantityLabel.text!) > 0){
                let quantity = Int(self.quantityLabel.text!)! as Int
                self.quantityLabel.text = String(quantity - 1)
            }else{
                //alert
                if Int(self.availableStock!)! == 0{
                    EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Out of stock!".localized(LANGUAGE!), buttonTitle: "OK")
                }else {
                    EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Quantity cannot be less than zero".localized(LANGUAGE!), buttonTitle: "OK")
                }
            }
        }else {
            if (Int(self.quantityLabel.text!) < Int(self.availableStock!)){
                let quantity = Int(self.quantityLabel.text!)! as Int
                self.quantityLabel.text = String(quantity + 1)
            }else{
                //alert
                if Int(self.availableStock!)! == 0{
                    EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Out of stock!".localized(LANGUAGE!), buttonTitle: "OK")
                }else {
                    EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Available quantity is \(self.availableStock!)".localized(LANGUAGE!), buttonTitle: "OK")
                }
            }
        }
    }
    
    @IBAction func addToCartButtonAction(sender: UIButton) {
        var params: NSDictionary!
        if (Int(self.quantityLabel.text!) > 0){
            if let userId = EX_DEFAULTS.valueForKey("User_Id") {
                params  = ["User_Id": USER_ID!,"Product_Id": self.productId, "lang_key": LANGUAGE!,"Quantity": self.quantityLabel.text!]
                //            params  = ["User_Id": userId,"Product_Id": self.productId, "lang_key": LANGUAGE!,"Quantity": self.quantityLabel.text!]
                print(params)
                postAddToCart(params)
            }else {
                params  = ["User_Id": USER_ID!,"Product_Id": self.productId, "lang_key": LANGUAGE!,"Quantity": self.quantityLabel.text!]
                //params  = ["Product_Id": self.productId, "lang_key": LANGUAGE!,"Quantity": self.quantityLabel.text!]
                print(params)
                postAddToCart(params)
            }
        }else{
            //alert
            if Int(self.availableStock!)! == 0{
                EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Out of stock!".localized(LANGUAGE!), buttonTitle: "OK")
            }else {
                EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Quantity cannot be zero".localized(LANGUAGE!), buttonTitle: "OK")
            }
        }
        
    }
    
    func loadPager(count: Int, position: Int){
        if count >= 1 {
            self.pagerDotBackView.frame = CGRectMake(0, 0, (self.pagerBackView.frame.size.height * CGFloat(count)), self.pagerBackView.frame.size.height)
            self.pagerDotBackView.center = CGPointMake(self.pagerBackView.center.x, self.pagerDotBackView.center.y)
            var j = 0
            for (var i = 0; i < count; i++) {
                let dotButton : UIButton = UIButton(frame: CGRectMake(CGFloat(j), 0, self.pagerBackView.frame.size.height, self.pagerBackView.frame.size.height))
                if i == position {
                    dotButton.selected = true
                }
                dotButton.tag = i
                dotButton.setImage(UIImage(named: "pager"), forState: UIControlState.Normal)
                dotButton.setImage(UIImage(named: "active"), forState: UIControlState.Selected)
                dotButton.addTarget(self, action: "pagerDotButtonSelected:", forControlEvents: .TouchUpInside)
                j = j + Int(self.pagerDotBackView.frame.size.height)
                self.pagerDotBackView.addSubview(dotButton)
            }
        }
    }
    
    func pagerDotButtonSelected(sender: UIButton){
        currentProductImageCount = sender.tag
        changeHighlightImage(sender.tag)
        let width = self.productImageScrollBackView.frame.size.width
        imageScroll.setContentOffset(CGPointMake(width * CGFloat(sender.tag), 0), animated: true)
        loadPager(self.productImage.count, position: sender.tag)
    }
    func autoScroll() {
        let width = self.productImageScrollBackView.frame.size.width
        if pageIndex != productImage.count-1 {
            imageScroll.setContentOffset(CGPointMake(width, 0), animated: true)
            pageIndex = pageIndex + 1
        }else {
            imageScroll.setContentOffset(CGPointMake(0, 0), animated: false)
            pageIndex = 0
        }
    }
    
    @IBAction func previous_NextButtonAction(sender: UIButton) {
        
        let scrollWidth: CGFloat = imageScroll.frame.size.width;
        let contentSize = imageScroll.contentSize.width
        print(imageScroll.contentSize.width)
        let page: CGFloat = (imageScroll.contentOffset.x + (0.5 * scrollWidth)) / scrollWidth
        let width = self.productImageScrollBackView.frame.size.width
        if sender.tag == 1 {
            if pageIndex != productImage.count-1 {
                imageScroll.setContentOffset(CGPointMake(width, 0), animated: true)
                pageIndex = pageIndex + 1
            }
            
            
            //            if currentProductImageCount > 0 {
            //                changeHighlightImage(--currentProductImageCount)
            //                loadPager(self.currentProductImageCount.count, position: currentProductImageCount)
            //            }
        } else {
            
            if pageIndex != 0 {
                let contect = Int(imageScroll.contentOffset.x) - Int(width)
                imageScroll.setContentOffset(CGPointMake(CGFloat(contect), 0), animated: true)
                pageIndex = pageIndex - 1
            }
            
            //            if currentProductImageCount < (currentProductImageCount.count-1) {
            //                changeHighlightImage(++currentProductImageCount)
            //                loadPager(self.currentProductImageCount.count, position: currentProductImageCount)
            //            }
        }
        loadPager(self.productImage.count, position: pageIndex)
    }
    
    func changeHighlightImage(position: Int){
        print(position)
        
        //        self.highlightImage[0].sd_setImageWithURL(NSURL(string: currentProductImageCount[position]), placeholderImage: UIImage(named: "Loading"))
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Left:
                print("Swiped left")
                if currentProductImageCount < (productImage.count-1) {
                    changeHighlightImage(++currentProductImageCount)
                    loadPager(self.productImage.count, position: currentProductImageCount)
                }
            case UISwipeGestureRecognizerDirection.Down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.Right:
                print("Swiped Right")
                if currentProductImageCount > 0 {
                    changeHighlightImage(--currentProductImageCount)
                    loadPager(self.productImage.count, position: currentProductImageCount)
                }
            case UISwipeGestureRecognizerDirection.Up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView == imageScroll {
            let pageWidth = imageScroll.frame.size.width
            let page = Int(floor((imageScroll.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
            pageIndex = page
            print("Page Index \(pageIndex)")
            loadPager(productImage.count, position: pageIndex)
        }
    }
    
    @IBAction func cartButtonAction(sender: UIButton) {
        self.tabBarController?.selectedIndex = 14
//        performSegueWithIdentifier("shoppingCartPage", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "shoppingCartPage" {
            shoppingCart = segue.destinationViewController as! EXShoppingCart
            shoppingCart.title_Logo_Delegate = self
        }
    }
    

}
