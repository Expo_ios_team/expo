//
//  EXShoppingCart.swift
//  Expo
//
//  Created by Ronish on 5/17/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXShoppingCart: UIViewController,Change_Title_Logo_Delegate {
    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    
    @IBOutlet var cartTable: UITableView!
    @IBOutlet var mainContainerView: UIView!
    var cartCount : String!
    var shippingCharge :String!
    var subTotal :String!
    var orderTotal :String!
    var productDetails : [NSMutableDictionary]!
    var purchaseDetails = EXPurchaseDetails()
    var timerForCartUpdate = NSTimer()
    //var quantityArray = []
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("SHOPPING CART".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 2)
        
        var params: NSDictionary!
        params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
        postShoppingCartList(params)
        
//        var params: NSDictionary!
//        if subTotal == nil {
//            params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
//            postShoppingCartList(params)
//        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.hidden = true
        var params: NSDictionary!
        
        params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
        postShoppingCartList(params)
        
//        if subTotal == nil {
//            params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!]
//            postShoppingCartList(params)
//        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    // MARK: - Actions
    
    @IBAction func continueShoppingButtonAction(sender: UIButton) {
//      let changeCountry: EXStoreList = storyboard!.instantiateViewControllerWithIdentifier("EXStoreList") as! EXStoreList
//        self.navigationController?.popToRootViewControllerAnimated(true)
        self.tabBarController?.selectedIndex = 3
    }
    
    @IBAction func purchaseButtonAction(sender: UIButton) {
        performSegueWithIdentifier("Purchase", sender: self)
    }
    
    @IBAction func closeButtonAction(sender: UIButton) {
        print(sender.tag)
        let productId = (self.productDetails[sender.tag])["ID"]as! String
        print(productId)
        var params: NSDictionary!
        if let userId = EX_DEFAULTS.valueForKey("User_Id") {
            params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!, "Product_Id": productId]
            //params  = ["User_Id": userId, "lang_key": LANGUAGE!, "productId" : productId]
            postRemoveCartItem(params)
        }else {
            params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!, "Product_Id": productId]
            //params  = ["Product_Id": self.productId, "lang_key": LANGUAGE!, "productId" : productId]
            postRemoveCartItem(params)
        }
        //print(params)
    }
    
    @IBAction func minusButtonAction(sender: UIButton) {
        //let availableStock = (self.productDetails[sender.tag])["AvailableStock"]!
        let availableStock = self.productDetails[sender.tag]["PurchaseQuantity"] as! String
        let quantity = (self.productDetails[sender.tag])["Quantity"]as! String
        //let productId = (self.productDetails[sender.tag])["ID"]as! String
        print(quantity)
        if Int(quantity) == 1 {
            
        }else if Int(quantity) > Int(availableStock) {
            EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Quantity not available".localized(LANGUAGE!), buttonTitle: "OK")
            self.productDetails[sender.tag]["Quantity"] = String(Int(quantity)! - 1)
            //let quantityForTimer = (self.productDetails[sender.tag])["Quantity"]as! String
            //print(temp)
            self.cartTable.reloadData()
            //timerForCartUpdate.invalidate()
            //timerForCartUpdate = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("timerCounter:"), userInfo:["SenderTag":sender.tag, "Quantity" : quantityForTimer], repeats: false)
        }
        else {
            print(Int(quantity)! - 1)
            print(String(Int(quantity)! - 1))
            self.productDetails[sender.tag]["Quantity"] = String(Int(quantity)! - 1)
            let quantityForTimer = (self.productDetails[sender.tag])["Quantity"]as! String
            //print(temp)
            self.cartTable.reloadData()
            timerForCartUpdate.invalidate()
            timerForCartUpdate = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("timerCounter:"), userInfo:["SenderTag":sender.tag, "Quantity" : quantityForTimer], repeats: false)
        }
    }
    
    @IBAction func plusButtonAction(sender: UIButton) {
        print(self.productDetails[sender.tag]["PurchaseQuantity"])
        let availableStock = self.productDetails[sender.tag]["PurchaseQuantity"] as! String
        let quantity = (self.productDetails[sender.tag])["Quantity"]as! String
        let productId = (self.productDetails[sender.tag])["ID"]as! String
        print(quantity)
        if Int(quantity) >= Int(availableStock) {
            EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: "Available quantity is \(quantity)".localized(LANGUAGE!), buttonTitle: "OK")
        }else {
            print(Int(quantity)! + 1)
            print(String(Int(quantity)! + 1))
            self.productDetails[sender.tag]["Quantity"] = String(Int(quantity)! + 1)
            let quantityForTimer = (self.productDetails[sender.tag])["Quantity"]as! String
            //print(temp)
            self.cartTable.reloadData()
            timerForCartUpdate.invalidate()
            timerForCartUpdate = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("timerCounter:"), userInfo:["SenderTag":sender.tag, "Quantity" : quantityForTimer], repeats: false)
        }
    }
    
    func timerCounter(timer: NSTimer){
        print(timer.userInfo!)
        let sendertag = timer.userInfo!["SenderTag"]as! Int
        //print(sender.tag)
        //let availableStock = (self.productDetails[sendertag])["AvailableStock"]!
        let quantity = (self.productDetails[sendertag])["Quantity"]as! String
        let productId = (self.productDetails[sendertag])["ID"]as! String
        print(quantity)
        var params: NSDictionary!
        params  = ["User_Id": USER_ID!, "lang_key": LANGUAGE!, "Product_Id": productId, "Quantity" :quantity]
        postUpdateCartItem(params)
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cartCount != nil  {
            if Int(cartCount) == 0{
                return 1
            }else {
                return productDetails.count+1
            }
        } else{
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = ""
        if LANGUAGE == "en"{
            if Int(cartCount) == 0{
                identifier = "EmptyCart"
            }else{
                if indexPath.row == productDetails.count {
                    identifier = "cartDetails"
                }else {
                    identifier = "shoppingCartItems"
                }
            }
        }else {
            if Int(cartCount) == 0{
                identifier = "EmptyCart_ar"
            }else{
                if indexPath.row == productDetails.count {
                    identifier = "cartDetails_ar"
                }else {
                    identifier = "shoppingCartItems_ar"
                }
            }
        }
        
        let cell = cartTable.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXShoppingCartCell
        
        if Int(cartCount) == 0 {
            
        }else {
            if indexPath.row == productDetails.count {
                print(self.shippingCharge)
                let shipping = String(self.shippingCharge)
//                if shipping == "0"{
//                    cell.shippingChargeLabel.text = "SHIPPING : " + "0" + "KD"
//                }else{
//                    cell.shippingChargeLabel.text = "SHIPPING : " + shipping + "KD"
//                }
                cell.shippingChargeLabel.text = "SHIPPING : ".localized(LANGUAGE!) + shipping + "KD".localized(LANGUAGE!)
                let subTotal = String(self.subTotal)
                cell.subTotalLabel.text = "CART SUBTOTAL : ".localized(LANGUAGE!) + subTotal + "KD".localized(LANGUAGE!)
                let orderTotal = String(self.orderTotal)
                cell.orderTotalLabel.text = "ORDER TOTAL : ".localized(LANGUAGE!) + orderTotal + "KD".localized(LANGUAGE!)
            }else {
                var loadingImage: String!
                if LANGUAGE == "en" {
                    loadingImage = "Loading"
                }else {
                    loadingImage = "loading_ar"
                }
                cell.productImageView.sd_setImageWithURL(NSURL(string: (self.productDetails[indexPath.row])["Image"]as! String), placeholderImage: UIImage(named: loadingImage))
                let flag = ((self.productDetails[indexPath.row])["Status"]) as? Bool
                if (flag == false) {
                    cell.warningLabel.hidden = false
                    cell.warningLabel.text = ((self.productDetails[indexPath.row])["Message"]) as? String
                    cell.warningLabel.adjustsFontSizeToFitWidth = true
                }
                print((self.productDetails[indexPath.row])["Quantity"])
                cell.quantityLabel.text = String(((self.productDetails[indexPath.row])["Quantity"])as! String)
                cell.brandNameLabel.text = (self.productDetails[indexPath.row])["BrandName"]as? String
                cell.itemNameLabel.text = (self.productDetails[indexPath.row])["ItemName"]as? String
                let price = (self.productDetails[indexPath.row])["Price"]as? String
                cell.productPriceLabel.text = "Price : ".localized(LANGUAGE!) + price! + "KD".localized(LANGUAGE!)
                cell.productPriceLabel.adjustsFontSizeToFitWidth = true
                cell.closeButton.tag = indexPath.row
                cell.minusButton.tag = indexPath.row
                cell.plusButton.tag = indexPath.row
            }
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if Int(cartCount) == 0{
            return self.mainContainerView.frame.size.width/2
        }else {
            if indexPath.row == productDetails.count {
                return 220
            }else {
                return self.mainContainerView.frame.size.width/2
            }
        }
    }
    
    // MARK: - WebServices
    func postShoppingCartList(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "shoppingcart.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                print(params)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let ShoppingCartListData = Json["Cart"] {
                        
                        self.cartCount = ShoppingCartListData["CartCount"] as? String
                        
                        self.shippingCharge = ShoppingCartListData["ShippingCharge"] as? String
                        
                        self.subTotal = ShoppingCartListData["SubTotal"] as? String
                        
                        self.orderTotal = ShoppingCartListData["OrderTotal"] as? String
                        
                        if (ShoppingCartListData["ProductList"] as? [NSDictionary])?.count > 0{
                            self.productDetails = (ShoppingCartListData["ProductList"] as? [NSMutableDictionary])!
                        }
                        
                        self.cartTable.reloadData()
                    }
                }
            })
        }
    }
    
    func postRemoveCartItem(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "removecartitem.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(params)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let removeCartItemData = Json["AddtoCartMessage"] {
                        
                        if let status = removeCartItemData["Success"]{
                            if status as! Bool == true {
                                let message = removeCartItemData["Message"] as! String
                                EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: message , buttonTitle: "OK")
                                IS_CART_UPDATED = [1, 1]
                                self.viewDidLoad()
                            }else{
                                let message = removeCartItemData["Message"] as! String
                                EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: message , buttonTitle: "OK")
                            }
                        }
                    }
                }
            })
        }
    }
    
    func postUpdateCartItem(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "cartupdate.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                //print(Succeeded)
                print(params)
                print(Json)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let removeCartItemData = Json["AddtoCartMessage"] {
                        
                        if let status = removeCartItemData["Success"]{
                            if status as! Bool == true {
                                let message = removeCartItemData["Message"] as! String
                                EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: message , buttonTitle: "OK")
                                IS_CART_UPDATED = [1, 1]
                                self.viewDidLoad()
                            }else{
                                let message = removeCartItemData["Message"] as! String
                                EX_DELEGATE.showAlert("Message".localized(LANGUAGE!), message: message , buttonTitle: "OK")
                            }
                        }
                    }
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Purchase" {
            purchaseDetails = segue.destinationViewController as! EXPurchaseDetails
            purchaseDetails.shippingCharge = self.shippingCharge
            purchaseDetails.orderTotal = self.orderTotal
            purchaseDetails.subTotal = self.subTotal
            purchaseDetails.title_Logo_Delegate = self
            purchaseDetails.pageSelector = "Billing"
        }
    }
}

class EXShoppingCartCell: UITableViewCell {
    
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var brandNameLabel: UILabel!
    @IBOutlet var itemNameLabel: UILabel!
    @IBOutlet var productPriceLabel: UILabel!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var shippingChargeLabel: UILabel!
    @IBOutlet var subTotalLabel: UILabel!
    @IBOutlet var orderTotalLabel: UILabel!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var minusButton: UIButton!
    @IBOutlet var plusButton: UIButton!
    @IBOutlet weak var warningLabel: UILabel!
}

