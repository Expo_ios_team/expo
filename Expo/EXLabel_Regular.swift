//
//  EXLabel_Regular.swift
//  Expo
//
//  Created by Ronish on 5/5/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXLabel_Regular: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.setup()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        self.setup()
    }
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    func setup() {
        self.text = self.text
        self.textColor = self.textColor
        self.font = self.font
        self.layer.display()
        let fontSize = self.font.pointSize;
        self.font = UIFont(name: FONT_NAME_R, size: fontSize)
    }

}
