//
//  EXOrderDetails.swift
//  Expo
//
//  Created by Ronish on 5/20/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit

class EXOrderDetails: UIViewController,Change_Title_Logo_Delegate {

    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    var dateOfPurchase : String!
    var orderId : String!
    var shippingCharge : String!
    var subTotal : String!
    var orderTotal : String!
    var productDetails : [NSDictionary]!
    @IBOutlet var mainContainerView: UIView!
    @IBOutlet weak var orderIDValue: UILabel!
    @IBOutlet weak var DOP: UILabel!
    @IBOutlet var orderDetailsTableView: UITableView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var DOPLabel: UILabel!
    @IBOutlet weak var shippingAddressButton: UIButton!
    @IBOutlet weak var productDetailsLabel: UILabel!
    
    
    override func viewWillAppear(animated: Bool) {
        title_Logo_Delegate!.mainTitleChange!("ORDER DETAILS".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(false)
        title_Logo_Delegate!.navigationBack!(self.navigationController!, tag: 0)
        
        if productDetails == nil {
            var params: NSDictionary!
            params  = ["Order_Id": self.orderId, "lang_key": LANGUAGE!]
            postorderDetails(params)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.orderIdLabel.text = "Order ID : ".localized(LANGUAGE!)
        self.DOPLabel.text = "PRODUCT DETAILS :".localized(LANGUAGE!)
        self.shippingAddressButton.setTitle("SHIPPING ADDRESS".localized(LANGUAGE!), forState: .Normal)
        self.productDetailsLabel.text = "PRODUCT DETAILS :".localized(LANGUAGE!)
        if LANGUAGE == "en"{
            self.orderIdLabel.frame = CGRectMake( 8, self.orderIdLabel.frame.origin.y, self.orderIdLabel.frame.size.width, self.orderIdLabel.frame.size.height)
            self.orderIdLabel.textAlignment = NSTextAlignment.Left
            
            self.orderIDValue.frame = CGRectMake((self.orderIdLabel.frame.origin.x + self.orderIdLabel.frame.width) + 8, self.orderIDValue.frame.origin.y, self.orderIDValue.frame.size.width, self.orderIDValue.frame.size.height)
            self.orderIDValue.textAlignment = NSTextAlignment.Left
            
            self.DOPLabel.frame = CGRectMake( 8, self.DOPLabel.frame.origin.y, self.DOPLabel.frame.size.width, self.DOPLabel.frame.size.height)
            self.DOPLabel.textAlignment = NSTextAlignment.Left
            
            self.DOP.frame = CGRectMake((self.DOPLabel.frame.origin.x + self.DOPLabel.frame.width) + 8, self.DOP.frame.origin.y, self.DOP.frame.size.width, self.DOP.frame.size.height)
            self.DOP.textAlignment = NSTextAlignment.Left
            self.productDetailsLabel.textAlignment = NSTextAlignment.Left
            
        }else{
            self.orderIdLabel.frame = CGRectMake((self.topView.frame.width - self.orderIdLabel.frame.width) - 8, self.orderIdLabel.frame.origin.y, self.orderIdLabel.frame.size.width, self.orderIdLabel.frame.size.height)
            self.orderIdLabel.textAlignment = NSTextAlignment.Right
            
            self.orderIDValue.frame = CGRectMake((self.orderIdLabel.frame.origin.x - self.orderIDValue.frame.width) - 8, self.orderIDValue.frame.origin.y, self.orderIDValue.frame.size.width, self.orderIDValue.frame.size.height)
            self.orderIDValue.textAlignment = NSTextAlignment.Right
            
            self.DOPLabel.frame = CGRectMake((self.topView.frame.width - self.DOPLabel.frame.width) - 8, self.DOPLabel.frame.origin.y, self.DOPLabel.frame.size.width - 5, self.DOPLabel.frame.size.height)
            self.DOPLabel.textAlignment = NSTextAlignment.Right
            
            self.DOP.frame = CGRectMake((self.DOPLabel.frame.origin.x - self.DOP.frame.width), self.DOP.frame.origin.y, self.DOP.frame.size.width, self.DOP.frame.size.height)
            self.DOP.textAlignment = NSTextAlignment.Right
            self.productDetailsLabel.textAlignment = NSTextAlignment.Right
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        orderIDValue.text = orderId
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Custom Delegate
    func mainTitleChange(title: String) {
        title_Logo_Delegate?.mainTitleChange!(title)
    }
    
    func ChangeLogoImage(isHide: Bool) {
        title_Logo_Delegate?.ChangeLogoImage!(isHide)
    }
    
    func HideBackButton(isHIde: Bool){
        title_Logo_Delegate?.HideBackButton!(isHIde)
    }
    func navigationBack(navi: UINavigationController, tag: Int) {
        title_Logo_Delegate?.navigationBack!(navi, tag: tag)
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.productDetails != nil  {
            return self.productDetails.count + 1
        } else{
            return 0
        }
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = ""
        if LANGUAGE == "en"{
            if indexPath.row == productDetails.count {
                identifier = "cartDetails"
            }else {
                identifier = "shoppingCartItems"
            }
        }else {
            if indexPath.row == productDetails.count {
                identifier = "cartDetails_ar"
            }else {
                identifier = "shoppingCartItems_ar"
            }
        }
        
        let cell = orderDetailsTableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! EXOrderDetailsCell
        
        if indexPath.row == productDetails.count {
            print(self.shippingCharge)
            let shipping = String(self.shippingCharge)
            cell.shippingCartValueLabel.text = shipping + "KD".localized(LANGUAGE!)
            let subTotal = String(self.subTotal)
            cell.subtotalValueLabel.text = subTotal + "KD".localized(LANGUAGE!)
            let orderTotal = String(self.orderTotal)
            cell.orderTotalValueLabel.text = orderTotal + "KD".localized(LANGUAGE!)
        }else {
            var loadingImage: String!
            if LANGUAGE == "en" {
                loadingImage = "Loading"
            }else {
                loadingImage = "loading_ar"
            }
            cell.productImageIcon.sd_setImageWithURL(NSURL(string: (self.productDetails[indexPath.row])["Image"]as! String), placeholderImage: UIImage(named: loadingImage))
            //cell.brandName.text = String(((self.productDetails[indexPath.row])["Quantity"])as! String)
            cell.brandName.text = (self.productDetails[indexPath.row])["BrandName"]as? String
            cell.itemName.text = (self.productDetails[indexPath.row])["ItemName"]as? String
            let price = (self.productDetails[indexPath.row])["Price"]as? String
            cell.priceValueLabel.text = price! + "KD".localized(LANGUAGE!)
            cell.quantityValueLabel.text = (self.productDetails[indexPath.row])["Quantity"]as? String
        }
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == productDetails.count {
            return self.mainContainerView.frame.size.width * 0.556666
        }else {
            return self.mainContainerView.frame.size.width * 0.333333
        }
    }
    
    // MARK: - WebServices
    func postorderDetails(params: NSDictionary) {
        EX_DELEGATE.showActivity(self.view, myTitle: "Loading..")
        EX_Network.postJson(WEB_SERVICE + "orderdetails.php", method: "POST", params: params as! Dictionary<String, AnyObject>, is_Dictionary: true) { (Succeeded, Json) -> () in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                print(Succeeded)
                print(Json)
                print(params)
                EX_DELEGATE.removeActivity(self.view)
                if Succeeded == true {
                    if let OrderDetailsData = Json["OrderDetailsPage"] {
                        
                        self.dateOfPurchase = OrderDetailsData["DateofPurchase"] as? String
                        
                        self.orderId = OrderDetailsData["OrderId"] as? String
                        
                        self.shippingCharge = OrderDetailsData["ShippingCharge"] as? String
                        
                        self.subTotal = OrderDetailsData["SubTotal"] as? String
                        
                        self.orderTotal = OrderDetailsData["OrderTotal"] as? String
                        
                        self.DOP.text = self.dateOfPurchase
                        
                        if (OrderDetailsData["ProductDetails"] as? [NSDictionary])?.count > 0{
                            self.productDetails = (OrderDetailsData["ProductDetails"] as? [NSMutableDictionary])!
                        }
                        
                        self.orderDetailsTableView.reloadData()
                    }
                }
            })
        }
    }

    @IBAction func shippingAddressButtonAction(sender: UIButton) {
        let purchaseDetails : EXPurchaseDetails = storyboard?.instantiateViewControllerWithIdentifier("EXPurchaseDetails") as! EXPurchaseDetails
        purchaseDetails.shippingCharge = ""
        purchaseDetails.orderTotal = ""
        purchaseDetails.subTotal = ""
        purchaseDetails.orderId = self.orderId!
        purchaseDetails.title_Logo_Delegate = self
        purchaseDetails.pageSelector = "Shipping"
        showViewController(purchaseDetails, sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class EXOrderDetailsCell: UITableViewCell {
    
    @IBOutlet var productImageIcon: UIImageView!
    @IBOutlet var brandName: UILabel!
    @IBOutlet var itemName: UILabel!
    @IBOutlet var priceValueLabel: UILabel!
    @IBOutlet var quantityValueLabel: UILabel!
    @IBOutlet var seperatorView: UIView!
    @IBOutlet var shippingCartValueLabel: UILabel!
    @IBOutlet var subtotalValueLabel: UILabel!
    @IBOutlet var orderTotalValueLabel: UILabel!
}
