//
//  EXShareTheApp.swift
//  Expo
//
//  Created by Ronish on 4/21/16.
//  Copyright © 2016 Mawaqaa. All rights reserved.
//

import UIKit
import Social

class EXShareTheApp: UIViewController {

    var title_Logo_Delegate : Change_Title_Logo_Delegate?
    @IBOutlet weak var shareLabel: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBarHidden = true
        title_Logo_Delegate!.mainTitleChange!("SHARE THE APP".localized(LANGUAGE!))
        title_Logo_Delegate!.ChangeLogoImage!(false)
        title_Logo_Delegate!.HideBackButton!(true)
        if LANGUAGE == "en" {
            shareLabel.text = "We would love to connect with you. You can also share our app with your friends and enjoy every offers we provide within your group. Its very simple here to....."
            shareLabel.textAlignment = NSTextAlignment.Left
        }else {
            shareLabel.text = "كنا نحب أن تواصل معكم . يمكنك أيضا مشاركة التطبيق لدينا مع أصدقائك و تتمتع كل العروض التي نقدمها ضمن مجموعتك . في غاية البساطة هنا ل ....."
            shareLabel.textAlignment = NSTextAlignment.Right
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func facebookButtonAction(sender: UIButton) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            var facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText("Share on Facebook")
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            var alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    @IBAction func googleButtonAction(sender: UIButton) {
        // Construct the Google+ share URL
        let urlComponents = NSURLComponents(URL: NSURL(string: "https://plus.google.com/share")!, resolvingAgainstBaseURL: true)
        let expectedHDQueryItem = NSURLQueryItem(name: "url", value: "shareURL")
        let url: NSURL = (urlComponents?.URL)!
        
        UIApplication.sharedApplication().openURL(url)
    }
    
    @IBAction func twitterButtonAction(sender: UIButton) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
            var twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitterSheet.setInitialText("Share on Twitter")
            self.presentViewController(twitterSheet, animated: true, completion: nil)
        } else {
            var alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
